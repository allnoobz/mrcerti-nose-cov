SQLAlchemy==0.8.0
Scrapy==0.16.4
Twisted==12.3.0
argparse==1.2.1
beautifulsoup4==4.2.0
boto==2.9.4
coverage==3.6
ddt==0.4.0
lxml==3.1.0
-e git+https://bitbucket.org/mrcertified/mrcerti-pdfminer.git#egg=pdfminer-dev
psutil==0.7.1
psycopg2==2.4.6
pyOpenSSL==0.13
python-dateutil==2.1
-e git+https://bitbucket.org/restan/morfeusz.git#egg=morfeusz
pytz==2012j
six==1.3.0
unittest-xml-reporting==1.7.0
w3lib==1.2
wsgiref==0.1.2
zope.component==4.1.0
zope.event==4.0.2
zope.interface==4.0.5
-e git+https://mrcertified:allahakbar@bitbucket.org/mrcertified/mrcerti-convert.git#egg=mrcerti_convert-dev
-e git+https://bitbucket.org/mrcertified/mrcerti-sqlalchemy-migrate.git#egg=sqlalchemy_migrate-dev


