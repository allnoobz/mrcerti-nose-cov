#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
from migrate.versioning.shell import main


def usage():
    print u"Usage: ./dbmanage <configuration file> ..."
    exit(1)

if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage()
    deploy_name = sys.argv[1]
    os.environ['DEPLOY_NAME'] = sys.argv[1]
    from mrcerti.crawler.settings import db
    url = db.DB_CONFIG['sqlalchemy.url']
    sys.argv[1:] = sys.argv[2:]
    main(url=url, debug='False', repository='migrations')
