# -*- coding: utf-8 -*-

from coverage import coverage
from runtests import runtests
import sys

if __name__ == '__main__':
    cov = coverage(source=['mrcerti.crawler'], omit=['*/tests/*'])
    cov.erase()
    cov.start()
    result = runtests('mrcerti.crawler', top_level_dir='.')
    cov.stop()
    cov.save()
    
    cov.html_report()
    
    exit_code = result
    sys.exit(exit_code)