#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import argparse
from mrcerti.crawler.scripts.db import setup_session
from mrcerti.crawler.scripts.job import (
    queue_run,
    queue_run_once, 
    queue_create, 
    queue_schedule_crawl,
    queue_clear, 
    queue_delete,
)


if __name__ == '__main__':
    setup_session()
    parser = argparse.ArgumentParser()
    parser.add_argument('--queue', metavar='NAME', nargs=1)
    parser.add_argument('--crawler', metavar='CRAWLER', nargs=1)
    parser.add_argument('--spider', metavar='SPIDER', nargs=1)
    parser.add_argument('--arg', metavar='KEY=VALUE', nargs=1, action='append')
    parser.add_argument('command', metavar='COMMAND', nargs=1)
    args = parser.parse_args(sys.argv[1:])
    if args.arg:
        args.kwargs = dict(
            kw[0].split('=')
            for kw in args.arg
        )
    if not args.command:
        parser.print_usage()
        print "no command was specified"
        exit(1)
    else:
        if not args.queue:
            parser.print_usage()
            print "argument --queue is required"
            exit(1)
        if args.command[0] == 'create':
            queue_create(queue=args.queue[0])
        elif args.command[0] == 'try_create':
            queue_create(queue=args.queue[0], silent=True)
        elif args.command[0] == 'delete':
            queue_delete(queue=args.queue[0])
        elif args.command[0] == 'clear':
            queue_clear(queue=args.queue[0])
        elif args.command[0] == 'run':
            queue_run(queue=args.queue[0])
        elif args.command[0] == 'run_once':
            queue_run_once(queue=args.queue[0])
        elif args.command[0] == 'schedule_crawl':
            if not args.crawler:
                parser.print_usage()
                print "argument --crawler is required for command schedule_crawl"
                exit(1)
            if not args.spider:
                parser.print_usage()
                print "argument --spider is required for command schedule_crawl"
                exit(1)
            queue_schedule_crawl(
                queue=args.queue[0], 
                crawler=args.crawler[0], 
                spider=args.spider[0], 
                **args.kwargs
            )
        else:
            parser.print_usage()
            print "Unknown command '%s'" % args.command[0]
