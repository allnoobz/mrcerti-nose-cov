# -*- coding: utf-8 -*-

from sqlalchemy import (
    MetaData,
    Table,
    Column,
    String,
    Integer,
    ForeignKey,
)


def upgrade(migrate_engine):
    meta = MetaData(bind=migrate_engine)
    result_relation = Table('result_relation', meta, autoload=True)
    result_relation.drop()
    
    meta = MetaData(bind=migrate_engine)
    Table('result', meta, autoload=True)
    result_relation = Table(
        'result_relation',
        meta,
        Column('relation_type', String(30), primary_key=True),
        Column('source_id', Integer, ForeignKey('result.id'), primary_key=True),
        Column('dest_scrapy_id', String(50), primary_key=True),
    )
    result_relation.create()


def downgrade(migrate_engine):
    meta = MetaData(bind=migrate_engine)
    result_relation = Table('result_relation', meta, autoload=True)
    result_relation.drop()

    meta = MetaData(bind=migrate_engine)
    result_relation = Table(
        'result_relation',
        meta,
        Column('relation_type', String(30), primary_key=True),
        Column('source_scrapy_id', String(50), primary_key=True),
        Column('dest_scrapy_id', String(50), primary_key=True),
    )
    result_relation.create()
