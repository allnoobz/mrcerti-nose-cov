from sqlalchemy import (
    MetaData,
    Table,
    Column,
    Integer,
    String,
    Boolean,
    LargeBinary,
    Sequence,
    DateTime,
    Text,
    ForeignKey
)

meta = MetaData()


item_id_seq = Sequence(
    'item_id_seq',
    start=1001,
    increment=1,
)

item = Table(
    'item', 
    meta,
    Column('id', Integer, item_id_seq, primary_key=True),
    Column('url', String(255), nullable=False),
    Column('spider', String(255), nullable=False),
    Column('extracted', Boolean, nullable=False, index=True),
    Column('queue', String(20)),
    Column('scrapy_item_id', String(50), nullable=False, index=True),
    Column('scrapy_item', LargeBinary),
    Column('item_hash', String(50))
)

result_id_seq = Sequence(
    'result_id_seq',
    start=1001,
    increment=1,
)

result = Table(
    'result',
    meta,
    Column('id', Integer, result_id_seq, primary_key=True),
    Column('type', String(30), nullable=False),
    Column('date', DateTime),
    Column('scrapy_item_id', String(50), nullable=False, index=True),
)

result_polish = Table(
    'result_polish',
    meta,
    Column('id', Integer, ForeignKey('result.id'), primary_key=True),
    Column('journal', String(20)),
    Column('year', Integer),
    Column('issue', Integer),
    Column('position', Integer),
    Column('title', Text),
    Column('status', String(30)),
    Column('publish_date', DateTime),
    Column('effective_date', DateTime),
    Column('publish_authority', String(20)),
    Column('signature', String(256)),
)

result_european = Table(
    'result_european',
    meta,
    Column('id', Integer, ForeignKey('result.id'), primary_key=True),
)

result_court_ruling = Table(
    'result_court_ruling',
    meta,
    Column('id', Integer, ForeignKey('result.id'), primary_key=True),
    Column('title', Text),
    Column('signature', String(256)),
    Column('ruling_date', DateTime),
    Column('reveive_date', DateTime),
    Column('court', String(256)),
    Column('judges', String(256)),
    Column('symbol_with_description', String(256)),
    Column('keywords', String(256)),
    Column('acused_body', String(256)),
    Column('result_text', String(256)),
)

result_tax_interpretation = Table(
    'result_tax_interpretation',
    meta,
    Column('signature', String(256)),
    Column('document_type', String(50)),
    Column('author', String(200)),
)

result_relation = Table(
    'result_relation',
    meta,
    Column('relation_type', String(30), primary_key=True),
    Column('source_scrapy_id', String(50), primary_key=True),
    Column('dest_scrapy_id', String(50), primary_key=True),
)

def upgrade(migrate_engine):
    meta.bind = migrate_engine
    
    item.create()
    result.create()
    result_polish.create()
    result_european.create()
    result_court_ruling.create()
    result_tax_interpretation.create()
    result_relation.create()

def downgrade(migrate_engine):
    meta.bind = migrate_engine
    
    result_relation.drop()
    result_tax_interpretation.drop()
    result_court_ruling.drop()
    result_european.drop()
    result_polish.drop()
    result.drop()
    item.drop()
