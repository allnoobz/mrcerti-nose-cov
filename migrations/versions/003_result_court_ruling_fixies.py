# -*- coding: utf-8 -*-

from sqlalchemy import (
    MetaData,
    Table,
    Column,
    String,
)

symbol_with_description = Column('symbol_with_description', String(256))
result_text = Column('result_text', String(256))


def upgrade(migrate_engine):
    meta = MetaData(bind=migrate_engine)
    court_ruling = Table('result_court_ruling', meta, autoload=True)

    court_ruling.c.acused_body.alter(name='defendant')
    court_ruling.c.reveive_date.alter(name='filing_date')
    symbol_with_description.drop(table=court_ruling)
    result_text.drop(table=court_ruling)


def downgrade(migrate_engine):
    meta = MetaData(bind=migrate_engine)
    court_ruling = Table('result_court_ruling', meta, autoload=True)

    result_text.create(table=court_ruling)
    symbol_with_description.create(table=court_ruling)
    court_ruling.c.filing_date.alter(name='reveive_date')
    court_ruling.c.defendant.alter(name='acused_body')
