# -*- coding: utf-8 -*-

from sqlalchemy import (
    MetaData,
    Table,
    Column,
    String,
    Date,
    Integer,
    Sequence,
    ForeignKey,
    PickleType,
    UniqueConstraint,
)


meta = MetaData()


job_queue_id_seq = Sequence(
    'job_queue_id_seq', 
    start=1001, 
    increment=1,
)

job_queue = Table(
    'job_queue',
    meta,
    Column('id', Integer, job_queue_id_seq, primary_key=True),
    Column('name', String(50), nullable=False)
)

job_queue_job_id_seq = Sequence(
    'job_queue_job_id_seq',
    start=1001,
    increment=1,
)

job_queue_job = Table(
    'job_queue_job',
    meta,
    Column('id', Integer, job_queue_job_id_seq, primary_key=True),
    Column('queue_id', Integer, ForeignKey('job_queue.id')),
    Column('order', Integer),
    Column('created', Date, nullable=False),
    Column('job_id', Integer, ForeignKey('job.id'), nullable=False),
    Column('status', String(20), nullable=True),
    UniqueConstraint('queue_id', 'order', name='queue_id_order_uc'),
)

job_id_seq = Sequence('job_id_seq', start=1001, increment=1)

job = Table(
    'job',
    meta,
    Column('id', Integer, job_id_seq, primary_key=True),
    Column('name', String(50), nullable=True),
    Column('type', String(20), nullable=False),
    Column('command', PickleType),
    Column('env', PickleType),
    Column('crawler', String(50)),
    Column('spider', String(50)),
)
    

def upgrade(migrate_engine):
    meta.bind = migrate_engine

    job_queue.create()
    job.create()
    job_queue_job.create()


def downgrade(migrate_engine):
    meta.bind = migrate_engine

    job_queue_job.drop()
    job.drop()
    job_queue.drop()
