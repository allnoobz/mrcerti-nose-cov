# -*- coding: utf-8 -*-

from sqlalchemy import (
    MetaData,
    Table,
    Column,
    String,
    Integer,
    ForeignKey,
)

old_meta = MetaData()
new_meta = MetaData()


def upgrade(migrate_engine):
    old_meta.bind = migrate_engine
    new_meta.bind = migrate_engine

    tax_interpretation = Table(
        'result_tax_interpretation',
        old_meta,
        autoload=True
    )
    tax_interpretation.drop()

    result = Table('result', new_meta, autoload=True)
    new_result_tax_interpretation = Table(
        'result_tax_interpretation',
        new_meta,
        Column('id', Integer, ForeignKey(result.c.id), primary_key=True),
        Column('signature', String(256)),
        Column('document_type', String(50)),
        Column('author', String(200)),
    )
    new_result_tax_interpretation.create()


def downgrade(migrate_engine):
    old_meta.bind = migrate_engine
    new_meta.bind = migrate_engine

    tax_interpretation = Table(
        'result_tax_interpretation',
        new_meta,
        autoload=True
    )
    tax_interpretation.drop()

    old_result_tax_interpretation = Table(
        'result_tax_interpretation',
        old_meta,
        Column('signature', String(256)),
        Column('document_type', String(50)),
        Column('author', String(200)),
    )
    old_result_tax_interpretation.create()
