# -*- coding: utf-8 -*-

import json
from scrapy import log, signals
from scrapy.contrib.spiders.crawl import CrawlSpider, Rule
from scrapy.http.request import Request
from scrapy.http.request.form import FormRequest
from scrapy.xlib.pydispatch import dispatcher
from mrcerti.crawler.exceptions import PageParseException


class FormSpider(CrawlSpider):
    item_link_extractors = ()
    search_page = None
    page_url_schema = None
    formdata_schema = None
    parse_all_pages_first = False
    
    run_spider = True
    
    def __init__(self, *args, **kwargs):
        super(FormSpider, self).__init__(*args, **kwargs)
        if 'show_info' in kwargs:
            log.msg(self.formdata_schema.info(), log.INFO)
            self.run_spider = False
            return
        self.formdata = self.formdata_schema.formdata(kwargs)
        self.results_to_parse = []
        self._compile_rules()
        if self.parse_all_pages_first:
            dispatcher.connect(self._schedule_results, signal=signals.spider_idle)
    
    def parse_start_url(self, response):
        self.log('Form data:\n%s' % json.dumps(self.formdata, indent=4), log.INFO)
        request = FormRequest.from_response(
            response,
            formdata=self.formdata,
            callback=self.after_search,
            #meta={'use_proxy':False}
        )
        return request
    
    def after_search(self,response):
        try:
            pages = self.extract_pages(response)
            if pages is None:
                self.log('No pages extracted. Stop crawling.', log.DEBUG)
                return
            self.log('Found %d pages of results.' % len(pages), log.DEBUG)
        except PageParseException:
            self.log('Cannot parse search result page.', log.ERROR)
            return
        for page in pages:
            request = Request(self.page_url_schema % page, callback=self.parse_page)
            yield request
    
    def extract_pages(self, response):
        raise NotImplemented()
    
    def parse_page(self, response):
        result_gen = self._parse_response(response, None, None)
        if self.parse_all_pages_first:
            self.results_to_parse.extend(list(result_gen))
        else:
            return result_gen
    
    def start_requests(self):
        if self.run_spider:
            self.start_urls = [self.search_page]
            for request in super(FormSpider, self).start_requests():
                yield request
        
    def _compile_rules(self):
        rules = []
        for extractor in self.item_link_extractors:
            rules.append(
                Rule(
                     extractor,
                     callback=self.parse_document
                )
            )
        self.rules = tuple(rules)
        super(FormSpider, self)._compile_rules()
    
    def _schedule_results(self, spider):
        self.log('Found %d search results.' % len(self.results_to_parse), log.INFO)
        for result_request in self.results_to_parse:
            spider.crawler.engine.crawl(result_request, spider)
        self.results_to_parse = []
