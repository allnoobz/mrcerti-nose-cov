# -*- coding: utf-8 -*-

import os
import contextlib
from unittest.case import (
    skipUnless,
    TestCase,
)
from uuid import uuid4
from multiprocessing import Queue, Process, Event
from sqlalchemy.engine import engine_from_config
from scrapy import signals, log
from scrapy.settings import Settings
from scrapy.crawler import CrawlerProcess
from mrcerti.crawler.signals import item_processed, workers_closed
from mrcerti.crawler.settings.db import test as db
from mrcerti.crawler.settings.www import test as www
from mrcerti.crawler.settings import CRAWLER_TESTS


def clean_db(db_config):
    from mrcerti.crawler.models.base import Base
    engine  = engine_from_config(db_config)
    with contextlib.closing(engine.connect()) as con:
        trans = con.begin()
        for table in reversed(Base.metadata.sorted_tables):
            con.execute(table.delete())
        trans.commit()


class CrawlerScript():
    
    def __init__(self, settings):
        self.crawler = CrawlerProcess(Settings(settings))
        self.items = []
        self.workers_enabled = settings.get('WORKERS_ENABLED', False)
        self.close_event = Event()
 
    def _item_passed(self, item):
        self.items.append(item)
    
    def _close(self):
        self.close_event.set()
 
    def _crawl(self, queue, spider):
        item_passed_signal = signals.item_passed
        close_signal = signals.engine_stopped
        if self.workers_enabled:
            item_passed_signal = item_processed
            close_signal = workers_closed
        self.crawler.signals.connect(self._item_passed, item_passed_signal)
        self.crawler.signals.connect(self._close, close_signal)
        
        self.crawler.install()
        self.crawler.configure()
        
        self.crawler.crawl(spider)
        self.crawler.start()
        self.close_event.wait()
        queue.put(self.items)

    def crawl(self, spider):
        queue = Queue()
        p = Process(target=self._crawl, args=(queue, spider,))
        p.start()
        self.close_event.wait()
        items = queue.get(True)
        p.terminate()
        return items


CRAWLER_TESTS = True

@skipUnless(CRAWLER_TESTS, "Crawler tests off")
class SpiderTestCase(TestCase):
    
    def setUp(self):
        super(SpiderTestCase, self).setUp()
        from mrcerti.crawler.settings.crawler import base
        self.settings = {
            'DB_CONFIG': db.DB_CONFIG,
            'DOWNLOAD_DIR': 'data',
            'API_URL': www.API_URL,
            'EXTENSIONS': {
                'mrcerti.crawler.extensions.dbconnector.DatabaseConnector': 10,
                'mrcerti.crawler.extensions.workers.ItemProcessors': 600,
            },
            'WORKERS_ENABLED': False,
            'OVERRIDE_DUPLICATES': True,
            'SPIDER_MIDDLEWARES': base.SPIDER_MIDDLEWARES,
            'DOWNLOADER_MIDDLEWARES': base.DOWNLOADER_MIDDLEWARES,
        }
    
    def tearDown(self):
        clean_db(self.settings['DB_CONFIG'])
        
    def run_spider(self, spider):
        crawler_script = CrawlerScript(self.settings)
        return crawler_script.crawl(spider)
    
    def assertItemIdIn(self, item_id, items):
        self.assertIn(item_id, {
            item['item_id'] for item in items
        })


class SpiderTestCaseWithScrapyObserver(SpiderTestCase):

    def __init__(self, *args, **kwargs):
        self.level = kwargs.pop('level', log.INFO)
        self.encoding = 'utf-8'
        self.fname = kwargs.pop('fname', 'spider_test_case')

        super(SpiderTestCaseWithScrapyObserver, self).__init__(*args, **kwargs)

    def setUp(self):
        super(SpiderTestCaseWithScrapyObserver, self).setUp()

        try:
            self.path = os.path.join('/', 'tmp', '%s_%s.log' %
                (self.fname, uuid4().hex))
            self.f = open(self.path, 'w+')
            self.sflo = log.ScrapyFileLogObserver(
                self.f, self.level, self.encoding
            )
            self.sflo.start()
        except IOError:
            self.f = None

    def tearDown(self):
        super(SpiderTestCaseWithScrapyObserver, self).tearDown()
        if self.f:
            self.sflo.stop()
            self.f.close()
