# -*- coding: utf-8 -*-

import urllib
from scrapy.http.request import Request
from mrcerti.crawler.spiders.isap.base import IsapBaseSpider


class IsapSingleDocumentSpider(IsapBaseSpider):
    
    name = 'single_document_spider'
    base_url = 'http://isap.sejm.gov.pl/DetailsServlet'

    def get_url(self):
        urlparams = dict()
        urlparams['id'] = self.item_id
        urlparams = urllib.urlencode(urlparams)
        url = '%s?%s' % (self.base_url, urlparams)
        return url

    def __init__(self, item_id=None, **kwargs):
        super(IsapSingleDocumentSpider, self).__init__(**kwargs)
        assert item_id is not None
        self.item_id = item_id
        self.start_url = self.get_url()
    
    def start_requests(self):
        return [Request(self.start_url, callback=self.parse_document)]
