# -*- coding: utf-8 -*-

from mrcerti.crawler.spiders.isap.keyword import KeywordSpider
from mrcerti.crawler.spiders.isap.tests.isap_spider_test_case import IsapSpiderTestCase


class KeywordSpiderTestCase(IsapSpiderTestCase):
    
    def test_crawling_single_page(self):
        keyword = u'badania geologiczne'
        spider = KeywordSpider(keyword=keyword)
        items = self.run_spider(spider)
        self.assertEquals(len(items), 2)
        self.assertItemIdIn('WDU19910320131', items)
        self.assertItemIdIn('WDU19940270096', items)
