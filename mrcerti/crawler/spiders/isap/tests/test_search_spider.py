# -*- coding: utf-8 -*-

from mrcerti.crawler.spiders.isap.search import SearchSpider
from mrcerti.crawler.spiders.isap.tests.isap_spider_test_case import IsapSpiderTestCase


class SearchSpiderTestCase(IsapSpiderTestCase):
    
    def test_crawling_single_page(self):
        spider = SearchSpider(
            status=SearchSpider.STATUS_ALL,
            publisher=SearchSpider.PUBLISHER_ACT_JOURNAL,
            year='2007',
            issue='',
            position='',
            title='',
            types='UST',
            keywords='',
            names='',
            start_date='2007.08.01',
        )
        items = self.run_spider(spider)
        self.assertEquals(len(items), 12)
        items_ids = {
            item['item_id']
            for item in items
        }
        self.assertItemsEqual(
            items_ids,
            {
                'WDU20071911365', 
                'WDU20071711207', 
                'WDU20071801280',
                'WDU20071711206', 
                'WDU20071911366', 
                'WDU20071911367',
                'WDU20071911364', 
                'WDU20071731219', 
                'WDU20071911363',
                'WDU20071651170', 
                'WDU20071921378', 
                'WDU20071921379',
            },
        )
    
    def test_crawling_multiple_pages(self):
        spider = SearchSpider(
            status=SearchSpider.STATUS_ALL,
            publisher=SearchSpider.PUBLISHER_ACT_JOURNAL,
            year='2007',
            title='Ustawa'
        )
        items = self.run_spider(spider)
        self.assertEqual(len(items), 39)
