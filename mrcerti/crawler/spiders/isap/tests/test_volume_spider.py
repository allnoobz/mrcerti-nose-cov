# -*- coding: utf-8 -*-

from mrcerti.crawler.spiders.isap.tests.isap_spider_test_case import (
    IsapSpiderTestCase,
)
from mrcerti.crawler.spiders.isap.volume import VolumeSpider


class VolumeSpiderTestCase(IsapSpiderTestCase):
    
    def test_crawling_single_page(self):
        spider = VolumeSpider(
            years='2001', 
            issues='154',
        )
        items = self.run_spider(spider)
        self.assertEqual(len(items), 1)
        self.assertItemIdIn('WDU20011541785', items)
        
    def test_crawling_multiple_entries(self):
        spider = VolumeSpider(
            years='1990', 
            issues='53,54',
        )
        items = self.run_spider(spider)
        self.assertEqual(len(items), 4)
        self.assertItemIdIn('WDU19900530307', items)
        self.assertItemIdIn('WDU19900540310', items)
        self.assertItemIdIn('WDU19900540311', items)
        self.assertItemIdIn('WDU19900540312', items)
        
    def test_crawling_whole_year(self):
        spider = VolumeSpider(
            years='1980',
        )
        items = self.run_spider(spider)
        self.assertEqual(len(items), 6)
        self.assertItemIdIn('WDU19800030006', items)
        self.assertItemIdIn('WDU19800070018', items)
        self.assertItemIdIn('WDU19800260104', items)
        self.assertItemIdIn('WDU19800270111', items)
        self.assertItemIdIn('WDU19800270112', items)
        self.assertItemIdIn('WDU19800220082', items)
