# -*- coding: utf-8 -*-

from datetime import date
from mrcerti.crawler.spiders.isap.single_document import IsapSingleDocumentSpider
from mrcerti.crawler.spiders.isap.tests.isap_spider_test_case import IsapSpiderTestCase
from mrcerti.crawler.items.isap import (
    IsapFileItem, 
    IsapRelationItem,
    IsapRelatedDocumentItem,
)


class SingleDocumentSpiderTestCase(IsapSpiderTestCase):

    def test_crawling(self):
        ident = 'WDU20071911363'
        spider = IsapSingleDocumentSpider(ident)
        items = self.run_spider(spider)
        self.assertEqual(len(items), 1, 'one document should be processed')
        item = items[0]

        self.assertIsNone(item['repeal_date'])
        self.assertIsNone(item['obliged_authority'])
        self.assertIsNone(item['authorized_authority'])
        self.assertIsNone(item['notes'])
        
        self.assertEqual(item['item_id'], 'WDU20071911363', 'invalid ident')
        self.assertEqual(
            item['url'],
            'http://isap.sejm.gov.pl/DetailsServlet?id=WDU20071911363',
        )
        self.assertEqual(
            item['signature'], 
            {
                'journal': 'polish',
                'year': '2007',
                'issue': '191',
                'position': '1363',
            },
        )
        self.assertEqual(
            item['title'],
            u'Ustawa z dnia 7 września 2007 r. o czasowym posługiwaniu '
            u'się dowodami osobistymi wydanymi przed dniem 1 stycznia 2001 r.',
            'invalid title'
        )
        self.assertIsInstance(item['published_file'], IsapFileItem)
        self.assertEquals(
            item['published_file']['url'],
            'http://isap.sejm.gov.pl/Download?id=WDU20071911363&type=1'
        )
        self.assertIsInstance(item['announced_file'], IsapFileItem)
        self.assertEquals(
            item['announced_file']['url'],
            'http://isap.sejm.gov.pl/Download?id=WDU20071911363&type=2'
        )
        self.assertIsNone(item['consistent_file'])
        
        self.assertEquals(item['status'], u'obowiązujący', 'invalid status')
        self.assertEquals(item['announced_date'].date(), date(2007, 10, 18), 'invalid announced_date')
        self.assertEquals(item['publish_date'].date(), date(2007, 9, 7), 'invalid publish_date')
        self.assertEquals(item['effective_date'].date(), date(2007, 12, 31), 'invalid effective_date')
        self.assertEquals(item['validity_date'].date(), date(2007, 12, 31), 'invalid validity_date')
        self.assertEquals(item['publish_authority'], u'SEJM', 'invalid publish_authority')
        
        self.assertIsInstance(item['relations'], list)
        self.assertEqual(len(item['relations']), 1)
        
        relation = item['relations'][0]
        self.assertIsInstance(relation, IsapRelationItem)
        self.assertEquals(relation['relation_type'], u'odesłania')
        self.assertIsNotNone(relation['documents'])
        self.assertEquals(len(relation['documents']), 1)
        
        related_document = relation['documents'][0]
        self.assertIsInstance(related_document, IsapRelatedDocumentItem)
        self.assertEquals(related_document['item_id'], u'WDU19971130733')
        self.assertEquals(
            related_document['url'], 
            u'http://isap.sejm.gov.pl/DetailsServlet?id=WDU19971130733&min=1'
        )
        self.assertEquals(
            related_document['title'], 
            u'Ustawa z dnia 20 sierpnia 1997 r. o zmianie ustawy '
            u'o ewidencji ludno\u015bci i dowodach osobistych oraz '
            u'ustawy o dzia\u0142alno\u015bci gospodarczej.'
        )
        self.assertEquals(
            related_document['signature'],
            {
                'journal': 'polish',
                'year': u'1997',
                'issue': u'113',
                'position': u'733'
            },
        )
        self.assertEqual(related_document['status'], u'obowi\u0105zuj\u0105cy')
        self.assertEqual(related_document['effective_date'], None)
