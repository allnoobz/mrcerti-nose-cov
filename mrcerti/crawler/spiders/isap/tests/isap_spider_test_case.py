# -*- coding: utf-8 -*-

from mrcerti.crawler.settings.crawler import isap
from mrcerti.crawler.spiders.tests.spider_test_case import SpiderTestCase


class IsapSpiderTestCase(SpiderTestCase):
    
    def setUp(self):
        super(IsapSpiderTestCase, self).setUp()
        assert self.settings
        self.settings.update(
            {
                'ITEM_PIPELINES': [
                    'mrcerti.crawler.pipelines.DuplicatesPipeline',
                ],
                'DOWNLOADER_MIDDLEWARES' : isap.DOWNLOADER_MIDDLEWARES
            }
        )
