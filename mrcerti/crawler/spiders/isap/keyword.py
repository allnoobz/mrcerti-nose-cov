# -*- coding: utf-8 -*-

import string
import re
import urllib
from urlparse import urlparse
from scrapy.http.request import Request
from scrapy.selector.lxmlsel import HtmlXPathSelector
from mrcerti.crawler.utils.deferred import wait_for, deferred, _result, no_wait
from mrcerti.crawler.spiders.isap.base import IsapBaseSpider


class KeywordSpider(IsapBaseSpider):
    
    name = 'keyword_spider'
    keyword_url = 'http://isap.sejm.gov.pl/KeyWordServlet'
    
    def _make_url(self, keyword):
        urlparams = {}
        view_name_letter = keyword[0]
        if view_name_letter in string.ascii_letters:
            urlparams['viewName'] = 'thas%s' % view_name_letter.upper()
        else:
            urlparams['viewName'] = 'thas%s' % {
                u'ł': 'LL',
                u'ś': 'SS',
                u'ż': 'ZZ',
            }[view_name_letter.lower()]
        urlparams['passName'] = keyword.encode('utf8')
        urlparams = urllib.urlencode(urlparams)
        return '%s?%s' % (self.keyword_url, urlparams)
    
    def __init__(self, keyword=None, **kwargs):
        super(KeywordSpider, self).__init__(**kwargs)
        if keyword is None:
            raise ValueError(u"Keyword parameter must be specified.")
        self.keyword = keyword
        self.keyword = self.keyword.strip()
        self.keyword = self.keyword.decode('utf-8')
        self.url = self._make_url(self.keyword)
    
    def start_requests(self):
        return [Request(self.url, callback=self.parse_page)]
    
    @deferred
    def parse_page(self, response):
        return self._parse_page(response)
    
    def _parse_page(self, response):
        def select_page_data(response):
            hxs = HtmlXPathSelector(response)
            return hxs.select(
                '/html/body/table/tr[3]/td[2]/table/'
                'tr[1]/td[1]/table[1]/tr[4]/td/table'
            )
        
        def has_pagination(response):
            data = select_page_data(response)
            text = ' '.join(data.select('tr[5]/td[1]//text()').extract())
            text = re.sub('\s+', '', text)
            return True if re.search('[0-9]+-[0-9]+z[0-9]+', text) else False
        
        def extract_next_page_url(response):
            if has_pagination(response):
                data = select_page_data(response)
                urls = data.select('tr[5]/td/a/@href').re('.*isNext=true.*')
                url = self.absolute_url(urls[0]) if urls else None
                return url
            else:
                return None
        
        def extract_document_urls(response):
            data = select_page_data(response)
            if has_pagination(response):
                row_query = 'tr[position() > 5 and position() < last()]'
            else:
                row_query = 'tr[position() > 4]'
            rows = data.select(row_query)
            urls = rows.select('td[1]/a/@href').extract()
            urls = [self.absolute_url(url) for url in urls]
            return urls
        
        for url in extract_document_urls(response):
            assert url.startswith(self.details_url)
            document_request = Request(url, callback=self.parse_document)
            yield no_wait(document_request)
        
        next_page_url = extract_next_page_url(response)
        
        if next_page_url:
            assert next_page_url.startswith(self.keyword_url)
            assert 'isNext=true' in urlparse(next_page_url).query.split('&')
            next_page_request = Request(next_page_url, callback=self._parse_page)
            yield wait_for(next_page_request)
        
        yield _result()
