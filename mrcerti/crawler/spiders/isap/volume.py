# -*- coding: utf-8 -*-

import urllib
import urlparse
from scrapy.http.request import Request
from scrapy.selector.lxmlsel import HtmlXPathSelector
from mrcerti.crawler.spiders.isap.base import IsapBaseSpider


class VolumeSpider(IsapBaseSpider):
    
    name = 'volume_spider'
    volume_url = 'http://isap.sejm.gov.pl/VolumeServlet'
    
    def __init__(self, years=None, issues=None, document_type='wdu', **kwargs):
        super(VolumeSpider, self).__init__(**kwargs)
        
        self.years = years.split(',') if isinstance(years, basestring) else []
        assert all([year.isdigit() for year in self.years])
        
        self.issues = issues.split(',') if isinstance(issues, basestring) else []
        assert all([issue.isdigit() for issue in self.issues])
        self.issues = ['%03d' % int(issue) for issue in self.issues]
        
        self.document_type = document_type.lower()
        assert self.document_type in ['wdu', 'wmp']
    
    def start_requests(self):
        urlparams = {'type': self.document_type}
        type_url = '%s?%s' % (self.volume_url, urllib.urlencode(urlparams))
        return [Request(type_url, callback=self.parse_type)]
    
    def _get_page_data(self, response):
        hxs = HtmlXPathSelector(response)
        return hxs.select(
            '/html/body/table/tr[3]/td[2]/table/tr[1]/td[1]/table[1]'
        )
    
    def _extract_document_type(self, response):
        return urlparse.parse_qs(urlparse.urlparse(response.url).query)['type'][0]
    
    def _extract_year(self, response):
        return urlparse.parse_qs(urlparse.urlparse(response.url).query)['rok'][0]
    
    def _extract_issue(self, response):
        return urlparse.parse_qs(urlparse.urlparse(response.url).query)['numer'][0]
    
    def parse_type(self, response):
        def _extract_years(response):
            data = self._get_page_data(response)
            years = data.select('tr[6]/td/a/text()').extract()
            years = [year.strip() for year in years]
            return years
        
        document_type = self._extract_document_type(response)
        years = _extract_years(response)
        for year in years:
            if year in self.years or not self.years:
                urlparams = {'type': document_type, 'rok': year}
                year_url = '%s?%s' % (self.volume_url, urllib.urlencode(urlparams))
                yield Request(year_url, callback=self.parse_year)
    
    def parse_year(self, response):
        def _extract_issues(response):
            data = self._get_page_data(response)
            issues = data.select('tr[6]/td/a/text()').extract()
            issues = [issue.strip() for issue in issues]
            return issues
        
        document_type = self._extract_document_type(response)
        year = self._extract_year(response)
        issues = _extract_issues(response)
        for issue in issues:
            if issue in self.issues or not self.issues:
                urlparams = {'type': document_type, 'rok': year, 'numer': issue}
                issue_url = '%s?%s' % (self.volume_url, urllib.urlencode(urlparams))
                yield Request(issue_url, callback=self.parse_issue)
    
    def parse_issue(self, response):
        def _extract_documents(response):
            data = self._get_page_data(response)
            document_urls = data.select('tr/td[1]/a/@href').re('DetailsServlet\?id=[^&]+')
            return document_urls
        
        document_urls = _extract_documents(response)
        for document_url in document_urls:
            document_url = urlparse.urljoin(self.details_url, document_url)
            yield Request(document_url, callback=self.parse_document)
