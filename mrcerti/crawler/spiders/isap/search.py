# -*- coding: utf8 -*-

import datetime
import time
from scrapy.http.request import Request
from scrapy.selector.lxmlsel import HtmlXPathSelector
from mrcerti.crawler.spiders.isap.base import IsapBaseSpider
from scrapy.http.request.form import FormRequest


class SearchSpider(IsapBaseSpider):
    
    STATUS_ALL = 'all'
    STATUS_EFFECTIVE = 'effective'
    
    PUBLISHER_ACT_JOURNAL = 'journal'
    PUBLISHER_ACT_MONITOR = 'monitor'
    
    name = 'search_spider'
    search_url = 'http://isap.sejm.gov.pl/search.jsp'
    
    def __init__(self, **kwargs):
        super(SearchSpider, self).__init__(**kwargs)
        self.form_data = self._make_form(**kwargs)
    
    def start_requests(self):
        return [Request(self.search_url, callback=self.parse_search_page)]
    
    def parse_search_page(self, response):
        return [
            FormRequest.from_response(
                response,
                formdata=self.form_data,
                callback=self.parse_result_page
            )
        ]
    
    def parse_result_page(self, response):
        hxs = HtmlXPathSelector(response)
        data = hxs.select('/html/body/table/tr[3]/td[2]/table/tr[1]/td[1]')
        waiting_node = data.select('meta')
        if waiting_node.extract():
            time.sleep(1)
            return [Request(response.url, self.parse_result_page)]
        elif data.re(u'Proszę uszczegółowić kryterium szukania.'):
            raise ValueError(u'Too many results')
        else:
            list_node = data.select('table/tbody/tr/td')
            if list_node.re(u'(Znaleziono więcej niż .* dokumentów)'):
                raise ValueError(u'Too many results')
            urls = list_node.select('a/@href').re(ur'(DetailsServlet\?id=[^&]+)')
            urls = [self.absolute_url(url) for url in urls]
            requests = [Request(url, self.parse_document) for url in urls]
            return requests
    
    def _make_date_form(self, start_date='', end_date='', date=''):
        fmt = '%Y.%m.%d'
        date_range = '-'
        if date:
            assert not start_date
            assert not end_date
            start_date = end_date = datetime.datetime.strptime(date, fmt)
        else:
            if start_date:
                start_date = datetime.datetime.strptime(start_date, fmt)
            else:
                start_date = datetime.date(1900, 1, 1)            
            if end_date:
                end_date = datetime.datetime.strptime(end_date, fmt)
            else:
                end_date = datetime.date(2100, 1, 1)
        return {
            'dwyddd_1': '%02d' % start_date.day,
            'dwydmm_1': '%02d' % start_date.month,
            'dwydrr_1': '%04d' % start_date.year,
            'dwyddd_2': '%02d' % end_date.day,
            'dwydmm_2': '%02d' % end_date.month,
            'dwydrr_2': '%04d' % end_date.year,
            'DWydTyp': date_range,
        }
    
    def _make_status_form(self, status=STATUS_ALL):
        if status == self.STATUS_EFFECTIVE:
            status = 'M'
        elif status == self.STATUS_ALL:
            status = 'W'
        else:
            raise ValueError('Invalid status')
        return {
            'st': status,
        }
        
    def _make_publisher_form(self, publisher='', year='', issue='', position=''):
        if publisher:
            if publisher == self.PUBLISHER_ACT_JOURNAL:
                publisher = 'WDU'
            elif publisher == self.PUBLISHER_ACT_MONITOR:
                publisher = 'WMP'
            else:
                raise ValueError("Invalid publisher")
        return {
            'adresWyd': publisher,
            'adresRR': year,
            'adresNr': issue,
            'adresPo': position,
        }
    
    def _make_criteria_form(self, title, types, keywords, names):
        return {
            'tyt': title,
            'rodz': types,
            'RodzChecked': bool(types) and 'on' or '',
            'has': keywords,
            'HasChecked': bool(keywords) and 'on' or '',
            'naz': names,
            'NazChecked': bool(names) and 'on' or '',
        }
    
    def _make_form(
            self,
            status=STATUS_ALL,
            publisher=PUBLISHER_ACT_JOURNAL, 
            year='', 
            issue='', 
            position='',
            title='', 
            types='', 
            keywords='', 
            names='', 
            start_date='',
            end_date='',
            date='',
        ):
        result = {
            'sort': '0',
            'max': '500',
        }
        result.update(
            self._make_status_form(status=status),
        )
        result.update(
            self._make_date_form(
                start_date=start_date, 
                end_date=end_date, 
                date=date,
            ),
        )
        result.update(
            self._make_publisher_form(
                publisher=publisher,
                year=year,
                issue=issue,
                position=position,
            ),
        )
        result.update(
            self._make_criteria_form(
                title=title,
                types=types, 
                keywords=keywords, 
                names=names,
            ),
        )
        return result
