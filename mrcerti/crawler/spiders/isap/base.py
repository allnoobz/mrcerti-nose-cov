# -*- coding: utf-8 -*-

import re
import urlparse
from urlparse import urljoin
from scrapy import log
from scrapy.spider import BaseSpider
from scrapy.http.request import Request
from scrapy.selector.lxmlsel import HtmlXPathSelector
from mrcerti.crawler.items.isap import (
    IsapDocumentItem, 
    IsapFileItem,
    IsapRelationItem,
    IsapRelatedDocumentItem,
)
from mrcerti.crawler.utils.deferred import (
    deferred, 
    wait_for, 
    _result,
)


class IsapBaseSpider(BaseSpider):
    
    allowed_domains = ['isap.sejm.gov.pl']
    base_url = 'http://isap.sejm.gov.pl'
    details_url = 'http://isap.sejm.gov.pl/DetailsServlet'
    
    def _extract_ident(self, response):
        return urlparse.parse_qs(urlparse.urlparse(response.url).query)['id'][0]
    
    def absolute_url(self, relative):
        return urljoin(self.base_url, relative)
    
    @deferred
    def parse_document(self, response):
        self.log("parse_document %s" % repr(response), log.DEBUG)
        
        item = IsapDocumentItem()
        item['item_id'] = self._extract_ident(response)
        item['url'] = response.url
        item['spider'] = 'isap'
        if self._is_polish_monitor(item):
            item = self._parse_polish_monitor(response, item)
        elif self._is_polish_legal_journal(item):
            item = yield wait_for(self._parse_polish_legal_journal(response, item))
        else:
            raise ValueError(u"Unrecognised document type for item: %s" % unicode(item))
        yield _result(item)
    
    def _parse_polish_monitor(self, response, item):
        # skip Monitor Polski
        return None
    
    def _parse_polish_legal_journal(self, response, item):
        def _select_main(response):
            hxs = HtmlXPathSelector(response)
            return hxs.select(
                '/html/body/table/tr[3]/td[2]/table/tr[1]/td[1]'
            )
        
        def _select_data(response):
            return _select_main(response).select(
                'table[1]/tbody'
            )
    
        def _select_related_table(response):
            hxs = HtmlXPathSelector(response)
            return hxs.select(
                '/html/body/table[2]'
            )
    
        def _select_related(response):
            return _select_main(response).select(
                'table[2]/tbody/tr/td/a'
            )
        
        def _extract_basic_field(element):
            return element.extract()[0].strip()
        
        def _extract_row_link(row):
            return row.select('td/a/@href').extract()[0]
        
        def _extract_row_value(row):
            return row.select('td/text()').extract()[0]
        
        def _extract_row_caption(row):
            return row.select('th/text()').extract()[0].strip(': ')
        
        def _remove_session_id(url):
            return re.sub(';jsessionid=[0-9A-Z]+', '', url)
        
        def _parse_row_link(row, name, item):
            item[name] = _extract_row_link(row)
            return item
        
        def _parse_row_value(row, name, item):
            item[name] = _extract_row_value(row)
            return item
        
        def _parse_download(row, type_):
            item = IsapFileItem()
            item['type'] = type_
            download_url = _extract_row_link(row)
            download_url = _remove_session_id(download_url)
            m = re.search(ur'Download\?id=([^&]+)&type=([0-9]+)', download_url, flags=re.UNICODE)
            item['item_id'] = m.group(1)
            item['url'] = self.absolute_url(download_url)
            item['type_id'] = m.group(2)
            item['format'] = 'pdf'
            item['save_path'] = None
            item['saved'] = False
            return item
        
        def _parse_basic_row(row, item):
            caption = _extract_row_caption(row)
            if caption == u'Tekst aktu':
                item['published_file'] = _parse_download(row, 'published')
            elif caption == u'Tekst ogłoszony':
                item['announced_file'] = _parse_download(row, 'announced')
            elif caption == u'Tekst ujednolicony':
                item['consistent_file'] = _parse_download(row, 'consistent')
            elif caption == u'Status aktu prawnego':
                item['status'] = _extract_row_value(row)
            elif caption == u'Data ogłoszenia': 
                item['announced_date'] = _extract_row_value(row)
            elif caption == u'Data wydania': 
                item['publish_date'] = _extract_row_value(row)
            elif caption == u'Data wejścia w życie': 
                item['effective_date'] = _extract_row_value(row)
            elif caption == u'Data obowiązywania':
                item['validity_date'] = _extract_row_value(row)
            elif caption == u'Data uchylenia': 
                item['repeal_date'] = _extract_row_value(row)
            elif caption == u'Data wygaśnięcia': 
                item['expiry_date'] = _extract_row_value(row)
            elif caption == u'Organ wydający': 
                item['publish_authority'] = _extract_row_value(row)
            elif caption == u'Organ zobowiązany': 
                item['obliged_authority'] = _extract_row_value(row)
            elif caption == u'Organ uprawniony': 
                item['authorized_authority'] = _extract_row_value(row)
            elif caption == u'Uwagi': 
                item['notes'] = _extract_row_value(row)
            else:
                raise ValueError(u"Unrecognised ISAP field caption: '%s'" % caption)
            return item
        
        def _parse_rows(rows, item):
            item = item or {}
            for row in rows:
                item = _parse_basic_row(row, item)
            return item
        
        def _parse_info(response, item):
            data = _select_data(response)
            item['signature'] = _extract_basic_field(data.select('tr[1]/td/span/text()'))
            item['title'] = _extract_basic_field(data.select('tr[2]/td/span/p/text()'))
            rows = data.select(
                'tr[position() > 2]'
            )
            item['relations'] = []
            return _parse_rows(rows, item)
            
        def _extract_relation_urls(response):
            related_urls = _select_related(response).select('@onclick')
            related_urls = related_urls.re(r'popitup\(\'(.*?)&isNew=true\'\)')
            related_urls = [self.absolute_url(url) for url in related_urls]
            return related_urls
        
        def _parse_relations(response, item):
            # The rest of processing takes place after the relation page is
            # fetched.
            related_urls = _extract_relation_urls(response)
            for url in related_urls:
                request = Request(url, self.parse_relation)
                request.meta['item'] = item
                request.meta['relation_urls'] = related_urls
                request.meta['cookiejar'] = response.url
                relation_item = yield wait_for(request)
                item['relations'].append(relation_item)
            yield _result(item)
        
        item = _parse_info(response, item)
        
        if self._is_amendment(item) and not self.follow_amendment:
            yield _result(None)
        
        if not self._is_legislative_act(item):
            # For now we skip everything other than legal acts.
            yield _result(None)
        
        item = yield wait_for(_parse_relations(response, item))
        yield _result(item)
    
    def _is_amendment(self, item):
        return re.search(ur'(o zmianie)|(zmieniająca)', item['title'], flags=re.UNICODE)
    
    def _is_legislative_act(self, item):
        return (
            item['publish_authority'] is not None and
            re.match(ur'^SEJM$', item['publish_authority'], flags=re.UNICODE) and
            not item['title'].lower().startswith(
                u'obwieszczenie marszałka sejmu rzeczypospolitej polskiej'
            )
        )
    
    def _is_polish_monitor(self, item):
        assert item['item_id'] is not None
        return item['item_id'].lower().startswith('wmp')
    
    def _is_polish_legal_journal(self, item):
        assert item['item_id'] is not None
        return item['item_id'].lower().startswith('wdu')
    
    def parse_relation(self, response):
        
        def _select_main(response):
            hxs = HtmlXPathSelector(response)
            return hxs.select(
                '/html/body/table[2]'
            )
        
        def _select_caption(response):
            main = _select_main(response)
            return main.select(
                'caption/text()'
            ) or main.select(
                'tbody/tr[1]/td[1]/font/text()'
            )
        
        def _extract_relation_type(respose):
            return _select_caption(response).extract()[1].strip().lower()
        
        def _has_date(item):
            return item['relation_type'] not in [
                u'odesłania',
                u'orzeczenie tk',
                u'informacja o tekście jednolitym',
                u'akty wykonawcze',
                u'akty zmieniające',
                u'akty uchylające',
            ]
        
        item = IsapRelationItem()
        item['relation_type'] = _extract_relation_type(response)
        
        if self._is_relation_european(item):
            item['documents'] = yield wait_for(self._parse_relation_european(response))
        else:
            assert self._is_relation_polish(item)
            item['documents'] = yield wait_for(self._parse_relation_polish(response, with_date=_has_date(item)))
        yield _result(item)
    
    def _is_relation_european(self, item):
        return item['relation_type'] == u"dyrektywy europejskie"
    
    def _parse_relation_european(self, response):
        
        def _select_main(response):
            hxs = HtmlXPathSelector(response)
            return hxs.select(
                '/html/body/table[2]'
            )
        
        def _select_rows(response):
            return _select_main(response).select(
                'tbody/tr'
            )
        
        def _parse_row(row, related_item):
            related_item['journal'] = 'eur_lex'
            related_item['effective_date'] = row.select(
                'td[2]/text()'
            ).extract()[0].strip()
            related_item['title'] = row.select(
                'td[3]/p/text()'
            ).extract()[0].strip()
            related_item['url'] = row.select(
                'td[4]/a/@href'
            ).re(
                ur"javascript\s*:\s*newDyrektywy\s*\(\s*'([^']*)'\s*\)\s*;\s*"
            )[0]
            m = re.search(ur"uri=(.*)$", related_item['url'])
            related_item['item_id'] = m.group(1)
            return related_item
        
        def _is_header(row):
            headers = row.select(
                'td/b'
            ).extract() or row.select(
                'td/font[@class="h1"]'
            ).extract() or row.select(
                'th'
            ).extract()
            return len(headers) > 0
        
        def _is_next_link(row):
            next_link = row.select(
                'td/descendant::font[@class="cel_a_grey"]'
            ).extract()
            return len(next_link) > 0
        
        def _extract_next_url(row):
            next_url = row.select(
                'td[1]/a/@href'
            ).extract()
            if len(next_url) == 0:
                return None
            next_url = next_url[0]
            if 'isNext' not in next_url:
                return None
            return self.absolute_url(next_url)
        
        def _parse_rows_page(response):
            related_items = []
            rows = _select_rows(response)
            ignore_next = True
            for row in rows:
                if _is_header(row):
                    pass
                elif _is_next_link(row):
                    if ignore_next:
                        continue
                    next_url = _extract_next_url(row)
                    if next_url is None:
                        continue
                    request = Request(next_url, _parse_rows_page)
                    next_related_items = yield wait_for(request)
                    assert isinstance(next_related_items, list)
                    related_items.extend(next_related_items)
                else:
                    ignore_next = False
                    related_item = IsapRelatedDocumentItem()
                    related_item = _parse_row(row, related_item)
                    related_items.append(related_item)
            yield _result(related_items)
        
        related_items = yield wait_for(_parse_rows_page(response))
        assert isinstance(related_items, list)
        yield _result(related_items)
    
    def _is_relation_polish(self, item):
        return item['relation_type'] != u"dyrektywy europejskie"
    
    def _parse_relation_polish(self, response, with_date=True):
        
        def _select_main(response):
            hxs = HtmlXPathSelector(response)
            return hxs.select(
                '/html/body/table[2]'
            )
        
        def _select_rows(response):
            return _select_main(response).select(
                'tbody/tr'
            )
        
        def _parse_row(row, related_item):
            related_item['status'] = row.select(
                'td[2]/text()'
            ).extract()[0]
            if related_item['status'] == u'-':
                return None
            related_item['journal'] = 'polish'
            # FIXME: Use signature parsing routines from mrcerti.convert
            signature = row.select(
                'td[1]/a/text()'
            ).re(
                ur'^\s*Dz\.\s*U\.\s*[0-9]+\s*nr\s*[0-9]+\s*poz\.\s*[0-9]+',
            )
            # FIXME: This should be unconditional
            related_item['signature'] = signature[0] if signature else None
            if with_date:
                related_item['effective_date'] = row.select(
                    'td[1]/a/text()'
                ).re(
                    ur'[0-9]+\.[0-9]+\.[0-9]+\s*$',
                )[0]
            related_item['item_id'] = row.select(
                'td[1]/a/@href'
            ).re(
                ur'DetailsServlet(;jsessionid=[0-9A-Z]+)?\?id=([^&]+)'
            )[1]
            related_item['url'] = self.absolute_url(
                row.select('td[1]/a/@href').extract()[0]
            )
            related_item['title'] = row.select(
                'td[3]/p/text()'
            ).extract()[0]
            return related_item
        
        def _is_header(row):
            headers = row.select(
                'td/b'
            ).extract() or row.select(
                'td/font[@class="h1"]'
            ).extract()
            return len(headers) > 0
        
        def _is_next_link(row):
            next_link = row.select(
                'td/descendant::font[@class="cel_a_grey"]'
            ).extract()
            return len(next_link) > 0
        
        def _extract_next_url(row):
            next_url = row.select(
                'td[1]/a/@href'
            ).extract()
            if len(next_url) == 0:
                return None
            next_url = next_url[0]
            if 'isNext' not in next_url:
                return None
            return self.absolute_url(next_url)
        
        def _parse_rows_page(response):
            related_items = []
            rows = _select_rows(response)
            ignore_next = True
            for row in rows:
                if _is_header(row):
                    pass
                elif _is_next_link(row):
                    if ignore_next:
                        continue
                    next_url = _extract_next_url(row)
                    if next_url is None:
                        continue
                    request = Request(next_url, _parse_rows_page)
                    next_related_items = yield wait_for(request)
                    assert isinstance(next_related_items, list)
                    related_items.extend(next_related_items)
                else:
                    ignore_next = False
                    related_item = IsapRelatedDocumentItem()
                    related_item = _parse_row(row, related_item)
                    if related_item is not None:
                        related_items.append(related_item)
            yield _result(related_items)
        
        related_items = yield wait_for(_parse_rows_page(response))
        assert isinstance(related_items, list)
        yield _result(related_items)
    
    def set_crawler(self, crawler):
        super(IsapBaseSpider, self).set_crawler(crawler)
        self.follow_amendment = crawler.settings.getbool('FOLLOW_AMENDMENT', False)
