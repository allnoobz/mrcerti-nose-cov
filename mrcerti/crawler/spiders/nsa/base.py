# -*- coding: utf-8 -*-

from urlparse import urljoin, urlparse
from scrapy.http.request import Request
from scrapy.selector.lxmlsel import HtmlXPathSelector
from scrapy.spider import BaseSpider
from mrcerti.crawler.items.nsa import NsaDocumentItem, NsaRelationItem


class NsaBaseSpider(BaseSpider):
    
    name = 'base_spider'
    allowed_domains = ['orzeczenia.nsa.gov.pl']
    base_url = 'http://orzeczenia.nsa.gov.pl/'
    
    def absolute_url(self, relative):
        return urljoin(self.base_url, relative)
    
    def parse_document(self, response):
        def _extract_header(response):
            hxs = HtmlXPathSelector(response)
            headers = hxs.select(
                '//span[@class="war_header"]/text()'
            ).extract()
            if headers:
                return headers[0]
            else:
                # TODO: When does this happen?
                return None
        
        # FIXME: This should return None if document cannot be extracted for
        # whatever reason. Preferably though, this should raise an exception
        def _need_refresh(response):
            header = _extract_header(response)
            return header is None
        
        def _select_rows(response):
            hxs = HtmlXPathSelector(response)
            return hxs.select('//table[contains(@class, "info-list")]/tr')
        
        def _extract_row_caption(row):
            return row.select(
                'td//*[@class="lista-label"]/text()'
            ).extract()[0]
        
        def _extract_ruling_date(row):
            return row.select(
                'td[@class="info-list-value"]//tr/td[position() = 1]/text()'
            ).extract()[0].strip()
        
        def _extract_simple_data(row):
            return row.select(
                'td[@class="info-list-value"]/text()'
            ).extract()[0].strip()
        
        def _extract_judges(row):
            judges = row.select(
                'td[@class="info-list-value"]/text()'
            ).extract()
            return [t.strip() for t in judges]
        
        def _extract_related_rulings(row):
            related = row.select('td[@class="info-list-value"]//a')
            related_items = []
            for rel in related:
                rel_url = rel.select('@href').extract()[0]
                item_id = rel_url.split('/')[-1]
                signature = rel.select('text()').extract()[0].split(' - ')[0]
                related_item = NsaRelationItem()
                related_item['item_id'] = item_id
                related_item['signature'] = signature
                related_item['url'] = urljoin(self.base_url, rel_url)
                related_item['spider'] = 'nsa'
                related_items.append(related_item)
            return related_items
        
        def _extract_referenced_laws(row):
            parts = row.select(
                'td[@class="info-list-value"]//text()[not(parent::span)]'
            ).extract()[1:-1]
            parts = [p.strip() for p in parts]
            rules = []
            for i in range(0, len(parts), 2):
                rules.append(' '.join(parts[i:i+2]))
            return rules
        
        def _extract_text(row):
            return ''.join(
                row.select(
                    'td/span[@class="info-list-value-uzasadnienie"]//*'
                ).extract()
            )
        
        def _parse_basic_row(row, item):
            caption = _extract_row_caption(row)
            
            item['text'] = item['text'] or u""
            
            if caption == u'Data orzeczenia':
                item['ruling_date'] = _extract_ruling_date(row)
            elif caption == u'Data wpływu':
                item['filing_date'] = _extract_simple_data(row)
            elif caption == u'Sąd':
                item['court'] = _extract_simple_data(row)
            elif caption == u'Sędziowie':
                item['judges'] = _extract_judges(row)
            elif caption == u'Symbol z opisem':
                item['topic'] = _extract_simple_data(row)
            elif caption == u'Hasła tematyczne':
                item['keywords'] = _extract_simple_data(row)
            elif caption == u'Sygn. powiązane':
                item['related_rulings'] = _extract_related_rulings(row)
            elif caption == u'Skarżony organ':
                item['defendant'] = _extract_simple_data(row)
            elif caption == u'Treść wyniku':
                item['judgment'] = _extract_simple_data(row)
            elif caption == u'Powołane przepisy':
                item['legal_acts'] = _extract_referenced_laws(row)
            elif caption == u'Sentencja':
                item['text'] += _extract_text(row)
            elif caption == u'Uzasadnienie':
                item['text'] += _extract_text(row)
            elif caption == u'Tezy':
                item['text'] += _extract_text(row)
            elif caption == u'Zdanie odrębne':
                item['text'] += _extract_text(row)
            
            return item
            
        def _parse_rows(rows, item):
            item = item or {}
            for row in rows:
                item = _parse_basic_row(row, item)
            return item

        def _parse_info(response, item):
            item['item_id'] = urlparse(response.url).path.split('/')[-1]
            item['url'] = response.url
            item['spider'] = 'nsa'
            header = _extract_header(response)
            item['title'] = header
            item['signature'] = header.split(' - ')[0]
            rows = _select_rows(response)
            return _parse_rows(rows, item)
        
        if _need_refresh(response):
            return Request(response.url, callback=self.parse_document)
        
        item = NsaDocumentItem()
        item = _parse_info(response, item)
        return item
