# -*- coding: utf-8 -*-

from urlparse import urljoin
from scrapy.http.request import Request
from mrcerti.crawler.spiders.nsa.base import NsaBaseSpider


class NsaSingleDocumentSpider(NsaBaseSpider):
    
    name = 'single_document_spider'
    ruling_url = 'http://orzeczenia.nsa.gov.pl/doc/'

    def get_url(self):
        url = urljoin(self.ruling_url, self.item_id)
        return url

    def __init__(self, item_id=None, **kwargs):
        super(NsaSingleDocumentSpider, self).__init__(**kwargs)
        assert item_id is not None
        self.item_id = item_id
        self.start_url = self.get_url()
    
    def start_requests(self):
        return [Request(self.start_url, callback=self.parse_document)]
