# -*- coding: utf-8 -*-

import re
import urllib
from scrapy import log
from scrapy.http.request import Request
from scrapy.http.request.form import FormRequest
from scrapy.selector.lxmlsel import HtmlXPathSelector
from mrcerti.crawler.exceptions import PageParseException
from mrcerti.crawler.formdata.nsa import NsaFormDataSchema
from mrcerti.crawler.spiders.nsa.base import NsaBaseSpider


class NsaSearchSpider(NsaBaseSpider):
    
    name = 'search_spider'
    search_url = 'http://orzeczenia.nsa.gov.pl/cbo/query'
    page_url = 'http://orzeczenia.nsa.gov.pl/cbo/find'
    
    def __init__(self, **kwargs):
        super(NsaSearchSpider, self).__init__(**kwargs)
        self.form_data = NsaFormDataSchema().formdata(kwargs)

    def start_requests(self):
        return [Request(self.search_url, callback=self.parse_search_page)]
    
    def parse_search_page(self, response):
        request = FormRequest.from_response(
            response,
            formdata=self.form_data,
            callback=self.parse_search_result
        )
        request.meta['use_proxy'] = False
        return [request]
    
    def parse_result_page(self, response):
        def _select_page_data(response):
            hxs = HtmlXPathSelector(response)
            return hxs.select(
                '/html/body/div[@class="tac"]/'
                'div[@class="tal"]/div[@class="tab"]'
            )
        
        def _select_rulings(response):
            data = _select_page_data(response)
            return data.select('div[@id="res-div"]/table')
            
        def _extract_ruling_urls(response):
            rulings = _select_rulings(response)
            return rulings.select('tr[1]/td/a[1]/@href').extract()
        
        ruling_urls = _extract_ruling_urls(response)
        if len(ruling_urls) == 0:
            open_in_browser(response)
            raise PageParseException('Result page should has at least one result')
        
        ruling_requests = []
        for url in ruling_urls:
            if not re.match('^/doc/[0-9A-Z]+$', url):
                raise PageParseException('Ruling url "%s" does not match with '
                                         '"^/doc/[0-9A-Z]+$"' % url)
            url = self.absolute_url(url)
            ruling_requests.append(Request(url, callback=self.parse_document))
        return ruling_requests
    
    def parse_search_result(self, response):
        def _has_results(response):
            hxs = HtmlXPathSelector(response)
            no_results_found_node = hxs.select(
                '//form[@name="QueryForm"]/div[@class="warning"]/text()'
            ).extract()
            return not (
                len(no_results_found_node) > 0 and
                re.search(
                    u'Nie znaleziono orzeczeń spełniających podany warunek!',
                    no_results_found_node[0]
                )
            )
        
        def _extract_size_info(response):
            if _has_results(response):
                hxs = HtmlXPathSelector(response)
                info_node = hxs.select(
                    '//table[@class="top-linki"]/tr[1]/td[1]/text()'
                ).extract()[0]
                m = re.search(
                    u'Znaleziono (\d+) orzeczeń, Str. (\d+) z (\d+)',
                    info_node
                )
                
                page_count = int(m.group(3))
                ruling_count = int(m.group(1))
            else:
                page_count = 0
                ruling_count = 0
            return page_count, ruling_count
        
        def _get_page_requests(page_count):
            requests = []
            for p in range(1, page_count+1):
                urlparams = dict()
                urlparams['p'] = str(p)
                urlparams = urllib.urlencode(urlparams)
                url = '%s?%s' % (self.page_url, urlparams)
                requests.append(Request(url, self.parse_result_page))
            return requests
        
        page_count, ruling_count = _extract_size_info(response)
        self.log(
            'Found %d pages, with %d rulings' % (page_count, ruling_count),
            log.INFO
        )
        if page_count == 0 or ruling_count == 0:
            open_in_browser(response)
        return _get_page_requests(page_count)
    