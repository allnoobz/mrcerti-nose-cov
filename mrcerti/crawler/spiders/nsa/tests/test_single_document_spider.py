# -*- coding: utf-8 -*-

import datetime
from mrcerti.crawler.spiders.nsa.single_document import NsaSingleDocumentSpider
from mrcerti.crawler.spiders.nsa.tests.nsa_spider_test_case import NsaSpiderTestCase


class TestSingleDocumentSpider(NsaSpiderTestCase):

    def test_crawling(self):
        ident = 'F5003F690B'
        spider = NsaSingleDocumentSpider(ident)
        items = self.run_spider(spider)
        self.assertEqual(len(items), 1, 'one document should be processed')
        item = items[0]
        self.assertEqual(item['item_id'], 'F5003F690B')
        self.assertEqual(item['url'], 'http://orzeczenia.nsa.gov.pl/doc/F5003F690B')
        self.assertEqual(item['title'], u'I SA/Wa 1189/11 - Wyrok WSA w Warszawie')
        self.assertEqual(item['topic'], u'6079 Inne o symbolu podstawowym 607')
        self.assertEqual(item['spider'], u'nsa')
        self.assertEqual(item['signature'], u'I SA/Wa 1189/11')
        self.assertEqual(item['ruling_date'], '2011.11.03')
        self.assertEqual(item['judgment'], u'Uchylono decyzję I i II instancji')
        self.assertEqual(item['filing_date'], '2011.06.27')
        self.assertEqual(item['keywords'], u'Nieruchomości')
        self.assertEqual(item['court'], u'Wojewódzki Sąd Administracyjny w Warszawie')
        self.assertItemsEqual(
            item['judges'], 
            [
                u'Agnieszka Miernik /przewodniczący/',
                u'Joanna Skiba /sprawozdawca/',
                u'Małgorzata Boniecka-Płaczkowska'
            ],
        )
        self.assertEqual(item['defendant'], u'Samorządowe Kolegium Odwoławcze')
        self.assertListEqual(
            item['referenced_laws'],
            [
                u'Dz.U. 2002 nr 153 poz 1270 art. 145 par. 1 pkt 1 lit. a i c',
                u'Dz.U. 2000 nr 98 poz 1071 art. 79 ust. 3, art. 105 par. 1, art. 127 par. 3, art. 127-144',
                u'Dz.U. 2010 nr 102 poz 651 art. 80',
            ]
        )
        
        self.assertIsNotNone(item['text'])
        self.assertTrue(
            item['text'].startswith(
                u'<p>Wojewódzki Sąd Administracyjny w Warszawie w składzie'
            )
        )

    def test_minimal_crawling(self):
        ident = 'EBD7669EB8'
        spider = NsaSingleDocumentSpider(ident)
        items = self.run_spider(spider)
        self.assertEqual(len(items), 1, 'one document should be processed')
        item = items[0]
        self.assertEqual(item['item_id'], ident)
        self.assertEqual(
            item['url'],
            'http://orzeczenia.nsa.gov.pl/doc/%s' % ident
        )
        self.assertEqual(item['title'], u'SA 109/81 - Wyrok NSA')
        self.assertEqual(
            item['topic'],
            u'604  Działalność gospodarcza, w tym z udziałem  '
            u'podmiotów zagranicznych'
        )
        self.assertEqual(item['spider'], u'nsa')
        self.assertEqual(item['signature'], u'SA 109/81')
        self.assertEqual(item['ruling_date'], '1981.02.23')
        self.assertEqual(item['court'], u'NSA w Warszawie (przed reformą)')
        self.assertListEqual(
            item['referenced_laws'],
            [u'Dz.U. 1980 nr 9 poz. 26 art. 156 par. 1 pkt 2, art. 104']
        )

        self.assertTrue(
            item['text'].startswith(
                u'<p>Decyzja administracyjna wydana w sprawie, której załatwienia '
                u'w formie decyzji przepisy nie przewidują, podlega stwierdzeniu '
                u'nieważności, jako wydana bez podstawy prawnej. </p>'
            ),
            "Text '%s...' doesn't start appropriately" % item['text'][:50],
        )

        self.assertFalse('judgment' in item)

        self.assertIsNone(item['filing_date'])
        self.assertIsNone(item['keywords'])
        self.assertIsNone(item['judges'])
        self.assertIsNone(item['defendant'])
