# -*- coding: utf-8 -*-

import cookielib
import os
import pickle
import urllib2
from urlparse import parse_qs
from unittest import TestCase
from scrapy.http import TextResponse
from scrapy.utils.project import get_project_settings
from mrcerti.crawler.formdata.nsa import NsaFormDataSchema
from mrcerti.crawler.spiders.nsa.single_document import NsaSingleDocumentSpider
from mrcerti.crawler.spiders.nsa.search import NsaSearchSpider


def load_responses(test, ids=None, urls=None, fresh=True, **kwargs):
    # Kwargs should be used to distinguish the requests to
    # pages with same URLs but eg. with different sessions.
    ids = ids or []
    urls = urls or []
    ruling_url = NsaSingleDocumentSpider.ruling_url
    path = 'responses/nsa'
    redownload = test.settings.get("TEST_RESPONSE_REDOWNLOAD", False)

    responses = []
    requests = []
    for ident in ids:
        save_path = '%s/%s.pickle' % (path, ident)
        if (not fresh or not redownload) and os.path.exists(save_path):
            with open(save_path, 'rb') as f:
                responses.append(pickle.load(f))
        else:
            url = "%s%s" % (ruling_url, ident)
            requests.append((save_path, url))

    for url in urls:
        save_path = '%s/%s.pickle' % (path, hash((url, unicode(kwargs))))
        if (not fresh or not redownload) and os.path.exists(save_path):
            with open(save_path, 'rb') as f:
                responses.append(pickle.load(f))
        else:
            requests.append((save_path, url))

    for (save_path, url) in requests:
        print "Downloading file %s from address %s." % (save_path, url)
        _response = test.opener.open(url)
        body = _response.read()
        response = TextResponse(url, body=body)
        with open(save_path, 'wb') as f:
            pickle.dump(response, f)
        responses.append(response)

    return responses


class TestSingleDocumentNsaSpider(TestCase):

    def setUp(self):
        self.shim_spider = NsaSingleDocumentSpider(item_id='')
        self.settings = get_project_settings()
        super(TestSingleDocumentNsaSpider, self).setUp()
        self.cj = cookielib.CookieJar()
        self.opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(self.cj))
        self.opener.addheaders = \
            [('User-agent', 'Mozilla/5.0 (Windows; U;'
                            ' Windows NT 5.1; it; rv:1.8.1.11) Gecko/20071127 Firefox/2.0.0.11')]

    def test_url(self):
        item_id = '1234'
        spider = NsaSingleDocumentSpider(item_id=item_id)
        self.assertEqual(spider.start_url, NsaSingleDocumentSpider.ruling_url + item_id)
        self.assertEqual(spider.item_id, item_id)

    def test_crawling(self):
        ident = 'F5003F690B'
        responses = load_responses(self, [ident])
        self.assertEqual(len(responses), 1)
        item = self.shim_spider.parse_document(responses[0])
        self.assertIsNotNone(item)
        self.assertEqual(item['item_id'], 'F5003F690B')
        self.assertEqual(item['url'], 'http://orzeczenia.nsa.gov.pl/doc/F5003F690B')
        self.assertEqual(item['title'], u'I SA/Wa 1189/11 - Wyrok WSA w Warszawie')
        self.assertEqual(item['topic'], u'6079 Inne o symbolu podstawowym 607')
        self.assertEqual(item['spider'], u'nsa')
        self.assertEqual(item['signature'], u'I SA/Wa 1189/11')
        self.assertEqual(item['ruling_date'], '2011.11.03')
        self.assertEqual(item['judgment'], u'Uchylono decyzję I i II instancji')
        self.assertEqual(item['filing_date'], '2011.06.27')
        self.assertEqual(item['keywords'], u'Nieruchomości')
        self.assertEqual(item['court'], u'Wojewódzki Sąd Administracyjny w Warszawie')
        self.assertItemsEqual(
            item['judges'],
            [
                u'Agnieszka Miernik /przewodniczący/',
                u'Joanna Skiba /sprawozdawca/',
                u'Małgorzata Boniecka-Płaczkowska'
            ],
        )
        self.assertEqual(item['defendant'], u'Samorządowe Kolegium Odwoławcze')
        self.assertListEqual(
            item['referenced_laws'],
            [
                u'Dz.U. 2002 nr 153 poz 1270 art. 145 par. 1 pkt 1 lit. a i c',
                u'Dz.U. 2000 nr 98 poz 1071 art. 79 ust. 3, art. 105 par. 1, art. 127 par. 3, art. 127-144',
                u'Dz.U. 2010 nr 102 poz 651 art. 80',
            ]
        )

        self.assertIsNotNone(item['text'])
        self.assertTrue(
            item['text'].startswith(
                u'<p>Wojewódzki Sąd Administracyjny w Warszawie w składzie'
            )
        )


class TestSearchNsaSpider(TestCase):

    def setUp(self):
        self.shim_spider = NsaSingleDocumentSpider(item_id='')
        self.settings = get_project_settings()
        super(TestSearchNsaSpider, self).setUp()
        self.cj = cookielib.CookieJar()
        self.opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(self.cj))
        self.opener.addheaders = \
            [('User-agent', 'Mozilla/5.0 (Windows; U;'
                            ' Windows NT 5.1; it; rv:1.8.1.11) Gecko/20071127 Firefox/2.0.0.11')]

    def _process_search(self, form_data, spider):
        request = spider.start_requests()[0]
        self.assertEqual(request.url, NsaSearchSpider.search_url)

        # DO NOT forget about the additional parameter!
        response = load_responses(self, urls=[request.url], form_data=form_data)[0]
        form_request = spider.parse_search_page(response)[0]
        form_request_data = parse_qs(form_request.body)
        for (key, value) in NsaFormDataSchema().formdata(form_data).items():
            if value:
                self.assertEqual(value, form_request_data[key][0].decode('utf-8'))
        form_url = "%s?%s" % (form_request.url, form_request.body)
        # DO NOT forget about the additional parameter!
        form_response = load_responses(self, urls=[form_url], form_data=form_data)[0]
        page_urls = []
        for page_request in spider.parse_search_result(form_response):
            page_urls.append(page_request.url)
        # DO NOT forget about the additional parameter!
        page_responses = load_responses(self, urls=page_urls, form_data=form_data)
        document_requests = map(spider.parse_result_page, page_responses)[0]
        document_urls = [request.url for request in document_requests]
        document_responses = load_responses(self, urls=document_urls)
        return document_responses

    def test_search_single(self):
        form_data = {
            'signature': 'I SA/Wa 1189/11',
        }
        spider = NsaSearchSpider(
            signature='I SA/Wa 1189/11'
        )
        document_responses = self._process_search(form_data, spider)
        self.assertEqual(len(document_responses), 1)
        document_response = document_responses[0]
        document = spider.parse_document(document_response)
        signature = document['signature']
        self.assertEqual(signature, form_data['signature'])

    def test_search_multiple(self):
        form_data = {
            'phrase': 'frytki',
            'court': u'Wojewódzki Sąd Administracyjny w Warszawie'
        }
        spider = NsaSearchSpider(
            phrase='frytki',
            court=u'Wojewódzki Sąd Administracyjny w Warszawie',
        )
        document_responses = self._process_search(form_data, spider)
        documents = map(spider.parse_document, document_responses)
        document_ids = {doc['item_id'] for doc in documents}
        ids = set([
            '889C12E2B4', '7F1E2BE82B', '671DB2F5B1',
            '1B4CE25BA7', '6F4A2FF287', 'AB710BFDC2'
        ])
        self.assertSetEqual(ids, document_ids)
