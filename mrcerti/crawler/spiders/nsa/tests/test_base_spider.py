# -*- coding: utf-8 -*-

import datetime
from scrapy.http.request import Request
from scrapy.http.response.html import HtmlResponse
from mrcerti.crawler.items.nsa import NsaDocumentItem
from mrcerti.crawler.spiders.nsa.base import NsaBaseSpider
from mrcerti.crawler.spiders.nsa.tests.nsa_spider_test_case import \
    NsaSpiderTestCase


class TestSearchSpider(NsaSpiderTestCase):
    
    def test_parse_document(self):
        spider = NsaBaseSpider()
        
        with open(self._resource_path('document_page_need_refresh.html')) as f:
            need_refresh = f.read()
            response = HtmlResponse('http://orzeczenia.nsa.gov.pl/doc/0DFDC7A6A3', body = need_refresh)
            result = spider.parse_document(response)
            self.assertIsInstance(result, Request)
            self.assertEqual('http://orzeczenia.nsa.gov.pl/doc/0DFDC7A6A3', result.url)
        
        with open(self._resource_path('document_page_ok_1.html')) as f:
            need_refresh = f.read()
            response = HtmlResponse('http://orzeczenia.nsa.gov.pl/doc/D97043CF84', body = need_refresh)
            result = spider.parse_document(response)
            self.assertIsInstance(result, NsaDocumentItem)
            self.assertEqual(result['item_id'], 'D97043CF84')
            self.assertEqual(result['url'], 'http://orzeczenia.nsa.gov.pl/doc/D97043CF84')
            self.assertEqual(result['spider'], 'nsa')
            self.assertEqual(result['title'], u'II GSK 89/11 - Wyrok NSA')
            self.assertEqual(result['signature'], u'II GSK 89/11')
            self.assertEqual(result['ruling_date'], '2013.09.18')
            self.assertEqual(result['filing_date'], '2011.01.17')
            self.assertEqual(result['court'], u'Naczelny Sąd Administracyjny')
            self.assertListEqual(
                result['judges'],
                [
                    u'Małgorzata Rysz',
                    u'Stanisław Gronowski /przewodniczący sprawozdawca/',
                    u'Wojciech Kręcisz'
                ]
            )
            self.assertEqual(result['keywords'], None)
            self.assertEqual(result['topic'],  u'6042 Gry losowe i zakłady wzajemne')
            self.assertListEqual(
                result['related_rulings'],
                [
                    {
                        'item_id': u'8045E1FBA7',
                        'signature': u'II SA/Ol 624/10',
                        'spider': 'nsa',
                        'url': u'http://orzeczenia.nsa.gov.pl/doc/8045E1FBA7'
                    }
                ]
            )
            self.assertEqual(result['referenced_laws'], None)
            self.assertEqual(result['defendant'], u'Dyrektor Izby Celnej')
            self.assertEqual(result['judgment'], u'Uchylono zaskarżony wyrok oraz decyzję I i II instancji')
            self.assertEqual(len(result['text']), 1013 + 14083)
            self.assertEqual(
                result['text'][100:130],
                u'zia NSA Małgorzata Rysz Sędzia'
            )
            self.assertEqual(
                result['text'][1013 + 100:1013 + 130],
                u'A/Ol 624/10, wydanego w sprawi'
            )
        
        with open(self._resource_path('document_page_ok_2.html')) as f:
            need_refresh = f.read()
            response = HtmlResponse('http://orzeczenia.nsa.gov.pl/doc/B036F17CF8', body = need_refresh)
            result = spider.parse_document(response)
            self.assertIsInstance(result, NsaDocumentItem)
            self.assertEqual(result['item_id'], 'B036F17CF8')
            self.assertEqual(result['url'], 'http://orzeczenia.nsa.gov.pl/doc/B036F17CF8')
            self.assertEqual(result['spider'], 'nsa')
            self.assertEqual(result['title'], u'I FPS 6/12 - Uchwała NSA')
            self.assertEqual(result['signature'], u'I FPS 6/12')
            self.assertEqual(result['ruling_date'], '2013.06.03')
            self.assertEqual(result['filing_date'], '2012.10.16')
            self.assertEqual(result['court'], u'Naczelny Sąd Administracyjny')
            self.assertListEqual(
                result['judges'],
                [
                    u'Marek Zirk-Sadowski /przewodniczący/',
                    u'Grażyna Jarmasz',
                    u'Roman Wiatrowski (sprawozdawca) /autor uzasadnienia/',
                    u'Adam Bącal',
                    u'Barbara Wasilewska',
                    u'Hieronim Sęk',
                    u'Arkadiusz Cudak /zdanie odrebne/'
                ]
            )
            self.assertEqual(result['keywords'], u'Podatek od towarów i usług')
            self.assertEqual(result['topic'],  u'6110 Podatek od towarów i usług')
            self.assertEqual(result['related_rulings'], None)
            self.assertEqual(
                result['referenced_laws'],
                [
                    u'Dz.U. 2005 nr 8 poz 60 art. 70 par. 4'
                ]
            )
            self.assertEqual(result['defendant'], u'Dyrektor Izby Skarbowej')
            self.assertEqual(result['judgment'], u'Podjęto uchwałę')
            self.assertEqual(len(result['text']), 351 + 1828 + 60784 + 10362)
            self.assertEqual(
                result['text'][100:130],
                u'tawy z dnia 29 sierpnia 1997 r'
            )
            self.assertEqual(
                result['text'][351 + 1828 + 60784 + 100:351 + 1828 + 60784 + 130],
                u'ię z treścią uchwały oraz z je'
            )
            self.assertEqual(
                result['text'][351 + 100:351 + 130],
                u'i, Sędzia NSA Adam Bącal, Sędz'
            )
            self.assertEqual(
                result['text'][351 + 1828 + 100:351 + 1828 + 130],
                u' dnia 16 października 2012 r.,'
            )
    