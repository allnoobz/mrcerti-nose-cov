# -*- coding: utf-8 -*-

import os
from mrcerti.crawler.settings.crawler import nsa
from mrcerti.crawler.spiders.tests.spider_test_case import (
    SpiderTestCaseWithScrapyObserver,
)


class NsaSpiderTestCase(SpiderTestCaseWithScrapyObserver):
    
    def _resource_path(self, name):
        base = os.path.normpath('mrcerti/crawler/spiders/nsa/tests/resources')
        return os.path.join(base, name)
    
    def setUp(self):
        super(NsaSpiderTestCase, self).setUp()
        assert self.settings
        self.settings.update(
            {                
                'ITEM_PIPELINES': nsa.ITEM_PIPELINES,
                'PROXY_ENABLED': nsa.PROXY_ENABLED,
                'DOWNLOADER_MIDDLEWARES': nsa.DOWNLOADER_MIDDLEWARES,
                'SPIDER_MIDDLEWARES': nsa.SPIDER_MIDDLEWARES,
                'RETRY_ENABLED': nsa.RETRY_ENABLED,
                'RETRY_TIMES': nsa.RETRY_TIMES,
                'RETRY_HTTP_CODES': nsa.RETRY_HTTP_CODES,
                'AWS_REGION': nsa.AWS_REGION,
                'AWS_ACCESS_KEY_ID': nsa.AWS_ACCESS_KEY_ID,
                'AWS_SECRET_ACCESS_KEY': nsa.AWS_SECRET_ACCESS_KEY,
             }
        )
