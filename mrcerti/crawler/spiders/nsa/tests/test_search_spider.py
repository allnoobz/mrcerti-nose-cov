# -*- coding: utf-8 -*-

from scrapy.http.response.html import HtmlResponse
from mrcerti.crawler.spiders.nsa.search import NsaSearchSpider
from mrcerti.crawler.spiders.nsa.tests.nsa_spider_test_case import \
    NsaSpiderTestCase


class TestSearchSpider(NsaSpiderTestCase):

    def __init__(self, *args, **kwargs):
        kwargs['fname'] = 'nsa_test_search_spider'
        super(TestSearchSpider, self).__init__(*args, **kwargs)

    def test_crawl_single_page(self):
        spider = NsaSearchSpider(
            phrase='frytki',
            court= u'Wojewódzki Sąd Administracyjny w Warszawie',
        )
        items = self.run_spider(spider)
        expected_items = 4
        self.assertGreaterEqual(len(items), expected_items, "Processed item number should match.")
        item_ids = {
            item['item_id'] 
            for item in items
        }
        self.assertEqual(len(item_ids), len(items))
        self.assertItemsEqual([
                '889C12E2B4', '7F1E2BE82B', '671DB2F5B1',
                '1B4CE25BA7', '6F4A2FF287', 'AB710BFDC2'
            ], 
            item_ids,
        )
    
    def test_parse_search_result(self):
        spider = NsaSearchSpider()
        
        with open(self._resource_path('result_page_empty.html')) as f:
            no_results = f.read()
            response = HtmlResponse('http://orzeczenia.nsa.gov.pl/cbo/search', body = no_results)
            results = spider.parse_search_result(response)
            urls = [result.url for result in results]
            self.assertListEqual([], urls)
        
        with open(self._resource_path('result_page_single.html')) as f:
            single_result_page = f.read()
            response = HtmlResponse('http://orzeczenia.nsa.gov.pl/cbo/search', body = single_result_page)
            results = spider.parse_search_result(response)
            urls = [result.url for result in results]
            self.assertListEqual([
                'http://orzeczenia.nsa.gov.pl/cbo/find?p=1'
            ], urls)
        
        with open(self._resource_path('result_page_multiple_1.html')) as f:
            multiple_result_pages = f.read()
            response = HtmlResponse('http://orzeczenia.nsa.gov.pl/cbo/search', body = multiple_result_pages)
            results = spider.parse_search_result(response)
            urls = [result.url for result in results]
            self.assertListEqual([
                'http://orzeczenia.nsa.gov.pl/cbo/find?p=1',
                'http://orzeczenia.nsa.gov.pl/cbo/find?p=2',
                'http://orzeczenia.nsa.gov.pl/cbo/find?p=3',
                'http://orzeczenia.nsa.gov.pl/cbo/find?p=4',
            ], urls)
    
    def test_parse_result_page(self):
        spider = NsaSearchSpider()
        
        with open(self._resource_path('result_page_single.html')) as f:
            single_result_page = f.read()
            response = HtmlResponse('http://orzeczenia.nsa.gov.pl/cbo/search', body = single_result_page)
            results = spider.parse_result_page(response)
            urls = [result.url for result in results]
            self.assertListEqual([
                'http://orzeczenia.nsa.gov.pl/doc/889C12E2B4',
                'http://orzeczenia.nsa.gov.pl/doc/671DB2F5B1',
                'http://orzeczenia.nsa.gov.pl/doc/1B4CE25BA7'
            ], urls)
        
        with open(self._resource_path('result_page_multiple_1.html')) as f:
            multiple_result_pages = f.read()
            response = HtmlResponse('http://orzeczenia.nsa.gov.pl/cbo/search', body = multiple_result_pages)
            results = spider.parse_result_page(response)
            urls = [result.url for result in results]
            self.assertListEqual([
                'http://orzeczenia.nsa.gov.pl/doc/9040C02D5D', 
                'http://orzeczenia.nsa.gov.pl/doc/047A903AB7', 
                'http://orzeczenia.nsa.gov.pl/doc/B3638AA32F', 
                'http://orzeczenia.nsa.gov.pl/doc/523EFFD003', 
                'http://orzeczenia.nsa.gov.pl/doc/A8C31A6DFB', 
                'http://orzeczenia.nsa.gov.pl/doc/CDA0D19D9A', 
                'http://orzeczenia.nsa.gov.pl/doc/374E334F53', 
                'http://orzeczenia.nsa.gov.pl/doc/DAEA59E484', 
                'http://orzeczenia.nsa.gov.pl/doc/6498F5E7BF', 
                'http://orzeczenia.nsa.gov.pl/doc/7CA935D814'
            ], urls)
        
        with open(self._resource_path('result_page_multiple_2.html')) as f:
            multiple_result_pages = f.read()
            response = HtmlResponse('http://orzeczenia.nsa.gov.pl/cbo/search', body = multiple_result_pages)
            results = spider.parse_result_page(response)
            urls = [result.url for result in results]
            self.assertListEqual([
                'http://orzeczenia.nsa.gov.pl/doc/7628207F16',
                'http://orzeczenia.nsa.gov.pl/doc/86A23EB506',
                'http://orzeczenia.nsa.gov.pl/doc/5910CE85EF',
                'http://orzeczenia.nsa.gov.pl/doc/C0C638D79F',
                'http://orzeczenia.nsa.gov.pl/doc/91431817A3',
                'http://orzeczenia.nsa.gov.pl/doc/6390C33969',
                'http://orzeczenia.nsa.gov.pl/doc/B96B9E6505',
                'http://orzeczenia.nsa.gov.pl/doc/E6F62D8D7C',
                'http://orzeczenia.nsa.gov.pl/doc/77BE2DD6AF',
                'http://orzeczenia.nsa.gov.pl/doc/AB47F2FC3B'
            ], urls)
