# -*- coding: utf-8 -*-

import datetime
import os
import pickle
from unittest import TestCase
from urllib2 import urlopen
from urllib import urlencode
from scrapy.http import Response
from scrapy.utils.project import get_project_settings
from mrcerti.crawler.settings.crawler import sejmometr
from mrcerti.crawler.items.nsa import NsaDocumentItem
from mrcerti.crawler.spiders.sejmometr.base import SejmometrSpider
from mrcerti.crawler.spiders.tests.spider_test_case import SpiderTestCase


def _test_single_document(test, items, document):
    for item in items:
        test.assertIsInstance(item, NsaDocumentItem)
    expected_items = 1
    test.assertGreaterEqual(len(items), expected_items)
    item = items[0]
    test.assertEqual(item['spider'], document['spider'])
    test.assertEqual(item['title'], document['title'])
    test.assertEqual(item['signature'], document['signature'])
    test.assertEqual(item['defendant'], document['defendant'])
    test.assertEqual(item['court'], document['court'])
    test.assertEqual(item['judges'], document['judges'])
    test.assertEqual(item['judgment'], document['judgment'])
    test.assertEqual(item['keywords'], document['keywords'])
    test.assertEqual(item['filing_date'], document['filing_date'])
    test.assertEqual(item['ruling_date'], document['ruling_date'])
    test.assertIsNotNone(item['text'])
    test.assertTrue(item['text'].startswith(document['text']))


class SejmometrSpiderCrawlingTestCase(SpiderTestCase):
    
    def setUp(self):
        super(SejmometrSpiderCrawlingTestCase, self).setUp()
        assert self.settings
        self.settings.update(
            {
                'WORKERS_ENABLED': sejmometr.WORKERS_ENABLED,
                'PROCESSORS': sejmometr.PROCESSORS,
                'ITEM_PIPELINES': sejmometr.ITEM_PIPELINES,
            }
        )

    def test_single_document(self):
        spider = SejmometrSpider(
            dataset='sa_orzeczenia',
            f_sygnatura='I SA/Gd 1275/12',
        )
        document = {
            'spider': 'sejmometr',
            'title': u'I SA/Gd 1275/12 - Wyrok WSA w Gdańsku',
            'signature': u'I SA/Gd 1275/12',
            'defendant': u'Samorządowe Kolegium Odwoławcze',
            'court': u'Wojewódzki Sąd Administracyjny w Gdańsku',
            'judges': [u'370', u'497', u'504'],
            'judgment': u'Uchylono zaskarżoną decyzję',
            'keywords': u'Podatek od nieruchomości',
            'filing_date': '2012.11.23',
            'ruling_date': '2013.02.05',
            'text': u'<p>Wojewódzki Sąd Administracyjny w Gdańsku w składzie następującym'
        }
        items = self.run_spider(spider)
        _test_single_document(self, items, document)

    def test_multiple_documents(self):
        sad_dopelniacz = 'WSA w Gda\u0144sku'
        limit = 1
        page = 2
        expected_items = 19  # one item doesn't have a signature
        spider = SejmometrSpider(
            dataset='sa_orzeczenia',
            f_sad_dopelniacz=sad_dopelniacz,
            limit=limit,
            start=page
        )
        items = self.run_spider(spider)
        for item in items:
            self.assertIsInstance(item, NsaDocumentItem)
        self.assertGreaterEqual(len(items), expected_items, "Processed item number should match.")
        for item in items:
            self.assertTrue(item['court'], sad_dopelniacz)

    def test_empty_signature(self):
        limit = 1
        page = 3
        expected_items = 18
        spider = SejmometrSpider(
            dataset='sa_orzeczenia',
            limit=limit,
            start=page
        )
        items = self.run_spider(spider)
        for item in items:
            self.assertIsInstance(item, NsaDocumentItem)
        self.assertLessEqual(len(items), expected_items, "Processed item number should match.")

    def test_nullable(self):
        sad_dopelniacz = 'WSA w Gda\u0144sku'
        limit = 10
        page = 2
        spider = SejmometrSpider(
            dataset='sa_orzeczenia',
            f_sad_dopelniacz=sad_dopelniacz,
            limit=limit,
            start=page
        )
        items = self.run_spider(spider)
        for item in items:
            for field in item.fields:
                if not getattr(item.fields[field], '_nullable'):
                    self.assertIsNotNone(item.__getitem__(field))


class SejmometrSpiderShimTest(TestCase):
    def setUp(self):
        self.shim_spider = SejmometrSpider(dataset='sa_orzeczenia')
        self.settings = get_project_settings()

    def load_responses(self, start=1, limit=10, fresh=True, **kwargs):
        api_url = SejmometrSpider.api_url
        path = 'responses'
        redownload = self.settings.get("TEST_RESPONSE_DOWNLOAD", False)

        parameters = dict()
        for key, value in kwargs.items():
            parameters["filter.%s" % key] = value

        responses = []
        for page in xrange(start, start + limit):
            save_path = '%s/%s-%d.pickle' % (path, hash(str(kwargs)), page)
            if (not fresh or not redownload) and os.path.exists(save_path):
                with open(save_path, 'rb') as f:
                    responses.append(pickle.load(f))
            else:
                parameters['page'] = page
                params = urlencode(parameters)
                url = "%s/sa_orzeczenia.php?%s" % (api_url, params)
                print "Downloading file %s from address %s." % (save_path, url)
                _response = urlopen(url, timeout=30)
                body = _response.read()
                response = Response(url, body=body)
                with open(save_path, 'wb') as f:
                    pickle.dump(response, f)
                responses.append(response)
        return responses

    def test_single_document(self):
        response = self.load_responses(
            sygnatura='I SA/Gd 1275/12',
        )[0]
        document = {
            'spider': 'sejmometr',
            'title': u'I SA/Gd 1275/12 - Wyrok WSA w Gdańsku',
            'signature': u'I SA/Gd 1275/12',
            'defendant': u'Samorządowe Kolegium Odwoławcze',
            'court': u'Wojewódzki Sąd Administracyjny w Gdańsku',
            'judges': [u'370', u'497', u'504'],
            'judgment': u'Uchylono zaskarżoną decyzję',
            'keywords': u'Podatek od nieruchomości',
            'filing_date': '2012.11.23',
            'ruling_date': '2013.02.05',
            'text': u'<p>Wojewódzki Sąd Administracyjny w Gdańsku w składzie następującym'
        }
        items = list(self.shim_spider.parse_ruling(response))
        _test_single_document(self, items, document)

    def test_multiple_documents(self):
        sad_dopelniacz = 'WSA w Gda\u0144sku'
        limit = 1
        page = 2
        expected_items = 19  # one item doesnt have a signature
        responses = self.load_responses(
            sad_dopelniacz=sad_dopelniacz,
            limit=limit,
            start=page
        )
        items = []
        for response in responses:
            items.extend(self.shim_spider.parse_ruling(response))
        for item in items:
            self.assertIsInstance(item, NsaDocumentItem)
        self.assertGreaterEqual(len(items), expected_items, "Processed item number should match.")
        for item in items:
            self.assertTrue(item['court'], sad_dopelniacz)

    def test_empty_signature(self):
        limit = 1
        page = 3
        expected_items = 18
        response = self.load_responses(
            limit=limit,
            start=page
        )[0]
        items = list(self.shim_spider.parse_ruling(response))
        for item in items:
            self.assertIsInstance(item, NsaDocumentItem)
        self.assertLessEqual(len(items), expected_items, "Processed item number should match.")

    def test_nullable(self):
        sad_dopelniacz = 'WSA w Gda\u0144sku'
        limit = 10
        page = 2
        responses = self.load_responses(
            sad_dopelniacz=sad_dopelniacz,
            limit=limit,
            start=page
        )
        items = []
        for response in responses:
            items.extend(self.shim_spider.parse_ruling(response))
        response = responses[0]
        items = list(self.shim_spider.parse_ruling(response))
        for item in items:
            for field in item.fields:
                if not getattr(item.fields[field], '_nullable'):
                    self.assertIsNotNone(item.__getitem__(field))
