# -*- coding: utf-8 -*-

from scrapy.spider import BaseSpider
import urlparse
import urllib
import json
import re
from scrapy.selector.lxmlsel import XmlXPathSelector
from mrcerti.crawler.items.nsa import NsaDocumentItem
from scrapy.http.request import Request


class SejmometrSpider(BaseSpider):
    name = 'sejmometr_spider'  
    api_url = 'http://epf-wrapper.herokuapp.com'
    start_urls = []
    required_types = {
        'sygnatura',
        'typ_str',
        'sad_dopelniacz',
        'data_orzeczenia',
        'data_wplywu',
        'sad_nazwa',
        'skarzony_organ_str',
        'wynik_str',
    }
    # Maximal number of pages downloaded during one crawling session.
    limit = 4000
    # The first page to be downloaded.
    start = 1
    
    # Delay between requests sent to the API. Too many concurrent requests block the server.
    download_delay = 7
    
    def parse_ruling(self, response):
                
        def get_element_text(element):
            text = element.select('div[@class="inner"]/node()').extract()[0]
            text = text.replace('\n', '')
            return text       
        
        def extract_extended_content(document, item):
            text = u'<root>%s</root>' % document['layers']['html']
            selector = XmlXPathSelector(text=text)
            content = selector.select('/root/div[@class="nsa_box"]')
            item['text'] = u""
            for element in content:
                elem_text = get_element_text(element) 
                item['text'] += elem_text
            return item

        def find_missing_fields(document):
            fields = ['id', 'data']
            for field in fields:
                if field not in document or not document[field]:
                    return field
            data = document['data']
            for type_ in self.required_types:
                if type_ not in data or not data[type_]:
                    return type_
            return None

        def extract_content(document, item):
            def remove_xml_tags(text):
                return re.sub('<[^>]*>', '', text)

            # Sejmometr API documents sometimes might be faulty
            # and not contain fields such as signature or judges.
            # Since we don't want them to cause trouble
            # in the later stage of processing, drop them as soon as possible.
            field_missing = find_missing_fields(document)
            if field_missing:
                self.log("Document %s dropped due to a missing field %s." % (document, field_missing))
                return None

            item['item_id'] = str(document['id'])
            item['spider'] = 'sejmometr'
            item['url'] = response.url

            item['signature'] = document['data']['sygnatura']
            item['title'] = '%s - %s %s' % (document['data']['sygnatura'],
                                            document['data']['typ_str'],
                                            document['data']['sad_dopelniacz'])
            item['ruling_date'] = document['data']['data_orzeczenia'].split(' ')[0]
            item['filing_date'] = document['data']['data_wplywu'].split(' ')[0]
            item['court'] = document['data']['sad_nazwa']
            item['defendant'] = document['data']['skarzony_organ_str']
            item['judgment'] = remove_xml_tags(document['data']['wynik_str'])
            if 'hasla_str' in document['data']:
                item['keywords'] = remove_xml_tags(document['data']['hasla_str'])
            if 'sedzia_id' in document['data']:
                item['judges'] = document['data']['sedzia_id']
            
            return extract_extended_content(document, item)        
                
        def parse_document(document, source):
            item = NsaDocumentItem()
            item['url'] = source
            return extract_content(document, item)
        
        documents = json.loads(response.body)
        source = response.url

        for document in documents:
            item = parse_document(document, source)
            if item is not None:
                yield item
    
    def parse_ruling_pagination(self, response):
        data = json.loads(response.body)
        pages = min(self.limit, ((data['total'] - 1) / data['limit']) + 1)
        assert pages > 0
        assert self.start >= 1
        print "Requesting pages from %d to %d" % (self.start, self.start + pages - 1)
        return (Request(url="%s&page=%d" % (response.url, page), 
                        callback=self.parse_ruling) for page in xrange(self.start, self.start + pages))
        
    def get_dataset_url(self, dataset):
        dataset_url = urlparse.urljoin(self.api_url, dataset)
        dataset_url = '%s.php' % dataset_url
        return dataset_url
    
    parse_methods = {
        u'sa_orzeczenia' : parse_ruling_pagination
    }
    
    def __init__(self, dataset, **kwargs):
        super(SejmometrSpider, self).__init__(**kwargs)
        
        self.parse = self.parse_ruling_pagination  
        dataset_url = self.get_dataset_url(dataset)
        
        if 'limit' in kwargs:
            self.limit = int(kwargs['limit'])
        if 'start' in kwargs:
            self.start = int(kwargs['start'])
        params = {}
        for key, value in kwargs.items():
            if key.startswith('f_'):
                key = key.replace('f_', 'filter.', 1)
                params[key] = value
        enc_params = urllib.urlencode(params)
        url = '%s?pagination&%s' % (dataset_url, enc_params)
        self.start_urls = [url]
