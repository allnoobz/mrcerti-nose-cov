# -*- coding: utf-8 -*-

import json
import time
from scrapy.http.request import Request
from scrapy.spider import BaseSpider
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
from mrcerti.crawler.utils.deferred import (
    wait_for,
    deferred,
    _result,
)


class ApiSpider(BaseSpider):
    name = 'api_spider'
    heroku_start_url = 'http://staging-lex43.herokuapp.com'
    local_start_url = 'http://localhost:6543'
    search = '/lex43/v1/search?query=%s'
    next_page = '/lex43/v1/search?query=%s&offset=%d'
    content = '/v1/content/%d'

    def __init__(self, query=None, clients=1, heroku=None, *args, **kwargs):
        super(ApiSpider, self).__init__(*args, **kwargs)

        def choose_server():
            self.start_url = self.heroku_start_url if heroku \
                else self.local_start_url

        # make one query for each client
        def make_start_urls():
            self.start_urls = list()
            for _ in range(self.clients):
                self.start_urls.append((self.start_url + self.search) % query)

        dispatcher.connect(self.spider_closed, signals.spider_closed)
        self.clients = int(clients)
        self.query = query
        self.request_count = 0
        self.next_query_id = 0
        self.query_info = dict()
        self.status = dict()
        choose_server()
        make_start_urls()

    def spider_closed(self, spider):
        if spider.status.has_key('ERROR'):
            return

        status = ''.join([' %s : %d,' % (http_status, count)
                          for http_status, count in spider.status.iteritems()])
        times = [time['end'] - time['start'] for time in spider.query_info.itervalues()]
        _sum = sum(times)

        spider.log('[QUERY_INFO] Requests status:%s' % status)
        spider.log('[QUERY_INFO] Queries count: %d' % spider.clients)
        spider.log('[QUERY_INFO] Requests count: %d' % spider.request_count)
        spider.log('[QUERY_INFO] Max query time: %.2f' % max(times))
        spider.log('[QUERY_INFO] Min query time: %.2f' % min(times))
        spider.log('[QUERY_INFO] Avg query time: %.2f' % (_sum/float(len(times))))
        spider.log('[QUERY_INFO] Time of all queries: %.2f' % _sum)
        spider.log('[QUERY_INFO] Requests per second: %.2f' % (spider.request_count/_sum))

        _times = ''.join(['%.2f ' % time for time in times])
        spider.log(_times)

    # every starting request represents one client and has unique id
    def make_requests_from_url(self, url):

        def make_next_query_id():
            self.next_query_id += 1
            return self.next_query_id

        query_id = str(make_next_query_id())
        self.query_info[query_id] = dict()
        self.query_info[query_id]['start'] = time.clock()
        self.query_info[query_id]['offset'] = 0

        return Request(
            url,
            dont_filter=True,
            meta={'query_id': int(query_id)}
        )

    @deferred
    def parse(self, response):
        body = json.loads(response.body)
        item_list = body['results']['items']
        actual_id = str(response.meta['query_id'])
        self.query_info[actual_id]['offset'] += len(item_list)
        actual_offset = self.query_info[actual_id]['offset']

        # end time of query is saved when we get response from view 3.1
        def crawl_content(response):
            self.query_info[actual_id]['end'] = time.clock()
            yield _result(None)

        def get_next_page():
            if actual_offset < self.total_items:
                request = Request(
                    (self.start_url + self.next_page) % (self.query, actual_offset),
                    dont_filter=True,
                    meta={'query_id': int(actual_id)}
                )
                return request

        if not hasattr(self, 'total_items'):
            self.total_items = body['results']['total_count']

        # create requests for content (view 3.1) and do nothing with response
        for search_result in item_list:
            if 'reference' in search_result['target']:
                for item in search_result['target']['reference']['targets']['items']:
                    request = Request(
                        (self.start_url + self.content) % item['id'],
                        dont_filter=True,
                        callback=crawl_content,
                    )
                    yield wait_for(request)

        yield _result(get_next_page())
