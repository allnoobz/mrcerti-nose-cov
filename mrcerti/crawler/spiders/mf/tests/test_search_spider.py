# -*- coding: utf-8 -*-

from mrcerti.crawler.spiders.mf.search import MfSearchSpider
from mrcerti.crawler.spiders.mf.tests.mf_spider_test_case import MfSpiderTestCase


class TestSearchSpider(MfSpiderTestCase):
    def __init__(self, *args, **kwargs):
        kwargs['fname'] = 'mf_test_search_spider'
        super(TestSearchSpider, self).__init__(*args, **kwargs)

    def test_crawl_single_page(self):
        spider = MfSearchSpider(
            date_from='2007.7.23',
            date_to='2007.7.25',
        )
        items = self.run_spider(spider)
        expected_items = 8
        self.assertEqual(
            len(items),
            expected_items,
            "Processed item number should match (%s != %s)." %
            (len(items), expected_items)
        )
        item_ids = {
            item['item_id'] 
            for item in items
        }
        self.assertEqual(
            len(item_ids),
            expected_items,
            "Each processed item should have different id (%s != %s)."
            % (len(item_ids), expected_items)
        )

    def test_crawl_multiple_pages(self):
        spider = MfSearchSpider(
            date_from='2007.7.23',
            date_to='2007.9.1',
            
        )
        items = self.run_spider(spider)
        expected_items = 179
        self.assertEqual(
            len(items),
            expected_items,
            "Processed item number should match (%s != %s)." %
            (len(items), expected_items)
        )
        item_ids = {
            item['item_id'] 
            for item in items
        }
        self.assertEqual(
            len(item_ids),
            expected_items,
            "Each processed item should have different id (%s != %s)." %
            (len(item_ids), expected_items)
        )
