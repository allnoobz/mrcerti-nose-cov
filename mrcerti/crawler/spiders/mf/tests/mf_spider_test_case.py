# -*- coding: utf-8 -*-

from mrcerti.crawler.settings.crawler import mf
from mrcerti.crawler.spiders.tests.spider_test_case import (
    SpiderTestCaseWithScrapyObserver,
)


class MfSpiderTestCase(SpiderTestCaseWithScrapyObserver):
    
    def setUp(self):
        super(MfSpiderTestCase, self).setUp()
        assert self.settings
        self.settings.update(
            {
                'ITEM_PIPELINES': [
                    'mrcerti.crawler.pipelines.DuplicatesPipeline',
                ],
                'DUPEFILTER_CLASS': mf.DUPEFILTER_CLASS,
            }
        )
