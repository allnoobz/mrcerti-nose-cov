# -*- coding: utf-8 -*-

import datetime
from mrcerti.crawler.spiders.mf.single_document import MfSingleDocumentSpider
from mrcerti.crawler.spiders.mf.tests.mf_spider_test_case import MfSpiderTestCase


class TestSingleDocumentSpider(MfSpiderTestCase):

    def test_crawling(self):
        signature = 'IBPBII/2/415-1376/12/MZa'
        spider = MfSingleDocumentSpider(signature)
        items = self.run_spider(spider)
        self.assertEqual(len(items), 1)
        item = items[0]

        self.assertEqual(item['spider'], u'mf')
        self.assertEqual(item['signature'], signature)
        self.assertEqual(item['item_id'], '338891')
        self.assertEqual(
            item['url'],
            'http://sip.mf.gov.pl/sip/index.php?p=1&i_smpp_s_dok_nr_sek=338891'
            '&i_smpp_s_strona=1&i_smpp_s_pozycja=1&i_smpp_s_stron=1'
        )
        self.assertEqual(item['author'], u'Dyrektor Izby Skarbowej w Katowicach')
        self.assertEqual(item['publication_date'], '2012.12.28')
        self.assertEqual(item['document_type'], u'interpretacja indywidualna')

        self.assertListEqual(
            item['essences'],
            [
                u'Czy po przekszta\u0142ceniu sp\xf3\u0142ki z '
                u'ograniczon\u0105 odpowiedzialno\u015bci\u0105 w '
                u'sp\xf3\u0142k\u0119 komandytow\u0105 po stronie '
                u'wsp\xf3lnik\xf3w sp\xf3\u0142ki kapita\u0142owej powstanie '
                u'obowi\u0105zek zap\u0142aty podatku dochodowego od os\xf3b '
                u'fizycznych na podstawie art. 24 ust. 5pkt 8 ustawy z dnia '
                u'26 lipca 1991 r. o podatku dochodowym od os\xf3b fizycznych?',
            ]
        )
        self.assertListEqual(
            item['keywords'],
            [
                u'kapita\u0142 zapasowy',
                u'przekszta\u0142canie',
                u'sp\xf3\u0142ka kapita\u0142owa',
                u'sp\xf3\u0142ka komandytowa',
                u'zysk niepodzielony',
            ]
        )
        self.assertListEqual(
            item['topics'],
            [
                u'Podatek dochodowy od os\xf3b fizycznych',
                u'Szczeg\xf3lne zasady ustalania dochodu',
            ]
        )

        self.assertIsNotNone(item['text'])
        self.assertTrue(
            item['text'].startswith(
                u"<p sip=\"cb\" > INTERPRETACJA INDYWIDUALNA<br><br></p>\n"
                u"<p sip=\"0\" >Na podstawie art. 14b \xa7 1 i \xa7 6 ustawy"
            ),
            "Text '%s' doesn't start appropriately" % item['text'][:200]
        )

        self.assertIsNotNone(item['title'])
        self.assertTrue(
            item['title'].startswith(
                u'Czy po przekszta\u0142ceniu sp\xf3\u0142ki z '
                u'ograniczon\u0105 odpowiedzialno\u015bci\u0105 w '
                u'sp\xf3\u0142k\u0119 komandytow\u0105 po stronie'
            )
        )
