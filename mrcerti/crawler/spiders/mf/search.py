# -*- coding: utf-8 -*-

import re
import json
from datetime import datetime
from dateutil.rrule import rrule, DAILY
from scrapy import log
from scrapy.http import FormRequest, Request
from scrapy.selector.lxmlsel import HtmlXPathSelector
from mrcerti.crawler.formdata.mf import MfFormDataSchema, MfFilledForm
from mrcerti.crawler.exceptions import PageParseException
from mrcerti.crawler.spiders.mf.base import MfBaseSpider
from mrcerti.crawler.utils.deferred import wait_for, deferred, _result, no_wait


class MfSearchSpider(MfBaseSpider):
    """Crawler to search tax interpretations from MF sites.

    Additional argument passing to constructor:
    search_type -- possible values:
        general_interpretations -- for individual tax interpretations,
        court_rulings -- for court tax rulings and tax authority interpretations
    Arguments passing to search form:
        phrase -- searching phrase,
        doc_type,
        date_from -- in %Y.%m.%d format,
        date_to -- in %Y.%m.%d format,
        publicator,
        author,
        keyword,
        topic,
        signature.

    """
    name = 'search_spider'
    
    allowed_domains = ['sip.mf.gov.pl']
    
    page_url_schema = 'http://sip.mf.gov.pl/sip/index.php?p=2&i_smpp_s_strona=%s'
    main_page_url_schema = 'http://sip.mf.gov.pl/sip/%s'

    min_date = datetime(2007, 1, 1)
    max_search_results_per_page = 11
    search_types = {
        'general': 2,
        'court_rulings': 1,
    }

    formdata_schema = MfFormDataSchema()

    def __init__(self, search_type='general', **kwargs):
        super(MfSearchSpider, self).__init__(self.name, **kwargs)
        self.start_urls = [self.search_page(search_type)]
        self.filled = MfFilledForm(self.formdata_schema, **kwargs)
        self.results_to_parse = []

    def search_page(self, s_type):
        s_type = self.search_types[s_type]
        return 'http://sip.mf.gov.pl/sip/index.php?rodzaj=%s&czystka=1' % s_type

    def start_requests(self):
        for url in self.start_urls:
            yield Request(
                url=url,
                callback=self.parse_days,
                dont_filter=True,
            )

    @deferred
    def parse_days(self, response):
        self.log(
            'Form data:\n%s' %
            json.dumps(self.filled.formdata, indent=4), log.INFO
        )

        date_from = self.filled.fields.get('date_from')
        if date_from is not None:
            date_from = datetime.strptime(
                date_from, self.filled.date_format
            )
        else:
            date_from = self.min_date
        date_to = self.filled.fields.get('date_to')
        if date_to is not None:
            date_to = datetime.strptime(date_to, self.filled.date_format)
        else:
            date_to = datetime.today()

        for dt in rrule(DAILY, dtstart=date_from, until=date_to):
            self.log("Crawling day: %s" % dt)
            request = self.make_search_request(dt, response)
            yield wait_for(request)
        
        yield _result()

    def make_search_request(self, search_date, response):
        search_query = {
            'date_from': search_date.strftime(self.filled.date_format),
            'date_to': search_date.strftime(self.filled.date_format),
        }
        search_form = self.filled.clone(**search_query)

        request = FormRequest.from_response(
            response,
            formdata=search_form.formdata,
            callback=self.parse_result_list,
            #meta={'use_proxy':False}
        )
        return request

    def parse_result_list(self, response):
        pages = self._extract_page_count(response)
        
        self.log('Found %d pages of results.' % len(pages), log.DEBUG)
        for page in pages:
            request = Request(
                self.page_url_schema % page,
                callback=self.parse_page
            )
            yield wait_for(request)
        
        yield _result()
    
    def _extract_page_count(self, response):
        hxs = HtmlXPathSelector(response)
        search_results_number = hxs.select(
            '//div[@id="content"]/div[@class="column"]//ul[@class="lista"]/li[1]/text()'
        ).extract()

        if len(search_results_number) != 1:
            raise PageParseException()
        search_results_number = search_results_number[0]

        search_results_number = search_results_number.strip()
        if search_results_number.startswith(u'Wyniki wyszukiwania ograniczono do'):
            self.log('Too many results found. Create a more specific query.', log.WARNING)
            raise PageParseException()

        search_results_number = re.search(u'Znaleziono (\d+) dokumentów', search_results_number)
        if search_results_number is None:
            raise PageParseException()

        search_results_number = int(search_results_number.groups()[0])
        return range(1, (search_results_number + 10 - 1) / 10 + 1)

    def parse_page(self, response):
        hxs = HtmlXPathSelector(response)

        def document_paths(selector):
            return selector.select('//div[@class="wiecej"]/a/@href').extract()

        assert len(document_paths(hxs)) <= self.max_search_results_per_page

        for document_path in document_paths(hxs):
            request = Request(
                self.main_page_url_schema % document_path,
                callback=self.parse_document,
            )
            item = yield wait_for(request)
            yield no_wait(item)
        
        yield _result()

    def __str__(self, *args, **kwargs):
        return self.__class__.name
