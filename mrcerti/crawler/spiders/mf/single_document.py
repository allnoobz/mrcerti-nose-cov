# -*- coding: utf-8 -*-

from scrapy.http import FormRequest
from mrcerti.crawler.utils.deferred import wait_for, _result, deferred
from mrcerti.crawler.spiders.mf.search import MfSearchSpider


class MfSingleDocumentSpider(MfSearchSpider):

    name = 'single_document_spider'

    def __init__(self, signature=None, *args, **kwargs):
        kwargs.update({'signature': signature})
        super(MfSingleDocumentSpider, self).__init__(*args, **kwargs)

    @deferred
    def parse_days(self, response):
        signature = self.filled.fields.get('signature')
        if signature is None:
            raise ValueError("No signature specified")
        request = FormRequest.from_response(
            response,
            formdata=self.filled.formdata,
            callback=self.parse_result_list,
            #meta={'use_proxy':False}
        )
        yield wait_for(request)
        yield _result()
