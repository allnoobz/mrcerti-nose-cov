# -*- coding: utf-8 -*-

import re
import urlparse
from scrapy.selector.lxmlsel import HtmlXPathSelector
from scrapy.spider import BaseSpider
from scrapy.log import msg
from mrcerti.crawler.items.mf import MfDocumentItem


class MfBaseSpider(BaseSpider):
    
    def parse_document(self, response):
        hxs = HtmlXPathSelector(response)
        content = hxs.select(
            '//div[@id="content"]/div[@class="column"]'
        )
        assert len(content) == 1
        content = content[0]
        
        item = MfDocumentItem()
        item['item_id'] = self.extract_item_id(response.url)
        item['url'] = response.url
        item['spider'] = 'mf'
        
        def extract_list(name, required=True):
            result = content.select(
                './div[@class="fiszka fiszkaright"]'
                    '[preceding-sibling::div[@class="fiszka fiszkaleft"][1][text()="%s"]]/text()' % name
            ).extract()
            if required and len(result) == 0:
                raise ValueError("Failed to extract list '%s'" % name)
            return result
        
        def extract_one(name, required=True):
            result = extract_list(name, required=required)
            if len(result) == 0:
                return None
            elif len(result) > 1:
                raise ValueError("Multiple results for '%s'" % name)
            return result[0]
        
        item['document_type'] = extract_one(u"Rodzaj dokumentu")
        item['signature'] = extract_one(u"Sygnatura")
        item['publication_date'] = extract_one(u"Data")
        item['author'] = extract_one(u"Autor")
        item['topics'] = []
        for topic in extract_list(u"Temat", required=False):
            topic = topic.split('-->')
            topic = map(lambda s: s.strip(), topic)
            topic = filter(lambda s: len(s) > 0, topic)
            item['topics'].extend(topic)
        item['topics'] = item['topics'] or None
        item['essences'] = []
        for essence in extract_list(u"Istota interpretacji", required=False):
            essence = re.split(ur'\?(\s|$)', essence)
            essence = filter(lambda s: len(s.strip()) > 0, essence)
            essence = map(lambda s: s.strip() + '?', essence)
            item['essences'].extend(essence)
        item['essences'] = item['essences'] or []
        item['title'] = '; '.join(item['essences']) or item['signature']
        item['keywords'] = extract_list(u"Słowa kluczowe")
        
        text = "\n".join(
            content.select(
                './div[@class="dokumenttresc"]/*'
            ).extract()
        )
        text = re.sub(ur'style=("[^"]*")?', '', text)
        item['text'] = text
        
        return item
    
    def extract_item_id(self, url):
        return urlparse.parse_qs(
            urlparse.urlparse(url).query
        )['i_smpp_s_dok_nr_sek'][0]
