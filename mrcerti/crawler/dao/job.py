# -*- coding: utf-8 -*-

from datetime import datetime
from sqlalchemy.sql.expression import func, desc
from mrcerti.crawler.models.base import SessionManager
from mrcerti.crawler.models.job import (
    JobQueue, 
    JobQueueJob, 
    CrawlJob,
)


class JobDao():
    
    def __init__(self, name='default'):
        self.name = name
        self.man = SessionManager()
    
    @property
    def db(self):
        if self.man.db is None:
            raise ValueError("Attempt to access db outside of a transaction")
        return self.man.db
    
    def cleanup(self):
        with self.man:
            queue = self.get()
            for job in queue.jobs:
                job.cleanup()
                self.db.add(job)
    
    def exists(self):
        with self.man:
            queue = self.db.query(JobQueue).filter_by(name=self.name).first()
            return queue is not None
    
    def get(self):
        with self.man:
            queue = self.db.query(JobQueue).filter_by(name=self.name).first()
            if queue is None:
                raise ValueError("Queue with name '%s' doesn't exist" % self.name)
            return queue
    
    def create(self):
        with self.man:
            if self.exists():
                raise ValueError("Queue '%s' already exists" % self.name)
            queue = JobQueue(
                name=self.name,
                jobs=[],
            )
            self.db.add(queue)
    
    def delete(self):
        with self.mand:
            queue = self.get()
            self.db.delete(queue)
    
    def clear(self):
        with self.man:
            queue = self.get()
            queue.jobs = []
            self.db.add(queue)
    
    def schedule(self, job):
        with self.man:
            queue = self.get()
            order = self.db.query(func.max(JobQueueJob.order)).filter_by(
                queue_id=queue.id,
            ).scalar() or 0
            order += 1
            queue_job = JobQueueJob(
                order=order,
                job=job,
                created=datetime.now(),
            )
            queue.jobs.append(queue_job)
            self.db.add(queue)

    def run_once(self):
        self.cleanup()
        
        with self.man:
            queue_job = self._current_job()
            if queue_job is None:
                return False
            queue_job_id = queue_job.id
            queue_job.pre_run()
        
        with self.man:
            assert queue_job_id is not None
            queue_job = self.db.query(JobQueueJob).filter_by(id=queue_job_id).first()
            assert queue_job is not None
            queue_job.run()
        
        return True

    def _current_job(self):
        queue_job = self.db.query(JobQueueJob).join(JobQueue).filter(
            JobQueue.name == self.name,
            JobQueueJob.status == JobQueueJob.STATUS_PENDING,
        ).order_by(desc(JobQueueJob.order)).first()
        return queue_job

    def crawl(self, crawler, spider, **kwargs):
        job = CrawlJob(
            crawler=crawler,
            spider=spider,
            kwargs=kwargs,
        )
        self.schedule(job)
