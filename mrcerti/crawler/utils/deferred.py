# -*- coding: utf-8 -*-

import sys
import inspect, traceback
import types
from copy import copy
from scrapy.http.request import Request


class WaitResult(object):
    
    def __init__(self, result):
        self.result = result
        

class WaitExit(object):
    
    def __init__(self, result):
        self.result = result


class WaitFor(object):
    
    def __init__(self, **kwargs):
        assert len(kwargs) == 1
        assert 'request' in kwargs or 'generator' in kwargs
        self.request = kwargs.get('request')
        self.generator = kwargs.get('generator')


class NoWait(object):
    
    def __init__(self, value):
        self.value = value


_generator_logging = False
_generator_tb = None


def no_wait(value):
    global _generator_logging
    global _generator_tb
    frame = inspect.currentframe()
    try:
        _generator_tb = traceback.extract_stack(frame)[:-1]
        if _generator_logging:
            sys.stderr.write("Waiting for %s at:\n" % value)
            tb = traceback.extract_stack(frame, 5)[:-1]
            traceback.print_list(reversed(tb))
            sys.stderr.write("\n")
    finally:
        del frame
    return NoWait(value=value)


def wait_for(value):
    global _generator_logging
    global _generator_tb
    frame = inspect.currentframe()
    try:
        _generator_tb = traceback.extract_stack(frame)[:-1]
        if _generator_logging:
            sys.stderr.write("Waiting for %s at:\n" % value)
            tb = traceback.extract_stack(frame, 5)[:-1]
            traceback.print_list(reversed(tb))
            sys.stderr.write("\n")
    finally:
        del frame
    if isinstance(value, Request):
        if isinstance(value.callback, types.GeneratorType):
            value.callback = WaitFor(generator=value.callback)
        return WaitFor(request=value)
    else:
        assert isinstance(value, types.GeneratorType)
        return WaitFor(generator=value)


def _result(value=None):
    assert not isinstance(value, (WaitResult, WaitFor))
    global _generator_tb
    frame = inspect.currentframe()
    try:
        _generator_tb = traceback.extract_stack(frame, 5)[:-1]
        if _generator_logging:
            sys.stderr.write("Generator result at:\n")
            tb = traceback.extract_stack(frame, 5)[:-1]
            traceback.print_list(reversed(tb))
            sys.stderr.write("\n")
    finally:
        del frame
    return WaitResult(value)


def deferred(fn):
    global _generator_tb
    
    def make_callback(callback, stack):
        d = {'executed': False}
        def _callback(response):
            if d['executed']:
                raise ValueError("Callback function called twice!")
            d['executed'] = True
            result = callback(response)
            if isinstance(result, types.GeneratorType):
                # For the sake of safety we perform a shallow copy here.
                new_stack = stack + [result]
                result = None
            else:
                # For the sake of safety we perform a shallow copy here.
                new_stack = copy(stack)
            return deferred_step(new_stack, result)
        assert isinstance(stack, list)
        assert len(stack) > 0
        return _callback
    
    def deferred_step(stack, r):
        assert len(stack) > 0
        try:
            while True:
                if r is None:
                    x = stack[-1].next()
                else:            
                    x = stack[-1].send(r)
                if isinstance(x, WaitFor):
                    if x.generator is not None:
                        stack.append(x.generator)
                        r = None
                    else:
                        assert x.request is not None
                        assert isinstance(x.request, Request)
                        req = copy(x.request)
                        req.dont_filter = True
                        req.callback = make_callback(req.callback, stack)
                        yield req
                        break
                elif isinstance(x, WaitResult):
                    stack[-1].close()
                    stack.pop()
                    if len(stack) == 0:
                        yield x.result
                        break
                    r = x.result
                else:
                    assert isinstance(x, NoWait)
                    assert not isinstance(x.value, types.GeneratorType)
                    yield x.value
                    r = None
        except StopIteration:
            if _generator_tb is not None:
                sys.stderr.write("Premature exit from deferred,\n\n")
                sys.stderr.write("Last generator traceback:\n")
                traceback.print_list(reversed(_generator_tb))
            raise AssertionError("Premature exit from deferred")    
        except Exception as e:
            if _generator_tb is not None:
                sys.stderr.write("Exception in generator:")
                sys.stderr.write(unicode(e))
                sys.stderr.write("\n\n")
                sys.stderr.write("Last generator traceback:\n")
                traceback.print_list(reversed(_generator_tb))
            raise
    
    def wrapper(*args, **kwargs):
        gen = fn(*args, **kwargs)
        assert isinstance(gen, types.GeneratorType)
        return deferred_step([gen], None)

    return wrapper

