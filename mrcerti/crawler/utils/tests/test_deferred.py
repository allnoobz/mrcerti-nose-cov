# -*- coding: utf-8 -*-

from unittest import TestCase
from scrapy.item import Item, Field
from scrapy.http.request import Request
from mrcerti.crawler.utils.deferred import deferred, wait_for, _result


class TestItem(Item):
    x = Field()
    inner = Field()
    
    def __init__(self, x):
        super(TestItem, self).__init__()
        self['x'] = x


class DeferredTestCase(TestCase):
    
    def test_simple_inline(self):
        
        l = []
        
        def inner(n):
            l.append(2)
            yield _result(n)
            l.append(123)
            # Execution should never reach this point
            self.assertTrue(False)
        
        @deferred
        def outer(n):
            l.append(1)
            m = yield wait_for(inner(n))
            l.append(3)
            self.assertIsInstance(m, int)
            self.assertEquals(n, m)
            yield _result(None)
        
        list(outer(123))
        
        self.assertSequenceEqual(l, [1, 2, 3])
    
    def test_multi_inline(self):
        
        l = []
        
        def inner(n):
            l.append(('inner', 1))
            yield _result(n)
            l.append(('inner', 123))
            # Execution should never reach this point
            self.assertTrue(False)
        
        @deferred
        def outer(n):
            l.append(1)
            m = yield wait_for(inner(n))
            self.assertIsInstance(m, int)
            self.assertEquals(n, m)
            l.append(2)
            m2 = yield wait_for(inner(n + 1))
            self.assertIsInstance(m2, int)
            self.assertEquals(n + 1, m2)
            l.append(3)
            yield _result()
        
        list(outer(3))
        
        self.assertSequenceEqual(l, [1, ('inner', 1), 2, ('inner', 1), 3])
    
    def test_nested_inline(self):
        
        l = []
        
        def inner_inner(n):
            l.append(('inner_inner', 1))
            yield _result(n)
            l.append(('inner_inner', 234))
        
        def inner(n):
            l.append(('inner', 1))
            m = yield wait_for(inner_inner(n))
            self.assertIsInstance(m, int)
            self.assertEquals(m, n)
            l.append(('inner', 2))
            yield _result(n)
            l.append(('inner', 123))
            # Execution should never reach this point
            self.assertTrue(False)
        
        @deferred
        def outer(n):
            l.append(1)
            m = yield wait_for(inner(n))
            self.assertIsInstance(m, int)
            self.assertEquals(n, m)
            l.append(2)
            m2 = yield wait_for(inner(n + 1))
            self.assertIsInstance(m2, int)
            self.assertEquals(n + 1, m2)
            l.append(3)
            yield _result()
        
        list(outer(3))
        
        self.assertSequenceEqual(
            l, 
            [
                1, ('inner', 1), ('inner_inner', 1), ('inner', 2), 
                2, ('inner', 1), ('inner_inner', 1), ('inner', 2), 
                3,
            ],
        )
    
    def test_loop(self):
        
        l = []
        
        def inner_inner(n):
            xs = []
            for x in range(1, n + 1):
                l.append(('inner_inner', 2 * x - 1))
                xs.append(x)
                l.append(('inner_inner', 2 * x))
            yield _result(xs)
        
        def inner(n):
            xxs = []
            for x in range(1, n + 1):
                l.append(('inner', 2 * x - 1))
                xs = yield wait_for(inner_inner(x))
                xxs.append(xs)
                l.append(('inner', 2 * x))
            yield _result(xxs)
        
        @deferred
        def outer(n):
            for x in range(1, n + 1):
                l.append(2 * x - 1)
                xs = yield wait_for(inner(x))
                l.append(2 * x)
            yield _result(xs)

        r = outer(3)
        r = list(r)
        self.assertEquals(len(r), 1)
        r = list(r[0])
        
        self.assertEquals(
            r,
            [[1], [1, 2], [1, 2, 3]],
        )
        self.assertEquals(
            l,
            [
                1, 
                    ('inner', 1), 
                        ('inner_inner', 1), 
                        ('inner_inner', 2), 
                    ('inner', 2), 
                2, 
                3, 
                    ('inner', 1), 
                        ('inner_inner', 1), 
                        ('inner_inner', 2), 
                    ('inner', 2), 
                    ('inner', 3), 
                        ('inner_inner', 1), 
                        ('inner_inner', 2), 
                        ('inner_inner', 3), 
                        ('inner_inner', 4), 
                    ('inner', 4), 
                4, 
                5, 
                    ('inner', 1), 
                        ('inner_inner', 1),
                        ('inner_inner', 2), 
                    ('inner', 2), 
                    ('inner', 3), 
                        ('inner_inner', 1),
                        ('inner_inner', 2), 
                        ('inner_inner', 3), 
                        ('inner_inner', 4), 
                    ('inner', 4), 
                    ('inner', 5), 
                        ('inner_inner', 1), 
                        ('inner_inner', 2), 
                        ('inner_inner', 3), 
                        ('inner_inner', 4), 
                        ('inner_inner', 5), 
                        ('inner_inner', 6), 
                    ('inner', 6), 
                6,
            ],
        )
    
    def test_simple_wait_for_request(self):
        
        def inner_handler(response):
            return TestItem(x=123)
        
        @deferred
        def outer_handler(response):
            item1 = TestItem(x=1)
            request = Request(url='http://abc', callback=inner_handler)
            inner = yield wait_for(request)
            item2 = TestItem(x=2)
            item2['inner'] = inner['x']
            item3 = TestItem(x=3)
            yield _result([item1, item2, item3])
        
        request = outer_handler('xyz')
        request = list(request)
        self.assertEquals(len(request), 1)
        request = request[0]
        self.assertIsInstance(request, Request)
        self.assertEquals(request.url, 'http://abc')
        
        items = request.callback('cde')
        items = list(items)
        self.assertEquals(len(items), 1)
        items = items[0]
        self.assertIsInstance(items, list)
        self.assertEquals(len(items), 3)

    def test_dependent_wait_for_request(self):
        
        def inner_handler(response):
            return TestItem(x=response)
        
        @deferred
        def outer_handler(response):
            item1 = TestItem(x=1)
            request = Request(url='http://abc', callback=inner_handler)
            item2 = TestItem(x=2)
            inner = yield wait_for(request)
            item2['inner'] = inner['x']
            item3 = TestItem(x=3)
            request = Request(url='http://bcd', callback=inner_handler)
            inner = yield wait_for(request)
            item4 = TestItem(x=4)
            item4['inner'] = inner['x']
            item5 = TestItem(x=5)
            yield _result([item1, item2, item3, item4, item5])
        
        request = outer_handler('xyz')
        request = list(request)
        self.assertEquals(len(request), 1)
        request = request[0]
        self.assertIsInstance(request, Request)
        self.assertEquals(request.url, 'http://abc')
        
        request2 = request.callback('xyz')
        request2 = list(request2)
        self.assertEquals(len(request2), 1)
        request2 = request2[0]
        self.assertIsInstance(request2, Request)
        self.assertEquals(request2.url, 'http://bcd')
        
        items = request2.callback('yzw')
        items = list(items)
        self.assertIsInstance(items, list)

    def test_nested_wait_for_request(self):
        
        l = []
        
        def inner3_handler(response):
            l.append('inner3_handler')
            return TestItem(x=response)
        
        def inner4_handler(response):
            l.append('inner4_handler')
            yield _result(TestItem(x=response))
                
        def inner_inner_handler(response):
            l.append('inner_inner_handler')
            item1 = TestItem(x=response)
            request = Request(url='http://abc', callback=inner3_handler)
            item2 = yield wait_for(request)
            self.assertIsInstance(item2, TestItem)
            request2 = Request(url='http://bcd', callback=inner4_handler)
            item3 = yield wait_for(request2)
            self.assertIsInstance(item3, TestItem)
            yield _result([item1, item2, item3])
        
        def inner_handler(response):
            l.append('inner_handler')
            items = yield wait_for(inner_inner_handler(response))
            self.assertEquals(len(items), 3)
            yield _result(items)
        
        @deferred
        def outer_handler(response):
            l.append('outer_handler')
            items = yield wait_for(inner_handler(response))
            yield _result(items)
        
        request = outer_handler('aaa')
        request = list(request)
        self.assertEquals(len(request), 1)
        request = request[0]
        self.assertIsInstance(request, Request)
        self.assertEquals(request.url, 'http://abc')
        
        request2 = request.callback('bbb')
        request2 = list(request2)
        self.assertEquals(len(request2), 1)
        request2 = request2[0]
        self.assertIsInstance(request2, Request)
        self.assertEquals(request2.url, 'http://bcd')
        
        items = request2.callback('ccc')
        items = list(items)
        self.assertEquals(len(items), 1)
        items = items[0]
        self.assertIsInstance(items, list)
        self.assertEquals(len(items), 3)
        self.assertIsInstance(items[0], TestItem)
        self.assertEquals(items[0]['x'], 'aaa')
        self.assertIsInstance(items[1], TestItem)
        self.assertEquals(items[1]['x'], 'bbb')
        self.assertIsInstance(items[2], TestItem)
        self.assertEquals(items[2]['x'], 'ccc')
        
        self.assertIn('outer_handler', l)
        self.assertIn('inner_handler', l)
        self.assertIn('inner_inner_handler', l)
        self.assertIn('inner3_handler', l)
        self.assertIn('inner4_handler', l)
