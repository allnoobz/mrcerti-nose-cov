# -*- coding: utf-8 -*-

import os
import settings


# FIXME: Put this somewhere else
directory = settings.DOWNLOAD_DIR
if not os.path.exists(directory):
    os.makedirs(directory)
