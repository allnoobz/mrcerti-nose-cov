# -*- coding: utf-8 -*-

# USE_DB = True

DB_CONFIG = {
    'sqlalchemy.url': 'postgresql://crowdera:crowderatoniezabawka@localhost:5432/crowdera-crawler',
    'sqlalchemy.convert_unicode': True,
    'sqlalchemy.encoding': 'utf-8',
    'sqlalchemy.client_encoding': 'utf-8',
    'sqlalchemy.echo': False,
}
