# -*- coding: utf-8 -*-

import os

_deploy_name = os.environ.get('DEPLOY_NAME') or 'development'

# FIXME: Maybe we could have a dynamic import here
if _deploy_name == 'development':
    from mrcerti.crawler.settings.db.development import *
elif _deploy_name == 'staging':
    from mrcerti.crawler.settings.db.staging import *
elif _deploy_name == 'production':
    from mrcerti.crawler.settings.db.production import *
elif _deploy_name == 'test':
    from mrcerti.crawler.settings.db.test import *
else:
    raise ValueError("Invalid database configuration name: '%s'" % _deploy_name)
