# -*- coding: utf-8 -*-

import os

_deploy_name = os.environ.get('DEPLOY_NAME') or 'development'

# FIXME: Maybe we could have a dynamic import here
if _deploy_name == 'development':
    from mrcerti.crawler.settings.www.development import *
elif _deploy_name == 'staging':
    from mrcerti.crawler.settings.www.staging import *
elif _deploy_name == 'production':
    from mrcerti.crawler.settings.www.production import *
elif _deploy_name == 'test':
    from mrcerti.crawler.settings.www.test import *
else:
    raise ValueError("Invalid www configuration name: '%s'" % _deploy_name)
