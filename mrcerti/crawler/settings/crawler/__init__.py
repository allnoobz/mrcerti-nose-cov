# -*- coding: utf-8 -*-

import os

_crawler_name = os.environ.get('CRAWLER_NAME') or ''

# FIXME: Maybe we could have a dynamic import here
if _crawler_name == 'api':
    from mrcerti.crawler.settings.crawler.api import *
if _crawler_name == 'isap':
    from mrcerti.crawler.settings.crawler.isap import *
elif _crawler_name == 'mf':
    from mrcerti.crawler.settings.crawler.mf import *
elif _crawler_name == 'nsa':
    from mrcerti.crawler.settings.crawler.nsa import *
elif _crawler_name == 'sejmometr':
    from mrcerti.crawler.settings.crawler.sejmometr import *
