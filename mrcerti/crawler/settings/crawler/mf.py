# -*- coding: utf-8 -*-

from mrcerti.crawler.settings.crawler.base import *

BOT_NAME = 'mf'

USER_AGENT = 'Mozilla/5.0 (Windows; U; Windows NT 6.1; rv:2.2) Gecko/20110201'

SPIDER_MODULES = ['mrcerti.crawler.spiders.mf']
NEWSPIDER_MODULE = 'mrcerti.crawler.spiders.mf'

ITEM_PIPELINES = [
    'mrcerti.crawler.pipelines.DuplicatesPipeline',
    'mrcerti.crawler.pipelines.mf.MfSavePipeline',
]

DUPEFILTER_CLASS = 'mrcerti.crawler.downloadermiddleware.mf.dupefilter.IdDupeFilter'

WORKERS_ENABLED = True

PROCESSORS = [
    ('mrcerti.crawler.processors.mf.MfConvertProcessor', 2),
    ('mrcerti.crawler.processors.mf.MfSendProcessor', 1),
]
