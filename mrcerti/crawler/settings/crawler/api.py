# -*- coding: utf-8 -*-

from mrcerti.crawler.settings.crawler.base import *

BOT_NAME = 'api'

SPIDER_MODULES = ['mrcerti.crawler.spiders.api']
NEWSPIDER_MODULE = 'mrcerti.crawler.spiders.api'

DOWNLOADER_MIDDLEWARES.update({
    'mrcerti.crawler.downloadermiddleware.api.requestcounter.RequestCounterMiddleware': 610,
})
