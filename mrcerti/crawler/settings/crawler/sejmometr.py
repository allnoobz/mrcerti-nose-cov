# -*- coding: utf-8 -*-

from mrcerti.crawler.settings.crawler.base import *

BOT_NAME = 'sejmometr'

SPIDER_MODULES = ['mrcerti.crawler.spiders.sejmometr']
NEWSPIDER_MODULE = 'mrcerti.crawler.spiders.sejmometr'

ITEM_PIPELINES = [
     'mrcerti.crawler.pipelines.DuplicatesPipeline',
     'mrcerti.crawler.pipelines.nsa.NsaSavePipeline',
]

WORKERS_ENABLED = True

PROCESSORS = [
    ('mrcerti.crawler.processors.nsa.NsaConvertProcessor', 2),                    
    ('mrcerti.crawler.processors.nsa.NsaSendProcessor', 1),
]
