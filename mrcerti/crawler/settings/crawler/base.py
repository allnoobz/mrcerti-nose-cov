# -*- coding: utf-8 -*-

OVERRIDE_DUPLICATES = False

SPIDER_MIDDLEWARES = {
    'mrcerti.crawler.spidermiddleware.logging.LoggingSpiderMiddleware': 999,
}

DOWNLOADER_MIDDLEWARES = {
    'mrcerti.crawler.downloadermiddleware.logging.LoggingDownloaderMiddleware': 999,
}

EXTENSIONS = {
    'mrcerti.crawler.extensions.dbconnector.DatabaseConnector': 10,
    'mrcerti.crawler.extensions.workers.ItemProcessors': 600,
    'mrcerti.crawler.extensions.optimizer.ContentOptimizer': 700
}

DUPEFILTER_CLASS = 'scrapy.dupefilter.BaseDupeFilter'
