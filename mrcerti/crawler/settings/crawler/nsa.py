# -*- coding: utf-8 -*-

from mrcerti.crawler.settings.crawler import base
from mrcerti.crawler.settings.crawler.base import *

BOT_NAME = 'nsa'

SPIDER_MODULES = ['mrcerti.crawler.spiders.nsa']
NEWSPIDER_MODULE = 'mrcerti.crawler.spiders.nsa'

USER_AGENT = 'Mozilla/5.0 (Windows; U; Windows NT 6.1; rv:2.2) Gecko/20110201'

ITEM_PIPELINES = [
    'mrcerti.crawler.pipelines.DuplicatesPipeline',
    'mrcerti.crawler.pipelines.nsa.NsaSavePipeline',
]

SPIDER_MIDDLEWARES = dict(base.SPIDER_MIDDLEWARES, **{
    # for GAE proxy
    #'scrapy.contrib.spidermiddleware.offsite.OffsiteMiddleware': None,
})

WORKERS_ENABLED = True

PROCESSING_SIGNALS_ENABLED = True

PROCESSORS = [
    ('mrcerti.crawler.processors.nsa.NsaConvertProcessor', 2),
    ('mrcerti.crawler.processors.nsa.NsaSendProcessor', 1),
]

DOWNLOADER_MIDDLEWARES = dict(base.DOWNLOADER_MIDDLEWARES, **{
    # for GAE
    #'mrcerti.crawler.downloadermiddleware.httpproxy.GaeProxyMiddleware': 45,
    #'mrcerti.crawler.downloadermiddleware.nsa.captcha.CaptchaRequestRetryMiddleware': 50,
    
    # for ProxyMesh
    #'mrcerti.crawler.downloadermiddleware.httpproxy.ProxyMeshMiddleware': 720,
    #'mrcerti.crawler.downloadermiddleware.nsa.captcha.CaptchaProxyBanMiddleware': 50,
    
    # for ProxyList
    #'mrcerti.crawler.downloadermiddleware.httpproxy.ProxyListMiddleware': 720,
    #'mrcerti.crawler.downloadermiddleware.nsa.captcha.CaptchaProxyBanMiddleware': 50,
    
    # for AWS
    'mrcerti.crawler.downloadermiddleware.httpproxy.AwsProxyMiddleware': 720,
    'mrcerti.crawler.downloadermiddleware.nsa.captcha.CaptchaProxyBanMiddleware': 50,
})

PROXY_ENABLED = True

CONCURRENT_REQUESTS = 1

### PROXY LIST ###

PROXY_LIST_FILE = 'proxy_list'
PERMANENT_PROXY_BAN = True
CONCURRENT_REQUESTS_PER_PROXY = 1

### GAE PROXY ###

GAE_PROXY_DOMAIN = 'mrcerti-proxy.appspot.com'

### PROXYMESH ###

PROXYMESH_URL = 'uk.proxymesh.com'
PROXYMESH_PORT = '31280'
PROXYMESH_USER = 'j.kopczewski'
PROXYMESH_PASS = 'crowderatoniezabawka'
PROXYMESH_COUNT = 10

### AWS PROXY ###

RETRY_ENABLED = True        # comment if not using AWS
RETRY_TIMES = 30            # comment if not using AWS
RETRY_HTTP_CODES = [111]    # comment if not using AWS
AWS_REGION = 'us-west-2'
AWS_ACCESS_KEY_ID = 'AKIAI6EDPURMA2YMVUKQ'
AWS_SECRET_ACCESS_KEY = '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'
