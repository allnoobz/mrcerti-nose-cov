# -*- coding: utf-8 -*-

from mrcerti.crawler.settings.crawler import base
from mrcerti.crawler.settings.crawler.base import *

BOT_NAME = 'isap'

SPIDER_MODULES = ['mrcerti.crawler.spiders.isap']
NEWSPIDER_MODULE = 'mrcerti.crawler.spiders.isap'

DOWNLOADER_MIDDLEWARES = dict(base.DOWNLOADER_MIDDLEWARES, **{
    'mrcerti.crawler.downloadermiddleware.refresh.RefreshMiddleware': 610,
})

ITEM_PIPELINES = [
    'mrcerti.crawler.pipelines.DuplicatesPipeline',
    'mrcerti.crawler.pipelines.isap.IsapSavePipline',
]

WORKERS_ENABLED = True
PROCESSORS = [
    ('mrcerti.crawler.processors.isap.IsapDownloadProcessor', 3),
    ('mrcerti.crawler.processors.isap.IsapConvertProcessor', 3),
    ('mrcerti.crawler.processors.isap.IsapSendProcessor', 1),
]
