# -*- coding: utf-8 -*-

from mrcerti.crawler.settings.base import *
from mrcerti.crawler.settings.db import *
from mrcerti.crawler.settings.www import *
from mrcerti.crawler.settings.crawler import *
