# -*- coding: utf-8 -*-

from mrcerti.crawler import settings
from mrcerti.crawler.models.base import Session


def setup_session():
    config = getattr(settings, 'DB_CONFIG', None)
    if config is not None:
        Session.from_config(config)
