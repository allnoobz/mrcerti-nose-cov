# -*- coding: utf-8 -*-

from time import sleep
from mrcerti.crawler.dao.job import JobDao


def queue_create(queue, silent=False):
    dao = JobDao(queue)
    if silent and dao.exists():
        return
    dao.create()


def queue_delete(queue):
    dao = JobDao(queue)
    dao.delete()
    

def queue_clear(queue):
    dao = JobDao(queue)
    dao.clear()


def queue_run_once(queue):
    dao = JobDao(queue)
    dao.cleanup()
    dao.run_once()


def queue_run(queue):
    dao = JobDao(queue)
    dao.cleanup()
    try:
        for _ in xrange(0, 100):
            if dao.run_once():
                break
            sleep(5)
    except BaseException:
        dao.cleanup()
        raise


def queue_schedule_crawl(queue, crawler, spider, **kwargs):
    dao = JobDao(queue)
    dao.cleanup()
    dao.crawl(crawler=crawler, spider=spider, **kwargs)
