# -*- coding: utf-8 -*-

import json
import sys
import os
import time
import datetime
import traceback
import urllib2
import httplib
from socket import timeout
from urlparse import urljoin
from scrapy import log
from mrcerti.crawler.signal_mixin import SignalMixin
from mrcerti.crawler.stats import StatManager


class RemoteError(Exception):
    
    def __init__(self, job_id, reason):
        self.job_id = job_id
        self.reason = reason
    
    def __str__(self):
        return ('Server failed to execute job "%d". Reason:\n%s' %
                (self.job_id, self.reason))


class Processor(SignalMixin):
    
    queue = None
    
    def __init__(self, crawler, settings):
        self.crawler = crawler
        self.settings = settings

    def _process_item(self, item, spider=None):
        raise NotImplemented()

    def __repr__(self):
        return u"Processor(queue=%s)" % repr(self.queue)


class DownloadProcessor(Processor):
    
    def __init__(self, crawler, settings):
        super(DownloadProcessor, self).__init__(crawler, settings)
        self._download_dir = settings.get('DOWNLOAD_DIR')
        assert self._download_dir # Don't want it to be empty or None
        
    def _get_url(self, item):
        raise NotImplementedError()
        
    def _get_path(self, item):
        return NotImplementedError()
    
    def _process_download(self, item):
        pass

    def _process_item(self, item, spider=None):
        # FIXME: Handle failure. Catch only the exceptions that are directly
        # related to file opening, networking problems, etc.
        url = self._get_url(item)
        path = self._get_path(item)
        path = os.path.abspath(os.path.join(self._download_dir, path))
        response = urllib2.urlopen(url)
        with open(path, 'wb') as fp:
            fp.write(response.read())
        self._process_download(item, url, path)
        return item


class SendProcessor(Processor, StatManager):
    
    name = 'sender'
    retry = 5
    retry_codes = (
        httplib.GATEWAY_TIMEOUT,
        httplib.REQUEST_TIMEOUT
    )

    metadata_keys = []
    
    def __init__(self, crawler, settings):
        super(SendProcessor, self).__init__(crawler, settings)
        self.api_url = settings.get('API_URL')
        self.send_url = urljoin(self.api_url, '/v1/content/add')
    
    def _send_with_retry(self, item, part, request):
        retry = self.retry
        response = None
        while not response:
            try:
                response = urllib2.urlopen(request, timeout=30)
                if response.code > 299:
                    raise RemoteError("Error while processing payload", response.read())
            except urllib2.URLError as e:
                if retry:
                    self._retry(item, part, e)
                    retry -= 1
                else:
                    raise
            except urllib2.HTTPError as e:
                if retry and e.code in self.retry_codes:
                    self._retry(item, part, e)
                    retry -= 1
                else:
                    raise
        return response
    
    def _send_payload(self, item, part, payload):
        data = json.dumps(payload)
        headers = {
            'Content-Type': 'application/json',
        }
        request = urllib2.Request(self.send_url, data, headers)
        response = self._send_with_retry(item, part, request)
        self.stat_inc('json_bytes_sent', sys.getsizeof(data))
        response = response.read()
        return json.loads(response)
    
    def _process_payload(self, item, part, payload):
        return self._send_payload(item, part, payload)
    
    def _process_item(self, item, spider=None):
        payloads = self._build_payload(item)
        for i, payload in enumerate(payloads, 1):
            try:
                result = self._process_payload(item, i, payload)
            except Exception as e:
                self._not_sent(item, i, e)
                raise
            else:
                self._sent(item, i, result)
        return item
    
    def _build_payload(self, item):
        raise NotImplementedError()

    def _retry(self, item, part, error):
        log.msg('Failed and retrying to send %s:\n%s' %
                (item['item_id'], error), log.INFO)
    
    def _not_sent(self, item, part, error):
        log.msg('Failed to send %s part %d:\n%s' %
                (item['item_id'], part, error), log.ERROR)
    
    def _sent(self, item, part, response):
        log.msg('%s part %d document for %s sent successfully' %
                (item['item_id'], part, item['spider']), log.DEBUG)


class BigContentSendProcessor(SendProcessor):
    
    JOB_EXECUTING = 'executing'
    JOB_COMPLETED = 'completed'
    JOB_FAILED = 'failed'
    
    def __init__(self, crawler, settings):
        super(BigContentSendProcessor, self).__init__(crawler, settings)
        self.status_url = urljoin(self.api_url, '/v1/job/status/')
    
    def _wait_for_result(self, job_id):
        status_url = urljoin(self.status_url, str(job_id))
        status_req = urllib2.Request(status_url)
        while True:
            response = urllib2.urlopen(status_req, timeout=30)
            response = response.read()
            response = json.loads(response)
            if response['status'] == self.JOB_COMPLETED:
                return response['result']
            elif response['status'] == self.JOB_FAILED:
                raise RemoteError(job_id, response['result'])
    
    def _process_payload(self, item, part, payload):
        payload['async'] = True
        result = self._send_payload(item, part, payload)
        job_id = result['job_id']
        return self._wait_for_result(job_id)
