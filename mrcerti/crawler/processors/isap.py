# -*- coding: utf-8 -*-

import os
from copy import copy
from pdfminer.cmapdb import CMapDB
from scrapy.exceptions import DropItem
from mrcerti.convert.content.transform.isap import legal_act
from mrcerti.convert.output.builder import to_json
from mrcerti.convert.pdf.extract import get_file_content
from mrcerti.crawler.processors import (
    Processor,
    DownloadProcessor,
    BigContentSendProcessor
)

class IsapDownloadProcessor(DownloadProcessor):
    
    queue = 'isap:download'
    domain = 'isap.sejm.gov.pl'
    
    def _get_file_item(self, item):
        return item['consistent_file'] or item['published_file']
    
    def _get_url(self, item):
        file_item = self._get_file_item(item)
        if not file_item:
            raise DropItem('Missing appropriate file document: %s' % item)
        return file_item['url']
    
    def _get_path(self, item):
        file_item = self._get_file_item(item)
        return '%s_%s.%s' % (
            file_item['item_id'],
            file_item['type'],
            file_item['format'],
        )
    
    def _process_download(self, item, url, path):
        file_item = self._get_file_item(item)
        file_item['save_path'] = path
        file_item['saved'] = True


class IsapConvertProcessor(Processor):
    
    queue = 'isap:convert'
    
    def _get_file_item(self, item):
        return item['consistent_file'] or item['published_file']
    
    def _process_item(self, item, spider=None):
        CMapDB.debug = 1
        file_item = self._get_file_item(item)
        if not os.path.exists(file_item['save_path']):
            raise ValueError(u"File doesn't exist: '%s'" % file_item['save_path'])
        # Yeah, yeah, it's a race condition. So what? Nobody will delete those 
        # files anyway.
        content = get_file_content(file_item['save_path'])
        content = legal_act(content)
        assert content is not None
        item['content'] = content
        return item
    

class IsapSendProcessor(BigContentSendProcessor):
    
    queue = 'isap:send'
    
    def _build_payload(self, item):
        payload = {}
        payload['content'] = self._build_content(item)
        payload['reference'] = self._build_reference(item)
        payload['source'] = self._build_source(item)
        return [payload]
    
    def _build_content(self, item):
        assert item['content'] is not None
        result = to_json(item['content'])
        result['title'] = item['title']
        result['author'] = 'Sejm'
        # FIXME: Changesets
        return result

    def _build_reference(self, item):
        result = copy(item['signature'])
        result['discriminator'] = 'publication'
        return result

    def _build_source(self, item):
        return {
            'ident': item['item_id'],
            'url': item['url'],
            'spider': item['spider'],
        }
