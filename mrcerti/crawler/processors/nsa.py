# coding: utf-8 -*-

from copy import copy
from mrcerti.convert.content.transform.nsa import court_ruling
from mrcerti.convert.html.extract import get_content
from mrcerti.convert.output.builder import to_json
from mrcerti.convert.reference.parser import (
    parse_court_ruling_signature,
    parse_element_signature,
)
from mrcerti.crawler.processors import Processor, SendProcessor


class NsaConvertProcessor(Processor):
    
    name = 'convert'
    queue = 'nsa:convert'

    def _process_item(self, item, spider=None):
        content = get_content(item['text'])
        content = court_ruling(content)
        assert content is not None  
        item['content'] = content
        return item


class NsaSendProcessor(SendProcessor):
    name = 'send'
    queue = 'nsa:send'

    def _build_payload(self, item):
        payload = {
            'content': self._build_content(item),
            'reference': self._build_reference(item),
            'source': self._build_source(item),
        }
        return [payload]
    
    def _build_content(self, item):
        assert item['content'] is not None
        result = to_json(item['content'])
        result['title'] = item['title']
        result['discriminator'] = u'court_ruling'
        result['info'] = self._build_info(item)
        for legal_act in (item['legal_acts'] or []):
            references = parse_element_signature(legal_act)
            for reference in references:
                reference['discriminator'] = 'element'
                reference['root']['discriminator'] = 'publication'
                result['references']['items'].append(reference)
                result['references']['total_count'] += 1
        return result
    
    def _build_info(self, item):
        info_keys = [
            'title',
            'signature',
            'ruling_date',
            'filing_date',
            'author',
            'court',
            'judges',
            'topic',
            'keywords',
            'defendant',
            'judgment',
        ]
        info = {
            key: item[key]
            for key in info_keys
        }
        info['discriminator'] = u'court_ruling'
        return info
    
    def _build_reference(self, item):
        result = {}
        result['signature'] = copy(item['signature'])
        result.update(parse_court_ruling_signature(item['signature']))
        del result["schema"]
        # FIXME (on server) field target_title of courtrulingreference is not writable
        # so this data is not used
        result['target_title'] = item['title']
        result['discriminator'] = 'court_ruling'
        return result

    def _build_source(self, item):
        return {
            'ident': item['item_id'],
            'url': item['url'],
            'spider': item['spider'],
        }    
