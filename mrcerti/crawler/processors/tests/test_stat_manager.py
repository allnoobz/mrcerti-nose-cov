# -*- coding: utf-8 -*-

from unittest.case import TestCase
from mrcerti.crawler.processors import StatManager
from mrcerti.crawler.spiders.tests.spider_test_case import CrawlerScript


class NsaProcessorTestCase(TestCase, StatManager):

    def setUp(self):
        crawler = CrawlerScript({})
        self.stats = crawler.crawler.stats

    def test_addition(self):
        label = 'sum'
        for i in xrange(1, 10):
            self.stat_inc(label, i)
            self.assertEqual(i*(i+1)/2, self._stat_get(label, -1))

    def test_incrementation(self):
        label = 'counter'
        for i in xrange(1, 10):
            self.stat_inc(label)
            self.assertEqual(i, self._stat_get(label, -1))

    def test_two(self):
        label1 = 'a'
        label2 = 'b'
        self.stat_inc(label1)
        self.stat_dec(label2)
        self.assertEqual(self._stat_get(label1, 0), 1)
        self.assertEqual(self._stat_get(label2, 0), -1)

    def test_list(self):
        label = 'list'
        l = []
        for i in xrange(1, 10):
            self.stat_append(label, i)
            l.append(i)
            self.assertEqual(l, self._stat_get(label, []))

    def test_dict(self):
        label = 'dict'
        for i in xrange(1, 10):
            self.stat_update(label, i, i*i)
        self.assertEqual(len(self._stat_get(label, {})), 9)
        for key in self._stat_get(label, {}):
            self.assertEqual(key*key, self._stat_get(label, {})[key])

    def test_replace(self):
        label = 'x'
        d = {'x' : 'abcdef'}
        l = [1, 2, 3]
        self.stat_set(label, d)
        self.assertEqual(d, self._stat_get(label, {}))
        self.stat_set(label, l)
        self.assertEqual(l, self._stat_get(label, []))
