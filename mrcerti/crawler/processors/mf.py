# -*- coding: utf-8 -*-

from mrcerti.convert.content.transform.mf import tax_interpretation
from mrcerti.convert.html.extract import get_content
from mrcerti.convert.reference.parser import parse_tax_interpretation_signature
from mrcerti.convert.output.builder import to_json
from mrcerti.crawler.processors import (
    Processor, 
    SendProcessor,
)


class MfConvertProcessor(Processor):
    
    queue = 'mf:convert'
    
    def _process_item(self, item, spider=None):
        content = get_content(item['text'])
        content = tax_interpretation(content)
        item['content'] = content
        return item


class MfSendProcessor(SendProcessor):
    
    queue = 'mf:send'

    def _build_payload(self, item):
        payload = {
            'content': self._build_content(item),
            'reference': self._build_reference(item),
            'source': self._build_source(item),
        }
        return [payload]
    
    def _build_content(self, item):
        result = to_json(item['content'])
        result['title'] = item['title']
        result['author'] = item['author']
        result['info'] = self._build_info(item)
        return result
    
    def _build_info(self, item):
        info_keys = [
            'title',
            'signature',
            'publication_date',
            'author',
            'topics',
            'keywords',
            'essences',
        ]
        info = {
            key: item[key]
            for key in info_keys
        }
        info['discriminator'] = u'tax_interpretation'
        return info

    def _build_reference(self, item):
        result = parse_tax_interpretation_signature(item['signature'])
        result['discriminator'] = u'tax_interpretation'
        return result

    def _build_source(self, item):
        return {
            'ident': item['item_id'],
            'url': item['url'],
            'spider': item['spider'],
        }
