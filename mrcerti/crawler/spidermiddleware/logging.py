# -*- coding: utf-8 -*-

from scrapy import log


def _log_results(response, result, spider):
    def wrapper():
        for i, r in enumerate(result):
            spider.log(
                '%d th item from "%s" is a %s' %
                    (i, response.url, r.__class__),
                level=log.INFO,
            )
            spider.log(
                'headers for "%s":\n    %s' % (response.url, response.headers),
                level=log.DEBUG,
            )

            spider.log('result %d:\n    %s' % (i, r), level=log.DEBUG)

            yield r
    return wrapper()


class LoggingSpiderMiddleware(object):

    def process_spider_input(self, response, spider):
        spider.log(
            'gets input from response from "%s"' % response.url,
            level=log.INFO,
        )
        spider.log(
            'headers for "%s":\n    %s' % (response.url, response.headers),
            level=log.DEBUG,
        )

    def process_spider_output(self, response, result, spider):
        if result:
            result = _log_results(response, result, spider)
        else:
            spider.log(
                'did not generate any results from response from "%s"' %
                    response.url,
                level=log.INFO,
            )
            spider.log(
                'headers for "%s":\n    %s' % (response.url, response.headers),
                level=log.DEBUG,
            )

        return result

    def process_spider_exception(self, response, exception, spider):
        spider.log(
            'process_spider_input for response from "%s" caused an exception'
                % response.url,
            level=log.ERROR,
        )
        spider.log(
            'headers for "%s":\n    %s' % (response.url, response.headers),
            level=log.DEBUG,
        )
        spider.log('exception:\n%s' % exception, level=log.DEBUG)
