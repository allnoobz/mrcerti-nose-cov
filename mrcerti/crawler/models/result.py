# -*- coding: utf-8 -*-

from sqlalchemy.schema import Column, Sequence, ForeignKey
from sqlalchemy.types import (
    Integer, 
    String, 
    DateTime, 
    Text,
)
from mrcerti.crawler.models.base import Base
from sqlalchemy.orm import relationship, backref


# NOTE: The models in this module are used solely for statistics purposes.

class Result(Base):
    id = Column(
        Integer, 
        Sequence('result_id_seq', start=1001, increment=1),
        primary_key=True
    )
    discriminator = Column('type', String(30), nullable=False)
    date = Column(DateTime)
    
    # Unique identifier of the pickled item
    scrapy_item_id = Column(String(50), nullable=False, index=True)
    
    __tablename__ = 'result'
    __mapper_args__ = {
        'polymorphic_on': discriminator,
    }


class LegislativeAct(Result):
    __mapper_args__ = {
        'polymorphic_identity': 'legislative_act',
    }


class PolishLegislativeAct(LegislativeAct):
    id = Column(Integer, ForeignKey('result.id'), primary_key=True)
    journal = Column(String(20))
    year = Column(Integer)
    issue = Column(Integer)
    position = Column(Integer)
    title = Column(Text)
    status = Column(String(30))
    publish_date = Column(DateTime)
    effective_date = Column(DateTime)
    publish_authority = Column(String(20))
    signature = Column(String(256))
    
    __tablename__ = 'result_polish'
    __mapper_args__ = {
        'polymorphic_identity': 'polish_legislative_act',
    }


class EuropeanUnionLegislativeAct(LegislativeAct):
    id = Column(Integer, ForeignKey('result.id'), primary_key=True)
    
    __tablename__ = 'result_european'
    __mapper_args__ = {
        'polymorphic_identity': 'european_legislative_act',
    }


class CourtRuling(Result):
    id = Column(Integer, ForeignKey('result.id'), primary_key=True)
    title = Column(Text)
    signature = Column(String(256))
    ruling_date = Column(DateTime)
    filing_date = Column(DateTime)
    court = Column(String(256))
    judges = Column(String(256))
    keywords = Column(String(256))
    defendant = Column(String(256))
        
    __tablename__ = 'result_court_ruling'
    __mapper_args__ = {
        'polymorphic_identity': 'court_ruling',
    }


class TaxInterpretation(Result):
    id = Column(Integer, ForeignKey('result.id'), primary_key=True)
    signature = Column(String(256))
    document_type = Column(String(50))
    author = Column(String(200))
    # topics, keywords, essences - purposefully omitted

    __tablename__ = 'result_tax_interpretation'
    __mapper_args__ = {
        'polymorphic_identity': 'tax_interpretation',
    }

    
class ResultRelation(Base):
    relation_type = Column(String(30), primary_key=True)
    source_id = Column(Integer, ForeignKey('result.id'), primary_key=True)
    source = relationship(
        'Result',
        foreign_keys=[source_id],
        backref=backref(
            'relations',
        )
    )
    dest_scrapy_id = Column(String(50), primary_key=True)
    
    __tablename__ = 'result_relation'
