# -*- coding: utf-8 -*-

from sqlalchemy.schema import Column, Sequence
from sqlalchemy.types import (
    Integer, 
    String, 
    Boolean, 
    LargeBinary,
)
from mrcerti.crawler.models.base import Base


# Used for serializing items
class ItemModel(Base):
    id = Column(
        Integer, 
        Sequence('item_id_seq', start=1001, increment=1),
        primary_key=True
    )
    url = Column(String(255), nullable=False)
    spider = Column(String(255), nullable=False)
    
    # 'True' - the item comes from scraping by a spider
    # 'False' - the item doesn't come from a spider (perhaps it's related to
    # another item and has been created this way. 
    extracted = Column(Boolean, nullable=False, index=True)
    queue = Column(String(20))
    
    # Unique identifier of the pickled item
    scrapy_item_id = Column(String(50), nullable=False, index=True)
    
    # Scrapy 'Item' instance associated with the result in a pickled format. This 
    # will include any partly processed content
    scrapy_item = Column(LargeBinary)
    
    # Scrapy 'Item' instance hash. This will be re-evaluated after loading the
    # item in order to check integrity. If the hash stored in the database
    # doesn't match hash of the recreated object, item is dropped. This might be
    # due to changes in the Item implementation among other things.
    item_hash = Column(String(50))
    
    __tablename__ = 'item'
