# -*- coding: utf-8 -*-

from sqlalchemy.engine import engine_from_config
from sqlalchemy.orm.scoping import scoped_session
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.declarative.api import declarative_base
from mrcerti.crawler.exceptions import DatabaseNotConfigured


def make_base():
    @property
    def db(self):
        return Session()
    cls = declarative_base()
    cls.db = db
    return cls

Base = make_base()


class SessionHolder(object):
    def __init__(self, sessionmaker=None):
        self.sessionmaker = sessionmaker
    
    def __call__(self):
        if self.sessionmaker is None:
            raise DatabaseNotConfigured()
        return self.sessionmaker()
    
    def set(self, sessionmaker):
        self.sessionmaker = sessionmaker
    
    def from_config(self, config):
        self.engine = engine_from_config(config)
        self.set(scoped_session(sessionmaker(bind=self.engine)))

Session = SessionHolder()


class SessionManager(object):
    
    def __init__(self, session_maker=Session):
        self.session_maker = session_maker
        self.db = None
        self.nesting = 0
    
    def __enter__(self):
        if self.nesting == 0:
            assert self.db is None
            self.db = self.session_maker()
        self.nesting += 1
    
    def __exit__(self, exc_type, exc_value, traceback):
        assert self.nesting > 0
        self.nesting -= 1
        if self.nesting == 0:
            assert self.db is not None
            try:
                self.db.flush()
                self.db.commit()
            except:
                self.db.rollback()
            self.db = None
