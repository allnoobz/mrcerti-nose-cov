# -*- coding: utf-8 -*-

import itertools
import os
import subprocess
from sqlalchemy.schema import Column, Sequence, ForeignKey
from sqlalchemy.types import (
    Integer, 
    String, 
    Date,
    PickleType,
)
from sqlalchemy.orm import relationship
from mrcerti.crawler.models.base import Base


class JobQueue(Base):
    id = Column(
        Integer,
        Sequence('job_queue_id_seq', start=1001, increment=1),
        primary_key=True,
    )
    name = Column(String(50), nullable=False, unique=True)
    jobs = relationship(
        'JobQueueJob', 
        uselist=True,
        cascade='save-update,merge,expunge,refresh-expire,delete'
    )
    
    __tablename__ = 'job_queue'


class JobQueueJob(Base):
    
    STATUS_PENDING = 'pending'
    STATUS_SUCCESS = 'success'
    STATUS_FAILED = 'failed'
    STATUS_RUNNING = 'running'
    STATUS_CANCELLED = 'cancelled'
    
    id = Column(
        Integer,
        Sequence('job_queue_job_id_seq', start=1001, increment=1),
        primary_key=True,
    )
    queue_id = Column(Integer, ForeignKey('job_queue.id'))
    order = Column(Integer)
    created = Column(Date, nullable=False)
    job_id = Column(Integer, ForeignKey('job.id'), nullable=False)
    job = relationship(
        'Job',
        uselist=False,
        cascade='save-update,merge,expunge,refresh-expire'
    )
    status = Column(String(20), nullable=True)
    
    def __init__(self, *args, **kwargs):
        self.status = self.STATUS_PENDING
        super(JobQueueJob, self).__init__(*args, **kwargs)
    
    def cleanup(self):
        if self.status == self.STATUS_RUNNING:
            self.status = self.STATUS_FAILED
    
    def pre_run(self):
        assert self.status == self.STATUS_PENDING
        self.status = self.STATUS_RUNNING
        self.db.add(self)
    
    def run(self):
        assert self.status == self.STATUS_RUNNING
        try:
            self.job.run()
        except Exception:
            self.fail()
        else:
            self.success()
    
    def cancel(self):
        assert self.status == self.STATUS_RUNNING
        self.status = self.STATUS_CANCELLED
    
    def fail(self):
        assert self.status == self.STATUS_RUNNING
        self.status = self.STATUS_FAILED
    
    def success(self):
        assert self.status == self.STATUS_RUNNING
        self.status = self.STATUS_SUCCESS
    
    __tablename__ = 'job_queue_job'


class Job(Base):
    id = Column(
        Integer,
        Sequence('job_id_seq', start=1001, increment=1),
        primary_key=True,
    )
    name = Column(String(50), nullable=True)
    discriminator = Column('type', String(20), nullable=False)
    
    def run(self):
        raise NotImplementedError()
    
    __tablename__ = 'job'
    __mapper_args__ = {
        'polymorphic_on': discriminator
    }


class CommandJob(Job):
    command = Column('command', PickleType)
    env = Column('env', PickleType)
    
    def __init__(self, *args, **kwargs):
        self.env = {}
        super(CommandJob, self).__init__(*args, **kwargs)
    
    def run(self):
        print "ENV:"
        print self.env
        print "COMMAND:"
        print self.command
        env = os.environ.copy()
        env.update(self.env)
        subprocess.Popen(
            self.command, 
            stdin=subprocess.PIPE, 
            stdout=subprocess.PIPE, 
            env=env,
        ).wait()
    
    __mapper_args__ = {
        'polymorphic_identity': 'command'
    }


class CrawlJob(CommandJob):
    crawler = Column(String(50))
    spider = Column(String(50))
    
    def __init__(self, *args, **kwargs):
        kwargs_ = kwargs.pop('kwargs', {})
        crawler = kwargs['crawler']
        spider = kwargs['spider']
        command = [
            'scrapy',
            'crawl',
            spider,
        ]
        command += itertools.chain.from_iterable(
            ['-a', '%s=%s' % kw]
            for kw in kwargs_.items()
        )
        env = {
            'CRAWLER_NAME': crawler,
        }
        super(CrawlJob, self).__init__(
            command=command, 
            env=env,
            crawler=crawler, 
            spider=spider,
        )
    
    __mapper_args__ = {
        'polymorphic_identity': 'crawl'
    }
