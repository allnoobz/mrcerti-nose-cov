# -*- coding: utf-8 -*-


class StatManager(object):
    stats = None

    def __init__(self, stats=None):
        self.stats = stats

    def _stat_get(self, name, default):
        assert(name is not None)
        assert(self.stats is not None)
        if not self.stats.get_value(name):
            self.stats.set_value(name, default)
        return self.stats.get_value(name)

    def stat_set(self, name, value):
        self.stats.set_value(name, value)

    def stat_inc(self, name, delta=1):
        stat = self._stat_get(name, 0)
        stat += delta
        self.stat_set(name, stat)

    def stat_dec(self, name, delta=1):
        stat = self._stat_get(name, 0)
        stat -= delta
        self.stat_set(name, stat)

    def stat_append(self, name, elem):
        stat = self._stat_get(name, list())
        stat.append(elem)

    def stat_update(self, name, key, value):
        stat = self._stat_get(name, dict())
        stat[key] = value