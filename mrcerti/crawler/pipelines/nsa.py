# -*- coding: utf-8 -*-

from mrcerti.crawler.models.result import CourtRuling, ResultRelation
from mrcerti.crawler.pipelines import SavePipeline

class NsaSavePipeline(SavePipeline):
    
    def save_result(self, item, model, spider):
        ruling = self.db.query(CourtRuling).filter(
            CourtRuling.scrapy_item_id == model.scrapy_item_id
        ).first() or CourtRuling(
            scrapy_item_id=model.scrapy_item_id,
        )
        
        ruling.url = item['url']
        ruling.spider = item['spider']
        ruling.title = item['title']
        ruling.signature = item['signature']
        ruling.ruling_date = item['ruling_date']
        ruling.filing_date = item.get('filing_date')
        ruling.court = item['court']
        if item['judges'] is not None:
            ruling.judges = ', '.join(item['judges'])
        ruling.keywords = item['keywords']
        ruling.defendant = item.get('defendant')
         
        self.db.add(ruling)
        self.db.flush()
         
        if item['related_rulings'] is not None:
            for related in item['related_rulings']:
                relation = self.db.query(ResultRelation).filter_by(
                    source_id=ruling.id,
                    dest_scrapy_id=related['item_id'],
                    relation_type='related',
                )
                if relation is None:
                    relation = ResultRelation(
                        relation_type='related',
                        source_id=ruling.id,
                        dest_scrapy_id=related['item_id'],
                    )
                    self.db.add(relation)

        self.db.add(ruling)
        self.db.flush()
        return item

