# -*- coding: utf-8 -*-

import hashlib
import cPickle
from scrapy.exceptions import DropItem
from mrcerti.crawler.models.base import Session
from mrcerti.crawler.models.item import ItemModel
from mrcerti.crawler.signal_mixin import SignalMixin


class Pipeline(SignalMixin): pass


class DuplicatesPipeline(Pipeline):
    
    seen = set()
    
    def _process_item(self, item, spider):
        # Seen in this session
        assert item['item_id'] != None
        item['item_id'] = item['item_id'].strip()
        if item['item_id'] in self.seen:
            raise DropItem("Duplicate item found: %s" % item)
        else:
            self.seen.add(item['item_id'])
            
        # FIXME: This code will become part of the SessionRecorder once crawling
        # sessions are implemented.
         
        # session = Session()
        # if spider.settings.getbool('OVERRIDE_DUPLICATES'):
        #     duplicates = session.query(ItemModel).filter_by(
        #         scrapy_item_id=item['item_id'],
        #         extracted=True
        #     ).all()
        #     for duplicate in duplicates:
        #         session.delete(duplicate)
        # else:
        #     model = session.query(ItemModel).filter_by(
        #         scrapy_item_id=item['item_id'],
        #         extracted=True
        #     ).first()
        #     if model:
        #         raise DropItem("Duplicate item found: %s" % item)
        
        return item


class SavePipeline(Pipeline):
    
    def _process_item(self, item, spider):
        self.db = Session()
        self.save_item(item, spider)
        self.db.commit()
        del self.db
        return item
            
    def save_item(self, item, spider):
        model = self.save_model(item, spider)
        self.save_result(item, model, spider)

    def save_model(self, item, spider):
        model = self.db.query(ItemModel).filter_by(
            scrapy_item_id=item['item_id'],
        ).first() or ItemModel(
            scrapy_item_id=item['item_id']
        )
        
        model.extracted = True
        model.spider = item['spider']
        model.url = item['url']
        model.scrapy_item = cPickle.dumps(item, protocol=-1)
        m = hashlib.md5()
        m.update(unicode(item))
        model.scrapy_item_hash = unicode(m.hexdigest())
        
        self.db.add(model)
        return model
    
    def save_result(self, item, model, spider):
        raise NotImplementedError()
