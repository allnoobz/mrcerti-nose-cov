# -*- coding: utf-8 -*-

from mrcerti.crawler.models.result import TaxInterpretation
from mrcerti.crawler.pipelines import SavePipeline


class MfSavePipeline(SavePipeline):
    
    def save_result(self, item, model, spider):
        tax_interpretation = self.db.query(TaxInterpretation).filter_by(
            scrapy_item_id=model.scrapy_item_id
        ).first() or TaxInterpretation(
            scrapy_item_id=model.scrapy_item_id
        )

        attrs_to_set = ['author', 'document_type', 'signature', 'publication_date']
        for a in attrs_to_set:
            setattr(tax_interpretation, a, item[a])
        tax_interpretation.discriminator = 'tax_interpretation'
        
        self.db.add(tax_interpretation)
        self.db.commit()
