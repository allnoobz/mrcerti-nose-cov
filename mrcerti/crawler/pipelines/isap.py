# -*- coding: utf-8 -*-

from mrcerti.crawler.pipelines import SavePipeline
from mrcerti.crawler.models.result import (
    PolishLegislativeAct,
    ResultRelation,
)


class IsapSavePipline(SavePipeline):
    
    def save_result(self, item, model, spider):
        act = self.db.query(PolishLegislativeAct).filter(
            PolishLegislativeAct.scrapy_item_id == model.scrapy_item_id
        ).first() or PolishLegislativeAct(
            scrapy_item_id=model.scrapy_item_id,
        )
        
        # Only a subset of fields is stored in the database - these
        # are the fields that are useful for gathering stats.
        act.url = item['url']
        act.spider = item['spider']
        act.journal = item['signature']['journal']
        act.year = item['signature']['year']
        act.issue = item['signature']['issue']
        act.position = item['signature']['position']
        # FIXME: This should be replaced for a call to a routine in mrcerti.convert
        act.signature = "Dz.U. %s r. Nr %s poz. %s" % (act.year, act.issue, act.position)
        act.title = item['title']
        act.status = item['status']
        act.publish_date = item['publish_date']
        act.effective_date = item['effective_date']
        act.publish_authority = item['publish_authority']
        
        self.db.add(act)
        self.db.flush()

        for relation in item['relations']:
            for document in relation['documents']:
                if not document['item_id']:
                    continue
                relation_model = self.db.query(ResultRelation).filter_by(
                    source_id=act.id,
                    dest_scrapy_id=document['item_id'],
                    relation_type=relation['relation_type'],
                ).first()
                if relation_model is not None:
                    relation_model = ResultRelation(
                        source_id=act.id,
                        dest_scrapy_id=document['item_id'],
                        relation_type=relation['relation_type']
                    )
                    self.db.add(relation_model)
        
        self.db.flush()

        return item
