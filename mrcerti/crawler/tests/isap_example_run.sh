#!/bin/bash

scrapy crawl search_spider -a start_date=2002.06.01 -a end_date=2002.08.01
scrapy crawl search_spider -a start_date=2002.04.01 -a end_date=2002.06.01
scrapy crawl search_spider -a start_date=2012.01.01 -a end_date=2012.04.01
scrapy crawl search_spider -a start_date=2012.04.01 -a end_date=2012.06.01
scrapy crawl search_spider -a start_date=2012.06.01 -a end_date=2012.08.01
scrapy crawl search_spider -a start_date=2013.04.01 -a end_date=2013.06.01
scrapy crawl search_spider -a start_date=2013.07.01 -a end_date=2013.10.01
scrapy crawl search_spider -a start_date=2013.10.01 -a end_date=2013.11.01


# something bigger:
scrapy crawl single_document_spider -a item_id=WDU20130000021
scrapy crawl single_document_spider -a item_id=WDU20130000455
scrapy crawl single_document_spider -a item_id=WDU20130000628
scrapy crawl single_document_spider -a item_id=WDU20120001529
scrapy crawl single_document_spider -a item_id=WDU20120001512
