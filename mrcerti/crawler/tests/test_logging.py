# -*- coding: utf-8 -*-

from scrapy.item import Item, Field
from scrapy.http import Request
from scrapy.selector import HtmlXPathSelector
from scrapy.spider import BaseSpider
from mrcerti.crawler.downloadermiddleware.logging import (
    LoggingDownloaderMiddleware,
)
from mrcerti.crawler.spidermiddleware.logging import LoggingSpiderMiddleware
from mrcerti.crawler.spiders.tests.spider_test_case import (
    SpiderTestCase,
    SpiderTestCaseWithScrapyObserver,
)


class SimpleTestItem(Item):
    link_name = Field()


class SimpleTestSpider(BaseSpider):
    name = 'simple_test'
    allowed_domains = ['google.com']
    start_urls = [
        'http://www.google.com/',
    ]

    def parse(self, response):
        hxs = HtmlXPathSelector(response)
        for name in hxs.select('//a/text()').extract():
            yield SimpleTestItem(link_name=name)

        urls = hxs.select('//a/@href').extract()
        if urls:
            yield Request(urls[0], callback=self.ignore)

    def ignore(self, response):
        pass


# XXX: dead code
class LoggingMiddlewareImproperOrder(Exception):
    pass


# XXX: dead code
class TestLoggingDownloaderMiddleware(LoggingDownloaderMiddleware):

    process_request_called = False
    process_response_called = False

    def process_request(self, request, spider):
        self.__class__.process_request_called = True
        super(TestLoggingDownloaderMiddleware, self).process_request(
            request, spider,
        )

    def process_response(self, request, response, spider):
        self.__class__.process_response_called = True
        if not self.process_request_called:
            raise LoggingMiddlewareImproperOrder
        super(TestLoggingDownloaderMiddleware, self).process_response(
            request, response, spider,
        )


# XXX: dead code
class TestLoggingSpiderMiddleware(LoggingSpiderMiddleware):

    process_spider_input_called = False
    process_spider_output_called = False

    def process_spider_input(self, response, spider):
        self.__class__.process_spider_input_called = True
        if not self.process_spider_output_called:
            raise LoggingMiddlewareImproperOrder
        super(TestLoggingSpiderMiddleware, self).process_spider_input(
            response, spider,
        )

    def process_spider_output(self, response, result, spider):
        self.__class__.process_spider_output_called = True
        super(TestLoggingSpiderMiddleware, self).process_spider_output(
            response, result, spider,
        )


# XXX: dead code
class LoggingMiddlewareTestCase(SpiderTestCase):

    def setUp(self):
        super(LoggingMiddlewareTestCase, self).setUp()

        from mrcerti.crawler import settings
        sp_mws = {
            'mrcerti.crawler.tests.test_logging.TestLoggingSpiderMiddleware': 998,
        }
        sp_mws.update(settings.SPIDER_MIDDLEWARES)

        dl_mws = {
            'mrcerti.crawler.tests.test_logging.TestLoggingDownloaderMiddleware': 998,
        }
        dl_mws.update(settings.DOWNLOADER_MIDDLEWARES)

        self.settings.update(
            {
                'SPIDER_MIDDLEWARES': sp_mws,
                'DOWNLOADER_MIDDLEWARES': dl_mws,
            }
        )

    #def test_logging_middlewares(self):
        #self.run_spider(SimpleTestSpider())
        #self.assertTrue(TestLoggingDownloaderMiddleware.process_request_called)
        #self.assertTrue(TestLoggingDownloaderMiddleware.process_response_called)
        #self.assertTrue(TestLoggingSpiderMiddleware.process_spider_output_called)
        #self.assertTrue(TestLoggingSpiderMiddleware.process_spider_input_called)

class LoggingMiddlewareObserverTestCase(SpiderTestCaseWithScrapyObserver):

    def __init__(self, *args, **kwargs):
        kwargs['fname'] = 'test_logging_middlewares'
        super(LoggingMiddlewareObserverTestCase, self).__init__(*args, **kwargs)

    def test_logging_middlewares(self):
        if self.f:
            self.run_spider(SimpleTestSpider())
            process_request_called = False
            process_response_called = False
            process_spider_output_called = False
            process_spider_input_called = False

            self.f.seek(0)
            for line in self.f:
                if 'passing request to' in line:
                    process_request_called = True
                    break
            for line in self.f:
                if 'getting response from' in line:
                    process_response_called = True
                    break
            for line in self.f:
                if 'gets input from response from' in line:
                    process_spider_output_called = True
                    break
            for line in self.f:
                if '1 th item from' in line:
                    process_spider_input_called = True
                    break

            self.assertTrue(process_request_called)
            self.assertTrue(process_response_called)
            self.assertTrue(process_spider_input_called)
            self.assertTrue(process_spider_output_called)
