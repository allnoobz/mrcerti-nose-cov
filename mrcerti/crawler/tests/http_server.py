# -*- coding: utf-8 -*-

import re
import posixpath
from threading import Thread
from BaseHTTPServer import HTTPServer
from SimpleHTTPServer import SimpleHTTPRequestHandler


class AllHtmlHTTPRequestHandler(SimpleHTTPRequestHandler):
    protocol_version = "HTTP/1.0"
    
    def do_GET(self):
        root_dir = getattr(self.server, 'root_dir', '/')
        self.path = re.sub('^/+', '', self.path)
        self.path = posixpath.join(root_dir, self.path)
        if not posixpath.isabs(self.path):
            self.path = '/%s' % self.path
        SimpleHTTPRequestHandler.do_GET(self)
    
    def guess_type(self, path):
        return 'text/html'


class TestHTTPServer(HTTPServer):
    def __init__(self, ip='localhost', port=8000,
                 handler_class=AllHtmlHTTPRequestHandler, root_dir='tests'):
        HTTPServer.__init__(self, (ip, port), handler_class)
        self.server_thread = None
        self.root_dir = root_dir
    
    def serve(self):
        self.server_thread = Thread(target=self.serve_forever)
        self.server_thread.daemon = True
        self.server_thread.start()
    
    def shutdown(self):
        HTTPServer.shutdown(self)
        assert not self.server_thread.is_alive()
        self.server_thread = None