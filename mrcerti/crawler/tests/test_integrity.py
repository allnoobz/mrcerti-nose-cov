# -*- coding: utf-8 -*-

import json
import urllib2
from unittest.case import TestCase
from sqlalchemy.engine import engine_from_config
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.orm.scoping import scoped_session
from mrcerti.crawler.models.item import ItemModel
from mrcerti.crawler.models.result import CourtRuling
from mrcerti.crawler.spiders.isap.single_document import IsapSingleDocumentSpider
from mrcerti.crawler.spiders.isap.tests.isap_spider_test_case import IsapSpiderTestCase
from mrcerti.crawler.spiders.mf.search import MfSearchSpider
from mrcerti.crawler.spiders.mf.tests.mf_spider_test_case import MfSpiderTestCase
from mrcerti.crawler.spiders.nsa.single_document import NsaSingleDocumentSpider
from mrcerti.crawler.spiders.nsa.tests.nsa_spider_test_case import NsaSpiderTestCase
from mrcerti.crawler.spiders.sejmometr.base import SejmometrSpider
from mrcerti.crawler.spiders.tests.spider_test_case import SpiderTestCase
from mrcerti.crawler.settings.db import test

        
def test_connection(url):
    try:
        urllib2.urlopen(url, timeout=1)
        return True
    except urllib2.HTTPError:
        return True
    except urllib2.URLError:
        pass
    return False


def get_content(content_id):
    url = 'http://localhost:6543/v1/content/%d' % content_id
    req = urllib2.Request(url)
    response = urllib2.urlopen(req)
    response = response.read()
    response = json.loads(response)
    content = response['content']
    return content


def get_child(content, path):
    if not path:
        return content
    unit, value = path.pop(0)
    for child in content['children']['items']:
        if child['unit'] == unit and child['value'] == value:
            return get_child(child, path)
    return None

def find_dict(dict_list, params):
    for d in dict_list:
        if all([(k in d and d[k] == v) for k, v in params.items()]):
            return d
    return None


class IntegrityTestCase(TestCase):
    
    def remove_all_items(self):
        engine = engine_from_config(test.DB_CONFIG)
        session = scoped_session(sessionmaker(bind=engine))
        session.query(ItemModel).delete()
        session.commit()
    
    def setUp(self):
        super(IntegrityTestCase, self).setUp()
        self.remove_all_items()
        
    def tearDown(self):
        self.remove_all_items()
        super(IntegrityTestCase, self).tearDown()


class IsapIntegrityTestCase(IntegrityTestCase, IsapSpiderTestCase):
    
    def setUp(self):
        super(IsapIntegrityTestCase, self).setUp()
        from mrcerti.crawler.settings.crawler import isap
        self.settings.update(
            {
                'WORKERS_ENABLED': isap.WORKERS_ENABLED,
                'PROCESSORS': isap.PROCESSORS,
                'ITEM_PIPELINES': isap.ITEM_PIPELINES,
            }
        )
        self.assertTrue(test_connection(self.settings['API_URL']), "Cannot connect to API service")
    
    def test_simple_act(self):
        item_id = 'WDU20000941037'
        # item_id = 'WDU19971230776'
        spider = IsapSingleDocumentSpider(item_id)
        items = self.run_spider(spider)
        self.assertEqual(len(items), 1, "One act should be sent")
        
        # FIXME: Handle response from the server correctly. Right now the pipeline
        # doesn't return the actual payload returned by the server. Should this be
        # a separate produced item?
        
        # item = items[0]
        # self.assertIn('references', item)
        # references = item['references']
        # self.assertEqual(len(references), 1, "One reference should be received")
        # reference = references[0]
        # self.assertIn('targets', reference)
        # self.assertEqual(len(reference['targets']['items']), 1)
        # target = reference['targets']['items'][0]
        # self.assertIn('id', target)
        # content_id = target['id']
        # content = get_content(content_id)
        # self.assertEqual(content['in_force'], '2007-12-31')
        # self.assertEqual(content['author'], 'SEJM')
        # self.assertEqual(content['title'], u"Ustawa z dnia 7 września 2007 r. o czasowym posługiwaniu się dowodami osobistymi wydanymi przed dniem 1 stycznia 2001 r.")
        
        # self.assertEqual(content['children']['total_count'], 2)
        
        # child1 = get_child(content, [(u'article', '1')])
        # self.assertIsNotNone(child1)
        # self.assertTrue(child1['text'].startswith(u'Dowody osobiste wydane przed dniem 1 stycznia 2001 r.'))
        # self.assertIsInstance(child1['id'], int)
        
        # child2 = get_child(content, [(u'article', '2')])
        # self.assertIsNotNone(child2)
        # self.assertEqual(child2['text'], u'Ustawa wchodzi w życie z dniem 31 grudnia 2007 r.')
        # self.assertIsInstance(child2['id'], int)

        # self.assertIsNotNone(find_dict(child1['references']['items'], {'journal': 'polish', 'year': None, 'issue': '113', 'position': '733'}))
        # self.assertIsNotNone(find_dict(child1['references']['items'], {'journal': 'polish', 'year': '1998', 'issue': '113', 'position': '716'}))
        # self.assertIsNotNone(find_dict(child1['references']['items'], {'journal': 'polish', 'year': '1999', 'issue': '108', 'position': '1227'}))
        # self.assertIsNotNone(find_dict(child1['references']['items'], {'journal': 'polish', 'year': '2001', 'issue': '43', 'position': '476'}))
        # self.assertIsNotNone(find_dict(child1['references']['items'], {'journal': 'polish', 'year': '2002', 'issue': '183', 'position': '1522'}))

    def test_act_with_changesets(self):
        item_id = 'WDU20070350217'
        spider = IsapSingleDocumentSpider(item_id)
        items = self.run_spider(spider)
        self.assertEqual(len(items), 1, "One act should be sent")
        
        # FIXME: Handle response from the server correctly. Right now the pipeline
        # item = items[0]
        # self.assertIn('references', item)
        # references = item['references']
        
        # self.assertGreaterEqual(len(references), 23)
        # for reference in references:
        #     self.assertIn('discriminator', reference)
        # by_changeset_signatures = [
        #     (ref['root']['signature'], ref['changeset']['signature'])
        #     for ref in references if ref['discriminator'] == 'by_changeset'
        # ]
        # publication_signatures = [
        #     ref['signature'] for ref in references if ref['discriminator'] == 'publication'
        # ]
        
        # self.assertIn('Dz.U. z 2007 r. Nr 35 poz. 217', publication_signatures)
        # for sig in [s[0] for s in by_changeset_signatures]:
        #     self.assertEqual(sig, 'Dz.U. z 2007 r. Nr 35 poz. 217', "All roots must be the same")
        
        # changeset_signatures = list(publication_signatures)
        # changeset_signatures.remove('Dz.U. z 2007 r. Nr 35 poz. 217')
        # self.assertItemsEqual(
        #     [s[1] for s in by_changeset_signatures],
        #     changeset_signatures
        # )


class MfIntegrityTestCase(IntegrityTestCase, MfSpiderTestCase):
    
    def setUp(self):
        super(MfIntegrityTestCase, self).setUp()
        from mrcerti.crawler.settings.crawler import mf
        self.settings.update({
            'WORKERS_ENABLED': mf.WORKERS_ENABLED,
            'PROCESSORS': mf.PROCESSORS,
            'ITEM_PIPELINES': mf.ITEM_PIPELINES,
        })
        self.assertTrue(test_connection(self.settings['API_URL']), "Cannot connect to API service")
    
    def test_single_doc(self):
        signature = 'IPTPB1/415-283/13-3/MAP'
        spider = MfSearchSpider(signature=signature)
        items = self.run_spider(spider)
        self.assertEqual(len(items), 1, "One document should be sent")
        item = items[0]
        print item
        # NOTE: The format of MF web pages changed recently. References are not
        # available anymore.
        # self.assertIn('references', item)
        # references = item['references']
        # self.assertEqual(len(references), 1, "One reference should be received")
        # reference = references[0]
        # self.assertIn('signature', reference)
        # self.assertEqual(reference['signature'], u'IPTPB1/415-283/13-3/MAP')
        # self.assertIn('targets', reference)
        # self.assertEqual(len(reference['targets']['items']), 1)
        # target = reference['targets']['items'][0]
        # self.assertIn('id', target)
        # content_id = target['id']
        # FIXME: Why is this here? This should be part of the pipeline, no?
        # content = get_content(content_id)
        # self.assertEqual(content['children']['total_count'], 2)
        # self.assertEqual(content['children']['items'][0]['children'], 1)


class NsaIntegrityTestCase(IntegrityTestCase, NsaSpiderTestCase):
    
    def setUp(self):
        super(NsaIntegrityTestCase, self).setUp()
        from mrcerti.crawler.settings.crawler import nsa
        self.settings.update(
            {
                'WORKERS_ENABLED': nsa.WORKERS_ENABLED,
                'PROCESSORS': nsa.PROCESSORS,
                'ITEM_PIPELINES': nsa.ITEM_PIPELINES,
            }
        )
        self.assertTrue(test_connection(self.settings['API_URL']), "Cannot connect to API service")
    
    def test_single_doc(self):
        item_id = '1B4CE25BA7'
        spider = NsaSingleDocumentSpider(item_id)
        items = self.run_spider(spider)
        self.assertEqual(len(items), 1, "One document should be sent")
        item = items[0]
        self.assertIn('references', item)
        references = item['references']
        self.assertEqual(len(references), 1, "One reference should be received")
        reference = references[0]
        self.assertIn('signature', reference)
        self.assertEqual(reference['signature'], u'VI SA/Wa 322/06')
        self.assertIn('targets', reference)
        self.assertEqual(len(reference['targets']['items']), 1)
        target = reference['targets']['items'][0]
        self.assertIn('id', target)
        content_id = target['id']
        content = get_content(content_id)
        self.assertIn('children', content)
        self.assertIn('items', content['children'])
        children = content['children']['items']
        sentence = find_dict(children, {'unit': 'chapter', 'value': 'sentence'})
        substantiation = find_dict(children, {'unit': 'chapter', 'value': 'substantiation'})
        self.assertIsNotNone(sentence)
        self.assertIsNotNone(substantiation)


class SejmometrIntegrityTestCase(IntegrityTestCase, SpiderTestCase):
    
    def setUp(self):
        super(SejmometrIntegrityTestCase, self).setUp()
        from mrcerti.crawler.settings.crawler import sejmometr
        self.settings.update(
            {
                'WORKERS_ENABLED': sejmometr.WORKERS_ENABLED,
                'PROCESSORS': sejmometr.PROCESSORS,
                'ITEM_PIPELINES': sejmometr.ITEM_PIPELINES,
            }
        )
        self.assertTrue(test_connection(self.settings['API_URL']), "Cannot connect to API service")

    def test_database(self):
        spider = SejmometrSpider(
            dataset='sa_orzeczenia',
            f_sygnatura='I SA/Gd 1275/12',
        )
        items = self.run_spider(spider)
        self.assertEqual(len(items), 1)
        db_config = self.settings.DB_CONFIG
        engine = engine_from_config(db_config)
        session = scoped_session(sessionmaker(bind=engine))
        for ruling in session.query(CourtRuling).filter(CourtRuling.signature == 'I SA/Gd 1275/12'):
            self.assertEqual(ruling.keywords, u'Podatek od nieruchomości')
            self.assertEqual(ruling.scrapy_item_id, u'786147')

    def test_single_doc(self):
        limit = 2
        spider = SejmometrSpider(
            dataset='sa_orzeczenia',
            limit=limit
        )
        items = self.run_spider(spider)
        print "%d items downloaded." % len(items)
        self.assertEqual(len(items), 40)

    def test_single_doc_middle(self):
        limit = 1
        start = 2
        spider = SejmometrSpider(
            dataset='sa_orzeczenia',
            limit=limit,
            start=start
        )
        items = self.run_spider(spider)
        print "%d items downloaded." % len(items)
        self.assertGreater(len(items), (limit - 1) * 20)
        self.assertLessEqual(len(items), limit * 20)
