# -*- coding: utf-8 -*-

from mrcerti.crawler.items import (
    DocumentItem, 
    Field, 
    UnicodeField, 
    ListField, 
    DateField,
    StringField,
    date_input_processor,
    date_output_processor,
)


class MfDocumentItem(DocumentItem):
    signature = Field()
    publication_date = DateField(
        input_processor=date_input_processor,
        output_processor=date_output_processor,
    )
    title = UnicodeField()
    author = UnicodeField()
    topics = ListField()
    keywords = ListField()
    essences = ListField()
    document_type = StringField()
