# -*- coding: utf-8 -*-

from mrcerti.crawler.items import (
    DocumentItem, 
    StringField, 
    ResourceItem, 
    DateField, 
    ListField,
    date_input_processor,
    date_output_processor,
)


class NsaDocumentItem(DocumentItem):
    signature = StringField(nullable=False)
    ruling_date = DateField(
        input_processor=date_input_processor,
        output_processor=date_output_processor,
        nullable=False,
    )
    filing_date = DateField(
        input_processor=date_input_processor,
        output_processor=date_output_processor,
    )
    court = StringField(nullable=False)
    judges = ListField()
    topic = StringField()
    keywords = StringField()
    related_rulings = ListField()
    defendant = StringField()
    # Watch out for the spelling it's *NOT* judgement
    judgment = StringField(nullable=False)
    referenced_laws = ListField()
    legal_acts = ListField()
    text = StringField()


class NsaRelationItem(ResourceItem):
    signature = StringField()
