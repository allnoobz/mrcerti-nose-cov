# -*- coding: utf-8 -*-

from copy import copy
from datetime import date, datetime
from pprint import pformat
from scrapy.item import Item as Item_, Field as Field_
from scrapy.contrib.loader.processor import Identity
from mrcerti.convert.content.content import Content
from dateutil.parser import parse as parse_date
from dateutil.relativedelta import relativedelta


class Item(Item_):

    def __getitem__(self, key):
        if key not in self.fields:
            raise KeyError(key)
        field = self.fields[key]
        nullable = getattr(field, '_nullable')
        try:
            value = super(Item, self).__getitem__(key)
        except KeyError:
            if nullable:
                value = None
            else:
                raise KeyError(key)
        processor = getattr(field, '_output_processor', None)
        if processor is not None:
            value = processor(value)
        return value
    
    def __setitem__(self, key, value):
        if key not in self.fields:
            raise KeyError(key)
        field = self.fields[key]
        processor = getattr(field, '_input_processor', None)
        type_ = getattr(field, '_type')
        nullable = getattr(field, '_nullable')
        if processor is not None:
            value = processor(value)
        if not nullable and value is None:
            raise ValueError("Value %s is not allowed for non-nullable fields" % value)
        if value is not None and type_ is not None and not isinstance(value, type_):
            raise ValueError("Value %s is not of type %s" % (value, type_))
        super(Item, self).__setitem__(key, value)
    
    def __copy__(self):
        return self.__class__(
            copy(self._values)
        )


class Field(Field_):
        
    def __init__(self, **kwargs):
        self._input_processor = kwargs.get('input_processor')
        self._output_processor = kwargs.get('output_processor')
        self._type = kwargs.get('type')
        self._nullable = kwargs.get('nullable', True)
        super(Field, self).__init__()
   

class TypeField(Field):
    
    def __init__(self, **kwargs):
        assert 'type' not in kwargs
        cls = self.__class__
        super(TypeField, self).__init__(type=cls.type, **kwargs)


class StringField(TypeField):
    type = basestring

class UnicodeField(TypeField):
    type = unicode
    
class IntegerField(TypeField):
    type = int

class DictField(TypeField):
    type = dict
    
class ListField(TypeField):
    type = list
    
class SequenceField(TypeField):
    type = (list, tuple)
    
class BoolField(TypeField):
    type = bool

class DateField(TypeField):
    type = (date, datetime)


class ResourceItem(Item):
    item_id = Field(type=basestring)
    url = Field(type=basestring)
    spider = Field(type=basestring)


class DocumentItem(ResourceItem):
    title = StringField()
    text = StringField()
    content = Field(type=Content)        
    sent = Field()

    def __str__(self):
        d = dict(self)
        if 'content' in d:
            d['content'] = '...content...'
        if 'text' in d:
            d['text'] = d['text'][:100] + '...'
        return pformat(d)


class FileItem(ResourceItem):
    format = StringField()
    save_path = StringField()
    saved = BoolField()


def date_input_processor(value):
    if value is None:
        return None
    if isinstance(value, basestring):
        value = parse_date(value, yearfirst=True)
        if value.year >= 2018:
            value = value - relativedelta(years=100)
    assert isinstance(value, (datetime, date))
    return value


def date_output_processor(value):
    if value is None:
        return None
    if isinstance(value, (datetime, date)):
        value = value.strftime('%Y.%m.%d')
    assert isinstance(value, basestring)
    return value
