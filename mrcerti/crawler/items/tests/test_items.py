# -*- coding: utf-8 -*-

from unittest.case import TestCase
from mrcerti.crawler.items import (
    Item, 
    Field, 
    StringField, 
    IntegerField,
)


def processor1(value):
    return value + "_abc"


def processor2(value):
    return "abc_" + value


class TestItem(Item):
    
    field1 = Field(input_processor=processor1)
    field2 = Field(output_processor=processor2)
    field3 = Field(
        input_processor=processor1,
        output_processor=processor2,
    )
    str_field = StringField()
    int_field = IntegerField()
    nullable_field = Field(nullable=True)
    non_nullable_field = Field(nullable=False)


def _set_item(item, key, value):
    def fn():
        item[key] = value
    return fn


def _get_item(item, key):
    def fn():
        item[key]
    return fn


class ItemTestCase(TestCase):
    
    def test_input_output_processor(self):
        item = TestItem()
        item['field1'] = 'xyz'
        item['field2'] = 'xyz'
        item['field3'] = 'xyz'
        self.assertEquals(item['field1'], 'xyz_abc')
        self.assertEquals(item['field2'], 'abc_xyz')
        self.assertEquals(item['field3'], 'abc_xyz_abc')
    
    def test_types(self):
        item = TestItem()
        item['str_field'] = 'abc'
        self.assertEquals(item['str_field'], 'abc')
        item['int_field'] = 123
        self.assertRaises(ValueError, _set_item(item, 'int_field', '123'))
        item['int_field'] = 234
        self.assertEquals(item['int_field'], 234)
        
    def test_nullable(self):
        item = TestItem()
        self.assertRaises(KeyError, _get_item(item, 'blah'))
        self.assertIsNone(item['nullable_field'])
        item['nullable_field'] = 123
        self.assertEquals(item['nullable_field'], 123)
        item['nullable_field'] = None
        self.assertIsNone(item['nullable_field'])
        self.assertRaises(KeyError, _get_item(item, 'non_nullable_field'))
        item['non_nullable_field'] = 123
        self.assertEquals(item['non_nullable_field'], 123)
        self.assertRaises(ValueError, _set_item(item, 'non_nullable_field', None))
        self.assertEquals(item['non_nullable_field'], 123)
