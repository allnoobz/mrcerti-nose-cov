# -*- coding: utf-8 -*-

from datetime import datetime
from unittest import TestCase
from mrcerti.crawler.items.isap import IsapDocumentItem


class IsapTestCase(TestCase):
    
    def test_isap_document(self):
        item = IsapDocumentItem()
        item['url'] = 'http://blah.pl?xyz=abc'
        self.assertEquals(item['url'], 'http://blah.pl?xyz=abc')
        item['url'] = 'http://blah.pl;jsessionid=1234?abc=def'
        self.assertEquals(item['url'], 'http://blah.pl?abc=def')
        item['signature'] = {
            'year': '2004',
            'issue': '123',
            'position': '456',
        }
        self.assertEquals(item['signature']['year'], '2004')
        self.assertEquals(item['signature']['issue'], '123')
        self.assertEquals(item['signature']['position'], '456')
        item['signature'] = 'Dz.U. 2005 Nr 321 poz. 654'
        self.assertEquals(item['signature']['year'], '2005')
        self.assertEquals(item['signature']['issue'], '321')
        self.assertEquals(item['signature']['position'], '654')
        item['announced_date'] = datetime(2009, 3, 3)
        self.assertEquals(item['announced_date'], datetime(2009, 3, 3))
        item['announced_date'] = '2011-01-01'
        self.assertEquals(item['announced_date'], datetime(2011, 1, 1))
