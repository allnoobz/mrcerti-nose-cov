# -*- coding: utf-8 -*-

from types import NoneType
import re
from pprint import pformat
from mrcerti.convert.reference.parser import parse_legal_act_signature
from mrcerti.crawler.items import (
    DocumentItem, 
    FileItem, 
    ResourceItem, 
    Field,
    StringField, 
    DateField, 
    ListField,
    DictField,
    date_input_processor
)



def signature_input_processor(value):
    if isinstance(value, basestring):
        value = parse_legal_act_signature(value)
    assert isinstance(value, (dict, NoneType))
    return value


def remove_jsession_input_processor(value):
    assert isinstance(value, basestring)
    if ';jsessionid=' in value:
        value = re.sub(';jsessionid=[0-9A-Z]*', '', value)
    return value


class IsapFileItem(FileItem):
    type = StringField()
    type_id = StringField()


class IsapDocumentItem(DocumentItem):
    url = StringField(
        input_processor=remove_jsession_input_processor,
    )
    
    signature = DictField(
        input_processor=signature_input_processor,
    )
    
    announced_file = Field(type=IsapFileItem)
    published_file = Field(type=IsapFileItem)
    consistent_file = Field(type=IsapFileItem)
    
    status = StringField()
    
    announced_date = DateField(
        input_processor=date_input_processor,
    )
    publish_date = DateField(
        input_processor=date_input_processor,
    )
    effective_date = DateField(
        input_processor=date_input_processor,
    )
    repeal_date = DateField(
        input_processor=date_input_processor,
    )
    validity_date = DateField(
        input_processor=date_input_processor,
    )
    expiry_date = DateField(
        input_processor=date_input_processor,
    )
    
    publish_authority = StringField()
    obliged_authority = StringField()
    authorized_authority = StringField()
    
    notes = StringField()

    relations = ListField()

    def __str__(self):
        d = dict(self)
        if 'content' in d:
            d['content'] = u'...content...'
        if 'text' in d:
            d['text'] = d['text'][:100] + '...'
        if 'relations' in d:
            d['relations'] = u'...relations...'
        return unicode(pformat(d))
    
    def __unicode__(self):
        return self.__str__()
    
    def __repr__(self):
        return self.__str__()


class IsapRelationItem(ResourceItem):
    relation_type = Field()
    documents = Field()
    

class IsapRelatedDocumentItem(DocumentItem):
    url = Field(
        input_processor=remove_jsession_input_processor,
    )
    
    title = Field()
    signature = Field(
        input_processor=signature_input_processor,
    )
    status = Field()
    journal = Field()
    
    effective_date = Field(
        input_processor=date_input_processor,
    )
