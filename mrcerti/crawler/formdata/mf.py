# -*- coding: utf-8 -*-

from datetime import datetime

from mrcerti.crawler.formdata import FormDataSchema, TextInput, FilledForm


class MfFormDataSchema(FormDataSchema):
    phrase = TextInput('i_smpp_s_fraza')
    doc_type = TextInput('i_smpp_s_typ_dok_nr_sek_t', '0')
    year_from = TextInput('i_smpp_s_rok_od', '0')
    month_from = TextInput('i_smpp_s_mc_od', '0')
    day_from = TextInput('i_smpp_s_dzien_od', '0')
    year_to = TextInput('i_smpp_s_rok_do', '0')
    month_to = TextInput('i_smpp_s_mc_do', '0')
    day_to = TextInput('i_smpp_s_dzien_do', '0')
    publicator = TextInput('i_smpp_s_pub_nr_sek_t', '0')
    author = TextInput('i_smpp_s_au_nr_sek_t', '0')
    keyword = TextInput('i_smpp_s_sl_nr_sek_t', '0')
    topic = TextInput('i_smpp_s_kl_nr_sek_t', '0')
    signature = TextInput('i_smpp_s_sygnatura')
    
    # TODO: change doc_type, publicator, author, keyword, topic to MultiSelect
    # after research of possible values


class MfFilledForm(FilledForm):

    date_format = '%Y.%m.%d'

    def __init__(self, formdata_schema, **kwargs):
        if 'date_from' in kwargs:
            date_from = datetime.strptime(kwargs['date_from'], self.date_format)
            kwargs['year_from'] = str(date_from.year)
            kwargs['month_from'] = str(date_from.month)
            kwargs['day_from'] = str(date_from.day)
        if 'date_to' in kwargs:
            date_to = datetime.strptime(kwargs['date_to'], self.date_format)
            kwargs['year_to'] = str(date_to.year)
            kwargs['month_to'] = str(date_to.month)
            kwargs['day_to'] = str(date_to.day)

        super(MfFilledForm, self).__init__(formdata_schema, **kwargs)
