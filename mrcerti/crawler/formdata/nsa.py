# -*- coding: utf-8 -*-

from mrcerti.crawler.formdata import (
    FormDataSchema, 
    TextInput, 
    Select, 
    CheckBox,
    FormField,
)


class NsaYesNoCheckBox(FormField):
    def __init__(self, name, value, default=None, **kwargs):
        super(NsaYesNoCheckBox, self).__init__(name, default, **kwargs)
        self.value = value
    
    def get_data(self, checked):
        if checked is None and self.default is None:
            return None
        elif checked is None:
            checked = self.default
        elif checked is not None:
            checked = checked.strip() in ['True', 'true', 't', 'Yes', 'yes', 'y', '1']
        name = '%s%s' % (checked and 'tak' or 'nie', self.name)
        return {
            'name': name,
            'value': self.value
        }
    
    def info(self):
        return '\n'.join([
            'type: %s' % self.__class__.__name__,
            'default: %s' % ('checked' if self.default else 'unchecked'),
        ])
        

class NsaFormDataSchema(FormDataSchema):
    phrase = TextInput('wszystkieSlowa')
    occurrence = Select(
        'wystepowanie',
        [
            u'gdziekolwiek',
            u'w sentencji',
            u'w tezach',
            u'w uzasadnieniu'
        ],
        'gdziekolwiek'
    )
    variation = CheckBox('odmiana', 'on', True)
    signature = TextInput('sygnatura')
    court = Select(
        'sad',
        [
            u'dowolny',
            u'Naczelny Sąd Administracyjny',
            u'Wojewódzki Sąd Administracyjny w Białymstoku',
            u'Wojewódzki Sąd Administracyjny w Bydgoszczy',
            u'Wojewódzki Sąd Administracyjny w Gdańsku',
            u'Wojewódzki Sąd Administracyjny w Gliwicach',
            u'Wojewódzki Sąd Administracyjny w Gorzowie Wlkp.',
            u'Wojewódzki Sąd Administracyjny w Kielcach',
            u'Wojewódzki Sąd Administracyjny w Krakowie',
            u'Wojewódzki Sąd Administracyjny w Lublinie',
            u'Wojewódzki Sąd Administracyjny w Łodzi',
            u'Wojewódzki Sąd Administracyjny w Olsztynie',
            u'Wojewódzki Sąd Administracyjny w Opolu',
            u'Wojewódzki Sąd Administracyjny w Poznaniu',
            u'Wojewódzki Sąd Administracyjny w Rzeszowie',
            u'Wojewódzki Sąd Administracyjny w Szczecinie',
            u'Wojewódzki Sąd Administracyjny w Warszawie',
            u'Wojewódzki Sąd Administracyjny we Wrocławiu',
            u'NSA w Warszawie (przed reformą)',
            u'NSA oz. w Białymstoku',
            u'NSA oz. w Bydgoszczy',
            u'NSA oz. w Gdańsku',
            u'NSA oz. w Katowicach',
            u'NSA oz. w Krakowie',
            u'NSA oz. w Lublinie',
            u'NSA oz. w Łodzi',
            u'NSA oz. w Poznaniu',
            u'NSA oz. w Rzeszowie',
            u'NSA oz. w Szczecinie',
            u'NSA oz. we Wrocławiu',
        ],
        'dowolny'
    )
    type = Select(
        'rodzaj',
        [
            u'dowolny',
            u'Postanowienie',
            u'Uchwała',
            u'Wyrok',
        ],
        'dowolny'
    )
    symbols = TextInput('symbole')
    date_from = TextInput('odDaty')
    date_to = TextInput('doDaty')
    judges = TextInput('sedziowie')
    function = Select(
        'funkcja',
        [
            u'dowolna',
            u'przewodniczący',
            u'sprawozdawca',
            u'autor uzasadnienia',
        ],
        'dowolna'
    )
    final_judgment = NsaYesNoCheckBox('Prawomocne', 'on')
    closing_judgment = NsaYesNoCheckBox('Konczace', 'on')
    with_thesis = NsaYesNoCheckBox('Tezy', 'on')
    with_justification = NsaYesNoCheckBox('Uzasadnienie', 'on')
    with_dissenting_opinion = NsaYesNoCheckBox('Odrebne', 'on')
    authority_type = TextInput('rodzaj_organu')
    keywords = TextInput('hasla')
    acts = TextInput('akty')
    regulations = TextInput('przepisy')
    published = CheckBox('opublikowane', 'on', False)
    publications = TextInput('publikacje')
    with_glosses = CheckBox('glosowane', 'on', False)
    glosses = TextInput('glosy')
    submit = TextInput('submit', 'Szukaj')
