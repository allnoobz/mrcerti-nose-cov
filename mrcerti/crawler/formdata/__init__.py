# -*- coding: utf-8 -*-


class MissingFieldException(Exception):
    pass


class WrongOptionException(Exception):
    pass


def is_field(value):
    return (isinstance(value, type) and issubclass(value, FormField)) or isinstance(value, FormField)
            

class FormField(object):
    def __init__(self, name, default='', required=False, skip_if_missing=False):
        self.name = name
        self.default = default
        self.required = required
        self.skip_if_missing = skip_if_missing
    
    def get_data(self, value):
        return {
            'name': self.name,
            'value': self.to_form(value)
        }
    
    def to_form(self, value):
        if value is None:
            return self.default
        else:
            return value
    
    def info(self):
        return '\n'.join([
            'type: %s' % self.__class__.__name__,
            'default: %s' % (self.default if self.default != '' else '<empty>'),
        ])


class TextInput(FormField):
    pass


class CheckBox(FormField):
    def __init__(self, name, value, default=False, **kwargs):
        super(CheckBox, self).__init__(name, default, **kwargs)
        self.value = value
    
    def get_data(self, checked):
        if checked is not None:
            checked = checked.strip() in ['True', 'true', 't', 'Yes', 'yes', 'y', '1']
        if checked or (checked is None and self.default):
            return {
                'name': self.name,
                'value': self.value
            }
        else:
            return None
    
    def info(self):
        return '\n'.join([
            'type: %s' % self.__class__.__name__,
            'default: %s' % ('checked' if self.default else 'unchecked'),
        ])

        
class Select(FormField):
    def __init__(self, name, options, default='', **kwargs):
        super(Select, self).__init__(name, default=default, **kwargs)
        self.options = options
    
    def to_form(self, value):
        if value is None:
            return self.default
        elif value in self.options:
            return value
        else:
            raise WrongOptionException(
                'Option for field "%s" must be one of: %s. Got: %s.' % 
                (self.name, str(self.options), value)
            )
    
    def info(self):
        return '\n'.join([
            super(Select, self).info(),
            'options:\n%s' % '\n'.join(['\t\t%s' % option for option in self.options]),
        ])


class MultiSelect(Select):    
    def to_form(self, value):
        if value is None:
            return self.default
        values = [v.strip() for v in value.split(',')]
        for opt in values:
            if not opt in self.options:
                raise WrongOptionException(
                    'All options for field "%s" must be one of: %s. Got: %s.' % 
                    (self.name, str(self.options), opt)
                )
        return value


class DeclarativeMeta(type):
    def __new__(cls, name, bases, attrs):
        fields = [name_ for name_, field in attrs.items() if is_field(field)]
        attrs['__fields__'] = fields
        inst = super(DeclarativeMeta, cls).__new__(cls, name, bases, attrs)
        return inst
    
    def __setattr__(self, name, value):
        super(DeclarativeMeta, self).__setattr__(name, value)
        if is_field(value):
            self.__fields__.append(name)


class FormDataSchema(object):
    __metaclass__ = DeclarativeMeta
    __fields__ = []
    
    def formdata(self, values):
        formdata = dict()
        cls = self.__class__
        for name in cls.__fields__:
            field = getattr(cls, name)
            if name not in values:
                if field.required:
                    raise MissingFieldException('Required field "%s" is missing.' % name)
                elif field.skip_if_missing:
                    continue
            fielddata = field.get_data(values.get(name, None))
            if fielddata:
                formdata[fielddata['name']] = fielddata['value']
        return formdata
    
    def info(self):
        return '\n'.join(
            ['', '#### %s #### ' % self.__class__.__name__] +
            [
                '\n'.join([
                    '\n## %s ## ' % field,
                    getattr(self, field).info()
                ]) for field in self.__fields__
            ]
        )


class FilledForm(object):

    def __init__(self, formdata_schema, **kwargs):
        self.formdata_schema = formdata_schema
        self.formdata = formdata_schema.formdata(kwargs)
        self.fields = kwargs.copy()

    def clone(self, **kwargs):
        """Returns clone of this `FilledForm` instance.

        After this, constructed `FilledForm` instance contains formdata
        which is a copy of `self.formdata` (except updated by passing kwargs).
        """
        values = dict(self.fields.items() + kwargs.items())
        return self.__class__(self.formdata_schema, **values)
