# -*- coding: utf-8 -*-

import urllib2
from urlparse import urljoin
import traceback
from scrapy import log
from scrapy.exceptions import NotConfigured
from mrcerti.crawler.signals import workers_closed


class ContentOptimizer(object):
    
    def __init__(self, crawler, settings):
        self.api_url = settings.get('API_URL')
        self.api_url = urljoin(self.api_url, '/v1/content/optimize')
        crawler.signals.connect(self.optimize, signal=workers_closed)
    
    @classmethod
    def from_crawler(cls, crawler):
        if not crawler.settings.getbool('OPTIMIZER_ENABLED', True):
            raise NotConfigured()
        return cls(crawler, crawler.settings)
    
    def optimize(self):
        log.msg('Optimizing contents', log.DEBUG)
        req = urllib2.Request(self.api_url)
        try:
            urllib2.urlopen(req)
        except Exception:
            log.msg('Contents optimization failed', log.ERROR)
            traceback.print_exc()
