# -*- coding: utf-8 -*-

import traceback
from threading import Thread
from Queue import Queue, Empty
from scrapy import signals, log
from scrapy.exceptions import (
    NotConfigured,
    DropItem,
)
from scrapy.utils.misc import load_object
from mrcerti.crawler.signals import (
    workers_closed,
    pre_stage,
    post_stage,
    item_processed
)
from mrcerti.crawler.stats import StatManager
from mrcerti.crawler.signal_mixin import SignalMixin

class ItemProcessors(StatManager):
    '''
    Extension using item processors.
    Creates queue of composed of processors fed by items already processed by pipelines.
    For each processor entry in the config file the following are created:
    - processor instance
    - queue for incoming items
    - worker threads.
    
    Unfinished items from last session are loaded using processor's load_items() method.
    
    Finally workers are started. Items are then fed into queues. Each processor 
    processes items from its queue. Once processing is completed for a particular
    item it is passed on to the next queue.
    
    
    Settings:
    
    WORKERS_ENABLED
      default: False
      Whether to create processors and run workers.
    
    PROCESSORS
      default: []
      Definition of processors and workers. Format:
      [
        ('processor.class.name', <number_of_workers>)
      ]
    '''
    def __init__(self, crawler, settings):
        self.signals = crawler.signals
        self.stats = crawler.stats

        self._setup_workers(crawler, settings)
        if len(self.workers) == 0:
            raise NotConfigured()

        if settings.get("PROCESSING_SIGNALS_ENABLED", True):
            SignalMixin.enable(self.signals)
        self._run()

    def _setup_workers(self, crawler, settings):
        self.workers = []
        self.processors = []
        self._processor_by_name = {}
        self._queue_by_name = {}
        self._workers_by_name = {}
        
        for clsname, count in settings.get('PROCESSORS', []):
            processor_cls = load_object(clsname)
            processor = processor_cls(crawler, settings)
            processor.stats = crawler.stats
            queue = Queue()
            name = processor.queue
            self.processors.append(processor)
            self._processor_by_name[name] = processor
            self._queue_by_name[name] = queue
            for i in range(count):
                worker = ProcessorWorker(processor, queue, i)
                worker.daemon = True
                self.workers.append(worker)
        
        for i, worker in enumerate(self.workers):
            processor = worker.processor
            name = processor.queue
            next_worker = None
            if i < len(self.workers) - 1:
                next_worker = self.workers[i + 1]
            if next_worker:
                close_callback = next_worker.stop_and_join
            else:
                close_callback = self.closed
            processed_callback = self._processed_next(name)
            worker.set_callbacks(
                close_callback=close_callback,
                processed_callback=processed_callback,
            )

    def connect_signals(self):
        self.signals.connect(self.pre_stage, signal=pre_stage)
        self.signals.connect(self.post_stage, signal=post_stage)
        self.signals.connect(self.item_scraped, signal=signals.item_scraped)
        self.signals.connect(self.spider_closed, signal=signals.spider_closed)

    def _run(self):
        # FIXME: This will now be part of a session restarter when it gets 
        # implemented
        # self._load()
        
        for worker in self.workers:
            assert worker.daemon
            worker.start()

        self.connect_signals()
    
    # def _load(self):
    #     session = Session()
    #     models = session.query(ItemModel).all()
    #     for model in models:
    #         self._load_item(session, model)
    #     session.commit()
    #     session.close()
    
    # def _load_item(self, session, model):
    #     if model.queue is None or model.queue not in self._processor_by_name:
    #         # session.delete(item)
    #         return
    #     processor = self._processor_by_name[model.queue]
    #     queue = self._queue_by_name[model.queue]
    #     item = cPickle.loads(model.scrapy_item)
    #     log.msg('Loaded item for %s processor: %s' % (processor, item), log.DEBUG)
    #     queue.put(item)

    # def _save_item(self, item, queue):
    #     session = Session()
    #     session.query(ItemModel).filter_by(
    #         scrapy_item_id=item['item_id'],
    #     ).update({
    #         'scrapy_item': cPickle.dumps(item, protocol=-1),
    #         'queue': queue,
    #     })
    #     session.commit()
    #     session.close()

    def _processed_next(self, name):
        # NOTE: This will be called in the context of the worker thread
        def fn(item):
            # FIXME: If we wish to admit addition and removal of processors on
            # the fly, synchronisation we need to put a mutex around this block
            # as well as other blocks accessing the processor list.
            processor = self._processor_by_name[name]
            i = self.processors.index(processor)
            if i < len(self.processors) - 1:
                next_processor = self.processors[i + 1]
                next_name = next_processor.queue
            else:
                next_processor = None
                next_name = None
            # FIXME: This will be part of the session recorder when it gets
            # implemented.
            # self._save_item(item, next_name)
            if next_processor:
                queue = self._queue_by_name[next_name]
                queue.put(item)
            else:
                self.item_processed(item)
        return fn

    def _processed_first(self, item):
        # session = Session()
        
        # model = session.query(ItemModel).filter_by(
        #     scrapy_item_id=item['item_id'],
        # ).first() or ItemModel()   
        
        # if model.id is None:
        #     model.spider = item['spider']
        #     model.url = item['url']
        #     model.scrapy_item_id = item['item_id']
        #     model.scrapy_item = cPickle.dumps(item, protocol=-1)
        #     model.item_hash = hash(item)
        
        assert len(self.processors) > 0
        
        queue = self.processors[0].queue
        
        # model.queue = self.processors[0].queue
        # if model.id is None:
        #     session.add(model)
        #     session.commit()
            
        queue = self._queue_by_name[queue]        
        queue.put(item)
    
    @classmethod
    def from_crawler(cls, crawler):
        if not crawler.settings.getbool('WORKERS_ENABLED'):
            raise NotConfigured()
        return cls(crawler, crawler.settings)
    
    def item_processed(self, item):
        log.msg('Processed item: %s' % item['item_id'])
        self.signals.send_catch_log(signal=item_processed, item=item)
    
    def item_scraped(self, item, spider):
        log.msg('Processing item: %s' % item['item_id'])
        self._processed_first(item)
    
    def spider_closed(self, spider):
        log.msg('Closing workers', log.DEBUG)
        assert len(self.workers) > 0
        self.workers[0].stop_and_join()
    
    def closed(self):
        log.msg('Workers closed', log.DEBUG)
        self.signals.send_catch_log(signal=workers_closed)

    def pre_stage(self, item, processor):
        self.stat_inc("%s-pre" % processor.__name__)
        log.msg('Processor %s began processing item %s' % (processor.__name__, item['item_id']))

    def post_stage(self, item, processor):
        self.stat_inc("%s-post" % processor.__name__)
        log.msg('Processor %s finished processing item %s' % (processor.__name__, item['item_id']))

class ProcessorWorker(Thread):
    '''
    Thread for processing items in processors. Callbacks must be set before run.
    
    processed_callback is called when item if successfully processed
    close_callback is called when worker is closing
    
    '''
    
    def __init__(self, processor, queue, number):
        self.processor = processor
        self.queue = queue
        self.stopping = False
        name = '%s-%s' % (processor, str(number))
        super(ProcessorWorker, self).__init__(name=name)
    
    def run(self):
        while True:
            try:
                item = self.queue.get(timeout=1)
                item = self.processor.process_item(item)
                if item:
                    self.processed_callback(item)
            except Empty:
                if self.stopping:
                    break
            except DropItem:
                log.msg("Item %s dropped by processor %s." % (item, self.name))
            except Exception as e:
                traceback.print_exc()
                log.msg("Exception %s(%s) caused failure during processing item %s by processor %s. "
                        "The item was dropped." % (e.__class__, e, item, self.name), log.ERROR)
        self.close_callback()
        log.msg('Closing %s worker' % self.name, log.DEBUG)
    
    def stop(self):
        self.stopping = True

    def stop_and_join(self):
        self.stop()
        self.join()

    @property
    def running(self):
        return not self.stopping or self.queue.qsize() > 0
    
    def set_callbacks(self, processed_callback, close_callback):
        self.processed_callback = processed_callback
        self.close_callback = close_callback

