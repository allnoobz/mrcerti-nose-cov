# -*- coding: utf-8 -*-

from scrapy.exceptions import NotConfigured
from mrcerti.crawler.models.base import Session


class DatabaseConnector(object):
    
    def __init__(self, crawler, settings):
        config = crawler.settings.get('DB_CONFIG')
        Session.from_config(config)
    
    @classmethod
    def from_crawler(cls, crawler):
        if not crawler.settings.get('DB_CONFIG'):
            raise NotConfigured()
        return cls(crawler, crawler.settings)
