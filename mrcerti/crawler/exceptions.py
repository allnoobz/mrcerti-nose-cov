# -*- coding: utf-8 -*-

class PageParseException(Exception):
    pass


class DatabaseNotConfigured(Exception):
    
    def __init__(self, message='"DB_CONFIG" not found in settings.'):
        Exception.__init__(self, message)
