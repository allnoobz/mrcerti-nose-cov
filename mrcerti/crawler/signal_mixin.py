# -*- coding: utf-8 -*-
from mrcerti.crawler.signals import (
    pre_stage,
    post_stage,
)


class SignalMixin(object):
    signal_manager = None
    enabled = False

    @classmethod
    def enable(cls, signals):
        assert signals is not None
        cls.enabled = True
        cls.signal_manager = signals

    def _process_item(self, item, spider=None):
        raise NotImplemented()

    def process_item(self, item, spider=None):
        if SignalMixin.enabled:
            SignalMixin.signal_manager.send_catch_log(
                pre_stage,
                item=item,
                processor=self.__class__
            )

            item = self._process_item(item, spider)

            SignalMixin.signal_manager.send_catch_log(
                post_stage,
                item=item,
                processor=self.__class__
            )

            return item

        else:
            return self._process_item(item, spider)




