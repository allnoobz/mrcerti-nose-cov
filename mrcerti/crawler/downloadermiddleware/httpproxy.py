# -*- coding: utf-8 -*-

from zope.component import getUtility, getGlobalSiteManager
from urlparse import urlunparse, urlparse
from twisted.internet.error import (
    TimeoutError,
    ConnectionRefusedError,
    ConnectionLost,
)
from scrapy import signals
from scrapy.exceptions import NotConfigured
from mrcerti.crawler.downloadermiddleware.service.httpproxy import (
    IHttpProxyService,
    ProxyMeshService,
    AwsProxyService,
    ProxyListService
)


class HttpProxyMiddleware(object):
    '''
    Base class for http proxy middlewares.
    
    On initialization registers IHttpProxyService implementation.
    Service class is taken from httpproxy_service_cls attribute.
    
    Set proxy when get request without 'use_proxy' set to False in request.meta.
    
    Ban proxy when got one of PROXY_BAN_EXCEPTIONS or response code is >= 400.
    Retry with new proxy.
    
    Settings:
    
    PROXY_ENABLED
      default: False
      Whether to enable http proxy. 
    '''
    
    PROXY_BAN_EXCEPTIONS = (
        TimeoutError,
        ConnectionRefusedError,
        ConnectionLost
    )
    
    def __init__(self, settings, crawler):
        gsm = getGlobalSiteManager()
        httpproxy_service = self.httpproxy_service_cls(settings, crawler)
        gsm.registerUtility(httpproxy_service, IHttpProxyService)
        crawler.signals.connect(self.spider_opened, signal=signals.spider_opened)
    
    @classmethod
    def from_crawler(cls, crawler):
        if not crawler.settings.getbool('PROXY_ENABLED', False):
            raise NotConfigured()
        return cls(crawler.settings, crawler)
    
    def process_request(self, request, spider):
        if request.meta.get('use_proxy', True):
            self.httpproxy_service.set_proxy(spider, request)
    
    def process_exception(self, request, exception, spider):
        if isinstance(exception, self.PROXY_BAN_EXCEPTIONS):
            self.httpproxy_service.ban_proxy(spider, request = request)
            return request
    
    def process_response(self, request, response, spider):
        if 400 <= response.status:
            self.httpproxy_service.ban_proxy(spider, response = response)
            self.httpproxy_service.set_proxy(spider, request)
            return request
        return response
    
    def spider_opened(self, spider):
        self.httpproxy_service = getUtility(IHttpProxyService)


class ProxyMeshMiddleware(HttpProxyMiddleware):
    '''
    Http proxy middleware using proxymesh.com
    
    
    Settings:
    
    PROXYMESH_URL
      default: None
      Url of the Proxymesh proxy server.
    
    PROXYMESH_PORT
      default: None
      Port of the Proxymesh proxy server.
    
    PROXYMESH_USER
      default: None
      User of the Proxymesh proxy server.
    
    PROXYMESH_PASS
      default: None
      Password of the Proxymesh proxy server user.
    
    PROXYMESH_COUNT
      default: 10
      Number of proxies which have to be banned before stop crawling due to lack of new proxies.
    '''
    
    httpproxy_service_cls = ProxyMeshService


class AwsProxyMiddleware(HttpProxyMiddleware):
    '''
    Http proxy middleware using Amazon EC2
    
    
    Settings:
    
    AWS_REGION
      default: None
      Region of the instances.
      
    AWS_ACCESS_KEY_ID
      default: None
      Amazon Access Key ID.
      
    AWS_SECRET_ACCESS_KEY
      default: None
      Amazon Secret Access Key.
      
    When using this http proxy middleware set the following settings as well:
    
    RETRY_ENABLED = True
    RETRY_TIMES = 10
    RETRY_HTTP_CODES = [111]
    '''
    
    PROXY_BAN_EXCEPTIONS = (
        TimeoutError,
        ConnectionLost
    )
    
    httpproxy_service_cls = AwsProxyService
    
    
class ProxyListMiddleware(HttpProxyMiddleware):
    '''
    Http proxy middleware using file with list of proxies
    
    
    Settings:
    
    PROXY_LIST_FILE
      default: None
      Path to file with proxies.
    
    PERMANENT_PROXY_BAN
      default: False
      Define if banned proxy should be removed from file.
    
    PROXY_LIST_FILE
      default: None
      Path to file with proxies.
    
    PROXY_LIST_FILE
      default: None
      Path to file with proxies.
    '''
    
    httpproxy_service_cls = ProxyListService
    
    
class GaeProxyMiddleware(object):
    '''
    Http proxy middleware using GAE
    
    Settings:
    
    GAE_PROXY_DOMAIN
      default: None
      Domain of GAE proxy server.
    '''
    
    def __init__(self, settings, crawler):
        self.gae_domain = settings.get('GAE_PROXY_DOMAIN')
        
    @classmethod
    def from_crawler(cls, crawler):
        if not crawler.settings.get('PROXY_ENABLED', False):
            raise NotConfigured()
        return cls(crawler.settings, crawler)
    
    def process_request(self, request, spider):
        if self.need_proxifying(request):
            request = self.proxify(request)
            return request
    
    def process_response(self, request, response, spider):
        if self.need_unproxifying(request):
            response = self.unproxify(request, response)
        return response
        
    def need_proxifying(self, request):
        return urlparse(request.url).netloc != self.gae_domain
        
    def need_unproxifying(self, request):
        return urlparse(request.url).netloc == self.gae_domain and request.meta.get('true_url')
    
    def proxify(self, request):
        true_url = request.url
        parsed = list(urlparse(true_url))
        parsed[1] = self.gae_domain
        url = urlunparse(parsed)
        request.meta['true_url'] = true_url
        return request.replace(url = url)
    
    def unproxify(self, request, response):
        true_url = request.meta.get('true_url')
        if true_url:
            response = response.replace(url = true_url)
        return response
        
