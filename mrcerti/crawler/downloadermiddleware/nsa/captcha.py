# -*- coding: utf-8 -*-

from zope.component import getUtility
from scrapy import log, signals
from mrcerti.crawler.downloadermiddleware.service.httpproxy import IHttpProxyService


class CaptchaFilterMiddleware(object):
    '''Base class for filtering captchas in responses'''
    
    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler.settings, crawler)
    
    def has_captcha(self, response):
        return response.body and (
            u'recaptcha_response_field' in response.body or
            u'Poczekaj trwa wczytywanie orzeczenia' in response.body or 
            u'Nie znaleziono orzeczeń spełniających podany warunek!' in response.body
        )


class CaptchaRequestRetryMiddleware(CaptchaFilterMiddleware):
    ''' Retry request if found captcha'''
    
    def __init__(self, settings, crawler):
        pass
    
    def process_response(self, request, response, spider):
        if self.has_captcha(response):
            log.msg('Found Captcha', log.DEBUG)
            return request
        return response


class CaptchaProxyBanMiddleware(CaptchaFilterMiddleware):
    ''' Ban used http proxy if found captcha'''
    
    def __init__(self, settings, crawler):
        crawler.signals.connect(self.spider_opened, signal=signals.spider_opened)
    
    def process_response(self, request, response, spider):
        if self.has_captcha(response):
            log.msg('Found Captcha', log.DEBUG)
            self.httpproxy_service.ban_proxy(spider, request=request, response=response)
            return request
        return response
    
    def spider_opened(self, spider):
        self.httpproxy_service = getUtility(IHttpProxyService)
