# -*- coding: utf-8 -*-

from scrapy import log


class LoggingDownloaderMiddleware(object):

    def process_request(self, request, spider):
        spider.log(
            'passing request to "%s" to downloader' % request.url,
            level=log.INFO,
        )
        spider.log(
            'headers for "%s":\n    %s' % (request.url, request.headers),
            level=log.DEBUG,
        )

    def process_response(self, request, response, spider):
        spider.log(
            'getting response from "%s"\n'
            'URL of the request that originated the response is "%s"' %
                (response.url, request.url),
            level=log.INFO,
        )
        spider.log(
            'headers for request "%s":\n    %s\n'
            'and headers for response "%s"\n    %s' %
                (request.url, request.headers, response.url, response.headers),
            level=log.DEBUG,
        )

        try:
            if id(response.request) != id(request):
                spider.log(
                    'for response from "%s" (with headers %s) request from response '
                    '(%s with headers %s) and request that originated the response '
                    '(%s with headers %s) differs.' %
                        (response.url, response.headers,
                        response.request.url, response.request.headers,
                        request.url, request.headers),
                    level=log.INFO,
                )
        except AttributeError:
            spider.log(
                'response.url, response.headers, request.url, '
                'request.headers, response.request.url or '
                'response.request.headers does not exist.',
                level=log.INFO,
            )

        return response

    def process_exception(self, request, exception, spider):
        spider.log(
            'exception while processing "%s".' % request.url,
            level=log.ERROR,
        )
        spider.log(
            'headers for "%s":\n    %s' % (request.url, request.headers),
            level=log.DEBUG,
        )
        spider.log('exception:\n%s' % exception, level=log.DEBUG)
