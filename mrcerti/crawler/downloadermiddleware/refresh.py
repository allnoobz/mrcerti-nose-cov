# -*- coding: utf-8 -*-

from scrapy import log
from scrapy.exceptions import IgnoreRequest


class RefreshMiddleware(object):
    def __init__(self, settings):
        self.max_refresh_times = settings.getint('REFRESH_MAX_TIMES', 5)
    
    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler.settings)
    
    def _refresh(self, request, refreshed):
        ttl = request.meta.setdefault('refresh_ttl', self.max_refresh_times)
        refreshes = request.meta.get('refresh_times', 0) + 1

        if ttl and refreshes <= self.max_refresh_times:
            refreshed.meta['refresh_times'] = refreshes
            refreshed.meta['refresh_ttl'] = ttl - 1
            refreshed.dont_filter = request.dont_filter
            refreshed.priority = request.priority
            log.msg("Refreshing %s" % refreshed, log.DEBUG)
            return refreshed
        else:
            log.msg("Discarding %s: max refreshes reached" % request, log.DEBUG)
            raise IgnoreRequest()
    
    def process_response(self, request, response, spider):
        if 'Refresh' in response.headers:
            refreshed = request.replace(method='GET', body='')
            refreshed.headers.pop('Content-Type', None)
            refreshed.headers.pop('Content-Length', None)
            
            return self._refresh(request, refreshed)
        return response