# -*- coding: utf-8 -*-

import random
import base64
import re
import time
import threading
import boto.ec2
from zope.interface import Interface, implements
from scrapy import log, signals
from scrapy.exceptions import CloseSpider
from scrapy.xlib.pydispatch import dispatcher


def synchronized(method):
    def new_method(self, *arg, **kws):
        with self._lock:
            return method(self, *arg, **kws)
    return new_method

class EmptyProxyListException(Exception):
    pass

class IHttpProxyService(Interface):
    '''
    Interface for all HttpProxyService classes.
    Used to set and ban proxies.
    
    Current used by CaptchaProxyBanMiddleware and all HttpProxyMiddlewares.
    '''
    
    def set_proxy(self, request):
        raise NotImplemented()
    
    def ban_proxy(self, request=None, response=None):
        raise NotImplemented()
    
    def _set_proxy(self, request):
        raise NotImplemented()
    
    def _ban_proxy(self, request=None, response=None):
        raise NotImplemented()


class HttpProxyService(object):
    implements(IHttpProxyService)
    _lock = threading.RLock()
    
    @synchronized
    def set_proxy(self, spider, request):
        try:
            self._set_proxy(spider, request)
            log.msg('Set proxy: %s' % request.meta['proxy'], log.DEBUG)
        except EmptyProxyListException:
            log.msg('Banned all proxies. Closing spider.', log.ERROR)
            raise CloseSpider()
                        
    @synchronized
    def ban_proxy(self, spider, request=None, response=None):
        if request and request.meta:
            log.msg('Ban proxy: %s' % request.meta['proxy'], log.DEBUG)
        return self._ban_proxy(spider, request, response)
    
    def _set_proxy(self, spider, request):
        raise NotImplemented()
    
    def _ban_proxy(self, spider, request=None, response=None):
        raise NotImplemented()
    
class ProxyMeshService(HttpProxyService):
    
    def __init__(self, settings, crawler):
        self.proxymesh_url = settings.get('PROXYMESH_URL')
        self.proxymesh_port = settings.get('PROXYMESH_PORT')
        self.proxymesh_user = settings.get('PROXYMESH_USER')
        self.proxymesh_pass = settings.get('PROXYMESH_PASS')
        self.proxies_count = settings.getint('PROXYMESH_COUNT', 10)
        self.creds = base64.encodestring(':'.join([self.proxymesh_user, self.proxymesh_pass]))
        self.proxymesh_address = ':'.join([self.proxymesh_url, self.proxymesh_port])
        if not self.proxymesh_address.startswith('http://'):
            self.proxymesh_address = ''.join(['http://', self.proxymesh_address]) 
        self.banned_proxies = set()
    
    def _set_proxy(self, spider, request):
        request.headers.pop('X-ProxyMesh-IP', None)
        request.meta['proxy'] = self.proxymesh_address
        request.headers['X-ProxyMesh-Not-IP'] = ','.join(self.banned_proxies)
        request.headers['Proxy-Authorization'] = 'Basic ' + self.creds
    
    def _ban_proxy(self, spider, request=None, response=None):
        proxy = None
        if request:
            pass
        if response:
            proxy = response.headers.get('X-ProxyMesh-IP')
        if proxy:
            log.msg('Ban proxy: %s' % proxy, log.DEBUG)
            self.banned_proxies.add(proxy)
        if len(self.banned_proxies) == self.proxies_count:
            log.msg('Banned all proxies. Closing spider.', log.ERROR)
            raise CloseSpider()


class ProxyListService(HttpProxyService):
    
    def __init__(self, settings, crawler):
        self.proxy_file = settings.get('PROXY_LIST_FILE')
        self.permanent_ban = settings.get('PERMANENT_PROXY_BAN')
        self.concurrent_requsets_per_proxy = settings.getint('CONCURRENT_REQUESTS_PER_PROXY')
        self.concurrent_requsets = settings.getint('CONCURRENT_REQUESTS')
        self.proxies = self.load_proxies()
        self.next_proxies = []
        if self.concurrent_requsets_per_proxy:
            dispatcher.connect(self.set_number_of_concurrent_requests, signal=signals.spider_opened)
                
    def _set_proxy(self, spider, request):
        proxy = self.get_proxy()
        request.meta['proxy'] = proxy
        log.msg('Use proxy: %s' % proxy, log.DEBUG)
    
    def _ban_proxy(self, spider, request=None, response=None):
        if not request or not request.meta:
            log.msg('Cannot ban proxy.')
            return
        proxy = request.meta['proxy']
        if not proxy or proxy not in self.proxies:
            return
        self.proxies.remove(proxy)
        log.msg('Banned proxy: %s' % proxy, log.INFO)
        self.set_number_of_concurrent_requests(spider)
        # permanent ban - remove from file
        if self.permanent_ban:
            with open(self.proxy_file, 'w') as f:
                for proxy in self.proxies:
                    f.write('%s\n' % proxy)
    
    def get_proxy(self):
        if not self.next_proxies:
            self.next_proxies = list(self.proxies)
            #random.shuffle(self.next_proxies)
        try:
            return self.next_proxies.pop()
        except:
            raise EmptyProxyListException()
        
    @synchronized
    def load_proxies(self):
        addresses = []
        with open(self.proxy_file, 'r') as f:
            for line in f:
                line = line.strip()
                address = re.search('^(http://)?[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}:[0-9]{1,5}$', line)
                if address:
                    address = address.group()
                    if not address.startswith('http://'):
                        address = 'http://%s' % address
                    addresses.append(address)
        return addresses

    def set_number_of_concurrent_requests(self, spider):
        if not self.concurrent_requsets_per_proxy:
            return
        number = self.concurrent_requsets_per_proxy * len(self.proxies)
        if number > self.concurrent_requsets:
            number = self.concurrent_requsets
        spider.crawler.engine.downloader.total_concurrency = number
        log.msg("Concurrent requests set to: %d" % number, log.DEBUG)
    
    
class AwsProxyService(HttpProxyService):
    
    def __init__(self, settings, crawler):
        self.aws_region = settings.get('AWS_REGION')
        self.aws_access_key_id = settings.get('AWS_ACCESS_KEY_ID')
        self.aws_secret_access_key = settings.get('AWS_SECRET_ACCESS_KEY')
        #self.proxy_user = settings.get('PROXY_USER')
        #self.proxy_pass = settings.get('PROXY_PASS')
        
        self.conn = boto.ec2.connect_to_region(
            region_name=self.aws_region,
            aws_access_key_id=self.aws_access_key_id,
            aws_secret_access_key=self.aws_secret_access_key
        )
        self.instances = {}
        for reservation in self.conn.get_all_instances():
            self.instances.update({
                instance: None for instance in reservation.instances
            })
        
        #self.creds = base64.encodestring(':'.join([self.proxy_user, self.proxy_pass]))
        
        crawler.signals.connect(self.spider_opened, signal=signals.spider_opened)
        crawler.signals.connect(self.spider_closed, signal=signals.spider_closed)
    
    def _set_proxy(self, spider, request):
        self.update_instances()
        instances = self.running_instances()
        while not instances:
            time.sleep(1)
            self.update_instances()
            instances = self.running_instances()
        instance = random.choice(instances)
        request.meta['proxy'] = self.instances[instance]
        #request.headers['Proxy-Authorization'] = 'Basic ' + self.creds
    
    def _ban_proxy(self, spider, request=None, response=None):
        instance = self.instance_by_url(request.meta['proxy'])
        if not instance:
            log.msg('Cannot match proxy to instance', log.WARNING)
            return
        self.instances[instance] = None
        self.stop_instance(instance)
        
    def spider_opened(self):
        self.update_instances()
        
    def spider_closed(self):
        for instance, _ in self.instances.items():
            self.stop_instance(instance)
    
    def update_instances(self):
        for instance, _ in self.instances.items():
            status = instance.update()
            url = None
            if status == u'stopped':
                self.start_instance(instance)
            elif status == u'running':
                dns_name = instance.dns_name
                port = instance.tags.get('proxy-port', '3128')
                url = 'http://%s:%s' % (dns_name, port)
            self.instances[instance] = url
    
    def running_instances(self):
        return [instance for instance, url in self.instances.items() if url]
    
    def instance_by_url(self, url):
        for instance, _url in self.instances.iteritems():
            if _url == url:
                return instance
    
    def start_instance(self, instance):
        instance.start()
        log.msg('Starting instance %s...' % instance.id, log.DEBUG)
    
    def stop_instance(self, instance):
        instance.stop()
        log.msg('Stopping instance %s...' % instance.id, log.DEBUG)
