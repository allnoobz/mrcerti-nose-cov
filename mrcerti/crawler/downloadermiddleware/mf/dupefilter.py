# -*- coding: utf-8 -*-

import re
import urlparse
from scrapy.dupefilter import BaseDupeFilter


class IdDupeFilter(BaseDupeFilter):
    '''Duplicates filter specific for MF domain'''
    
    idents = set()
    
    def request_seen(self, request):
        if not re.search('\?.*i_smpp_s_dok_nr_sek=',request.url):
            return
        url = request.url
        ident = urlparse.parse_qs(urlparse.urlparse(url).query)['i_smpp_s_dok_nr_sek'][0]
        if ident in self.idents:
            return True
        else:
            self.idents.add(ident)
