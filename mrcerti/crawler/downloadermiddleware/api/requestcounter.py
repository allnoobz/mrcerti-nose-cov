# -*- coding: utf-8 -*-


class RequestCounterMiddleware(object):

    def process_request(self, request, spider):
        spider.request_count += 1

    def process_exception(self, request, exception, spider):
        spider.log('[QUERY_INFO] Error')
        spider.status['ERROR'] = 1
        return None

    def process_response(self, request, response, spider):
        response_status = str(response.status)
        if spider.status.has_key(response_status):
            spider.status[response_status] += 1
        else:
            spider.status[response_status] = 1
        return response