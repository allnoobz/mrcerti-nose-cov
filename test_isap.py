# -*- coding: utf-8 -*-

import sys
import json
from mrcerti.convert.pdf.extract import get_file_content
from mrcerti.convert.content.transform.isap import legal_act
from mrcerti.convert.output import to_json


def manual():
    print (
        ur"""Usage:
        %s <pdf file name>"""
    ) % sys.argv[0] 


def main():
    if len(sys.argv) <= 1:
        manual()
        exit(1)
    content = get_file_content(filename=sys.argv[1])
    content = legal_act(content)
    data = to_json(content)
    print json.dumps(data, indent=4)
    # data = urllib.urlencode({'data': json.dumps(data)})
    # req = urllib2.Request(url, data)
    # response = urllib2.urlopen(req)
    # print response.read()

    
if __name__ == '__main__':
    main()
