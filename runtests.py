# -*- coding: utf-8 -*-

import sys
import argparse

if sys.argv[0].endswith("__main__.py"):
    sys.argv[0] = "python -m unittest"

__unittest = True

from unittest.main import main, TestProgram, USAGE_AS_MAIN
import xmlrunner

TestProgram.USAGE = USAGE_AS_MAIN


if __name__ == '__main__':
    parsed_args = filter(lambda s: s.startswith('--xml-output'), sys.argv)
    left_args = filter(lambda s: not s.startswith('--xml-output'), sys.argv)
    parser = argparse.ArgumentParser()
    parser.add_argument('--xml-output', const='output', nargs='?')
    args = parser.parse_args(parsed_args)
    sys.argv[:] = left_args
    if args.xml_output:
        try:
            runner = xmlrunner.XMLTestRunner(output=args.xml_output)
            main(testRunner=runner, module=None)
        except SystemExit:
            exit(0)
    else:
        main(module=None)
