import os

from collections import namedtuple
from setuptools import setup, find_packages
from pip.req import parse_requirements

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.txt')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()
options = namedtuple('InstallOptions', ['skip_requirements_regex', 'default_vcs'])
options.skip_requirements_regex = ur'mrcerti-convert|morfeusz|pdfminer'
options.default_vcs = None
requires = parse_requirements(os.path.join(here, 'requirements.txt'), options=options)
requires = [str(r.req) for r in requires]

setup(
    name='mrcerti-crawler',
    version='0.0',
    description='mrcerti-crawler',
    long_description=README + '\n\n' + CHANGES,
    classifiers=[
      "Programming Language :: Python",
      "Framework :: Pyramid",
      "Topic :: Internet :: WWW/HTTP",
      "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
      ],
    author='',
    author_email='',
    url='',
    keywords='web wsgi bfg pylons pyramid',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    test_suite='mrcerti-crawler',
    install_requires=requires,
    entry_points="",
)
